﻿using System;
using AgentLib.Spy.Statistics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AgentLibTesting.Spy.Statistics
{
    [TestClass]
    public class StatisticsActiveProcessInfoTest
    {
        [TestMethod]
        public void EqualsAllNullsTest()
        {
            var o1 = new StatisticsActiveProcessInfo();
            var o2 = new StatisticsActiveProcessInfoTest();
            Assert.Equals(o1, o2);
        }
    }
}
