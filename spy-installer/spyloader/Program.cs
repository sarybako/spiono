﻿using System;
using Shared;
using Shared.Logging;
using msctrlsysadm.launcher;

namespace msctrlsysadm
{
    static class Program
    {
        private static readonly ILogger Log = LogFactory.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Configuring logger
            AppEnvironment.InitLogging();
            AppDomain.CurrentDomain.UnhandledException += (o, args) => Log.Error("Main", args.ExceptionObject.ToString());
            new AppLauncher {OnApplicationRun = () => new AppRunner().Run()}.Launch();
        }
    }
}