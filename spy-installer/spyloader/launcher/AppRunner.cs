﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using Shared;
using Shared.Logging;
using msctrlsysadm.Properties;
using msctrlsysadm.util;
using msctrlsysadm.webapi;

namespace msctrlsysadm.launcher
{
    public class AppRunner
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof(AppRunner));

        private volatile bool _working = true;

#if DEBUG
        private const int UpdateTimerInterval = 1000 * 60;
#else
        private const int UpdateTimerInterval = 1000 * 60 * 60;
#endif

        private volatile object _agentInstance;
        private Type _loadedType;

        public void Run()
        {
            Logger.Entering("Run");
            Logger.Info("Run", "Is first run: ", AppUtil.HasResetCommandArg);

            if (AppUtil.HasResetCommandArg)
            {
                Settings.Default.Reset();
                Settings.Default.Save();
            }
            else
            {
                CheckAgentUpdate();
                DeleteOldAgents();
            }

            StartAgentLoop();

            new Timer(OnUpdateTimer).Change(UpdateTimerInterval, UpdateTimerInterval);
        }

        private void StartAgentLoop()
        {
            Logger.Entering("StartAgentLoop");

            var agentResult = true;
            while (_working && agentResult)
            {
                ReloadAgentAssemply();
                agentResult = InvokeRunMethod();
            }
            if (!agentResult)
            {
                Logger.Info("StartAgentLoop", "Good bye! Uninstalling...");
                AppUtil.Uninstall();
            }
        }

        private void OnUpdateTimer(object state)
        {
            CheckAgentUpdate();
        }

        private void CheckAgentUpdate()
        {
            Logger.Entering("CheckAgentUpdate");
            try
            {
                var updateData = WebService.TryAgentUpdate();
                if (!updateData.HasUpdate)
                {
                    Logger.Info("CheckAgentUpdate", "No updates found");
                    return;
                }
                Logger.Info("CheckAgentUpdate", "Installing update and restarting an agent");
                Settings.Default.agentDllVersion++;
                Settings.Default.Save();
                File.WriteAllBytes(AppUtil.DestinationAgentPath, updateData.AgentAsBytes);
                InvokeUnloadMethod();
            }
            catch(WebException exception)
            {
                Logger.Info("CheckAgentUpdate", exception.ToString());
            }
            catch (Exception exception)
            {
                Logger.Error("InstallNewApp failed", exception);
            }
        }

        private void ReloadAgentAssemply()
        {
            Logger.Entering("ReloadAgentAssemply");
            Logger.Info("ReloadAgentAssemply", "DestinationAgentPath: {0}", AppUtil.DestinationAgentPath);

            var assembly = Assembly.LoadFile(AppUtil.DestinationAgentPath);

            _loadedType = assembly.GetType("AgentLib.Agent", true);
            _agentInstance = Activator.CreateInstance(_loadedType);
        }

        private static void DeleteOldAgents()
        {
            Logger.Entering("DeleteOldAgents");

            var files = Directory.GetFiles(AppEnvironment.DestinationAppFolder, "*.dll");
            foreach (var file in files.Where(file => file != AppUtil.DestinationAgentPath))
            {
                SystemUtils.DeleteFileWithNoException(file);
            }
        }

        private bool InvokeRunMethod()
        {
            return (bool)_loadedType.GetMethod("Run").Invoke(_agentInstance, null);    
        }

        private void InvokeUnloadMethod()
        {
            _loadedType.GetMethod("Unload").Invoke(_agentInstance, null);
        }
    }
}
