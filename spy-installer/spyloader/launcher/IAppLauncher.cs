﻿namespace msctrlsysadm.launcher
{
    public interface IAppLauncher
    {
        /// <summary>
        /// Install or run current app
        /// </summary>
        void Launch();

        VoidCallback OnApplicationRun { get; set; }

        VoidCallback OnAppInstalledOk { get; set; }
    }

    public delegate void VoidCallback();
}
