﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Shared;
using Shared.Logging;
using Shared.Util;
using msctrlsysadm.Properties;
using msctrlsysadm.util;
using msctrlsysadm.webapi;
using msctrlsysadm.webapi.DTO;

namespace msctrlsysadm.launcher
{
    public class AppLauncher : IAppLauncher
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (AppLauncher));

        public VoidCallback OnApplicationRun { get; set; }

        public VoidCallback OnAppInstalledOk { get; set; }

        public void Launch()
        {
            Logger.Entering("Launch");

            LoadEnvironmentSettings();
            if (AppUtil.IsWeAreAlreadyInstalled)
            {
                RunApp();
            }
            else
            {
                InstallApp();
            }
        }

        private void RunApp()
        {
            Logger.Entering("RunApp");

            try
            {
                using (File.Create(AppUtil.DestinationLockerPath, 4096, FileOptions.DeleteOnClose))
                {
                    OnApplicationRun();
                }
            }
            catch(Exception exception)
            {
                Logger.Info("RunApp", "Seems that locker file is busy: {0}, {1}", AppUtil.DestinationLockerPath, exception);
            }
        }

        private void InstallApp()
        {
            Logger.Entering("InstallApp");

            if (!AppEnvironment.IsCertificateFileExists)
            {
                InstallNewApp();
            }
        }

        private void InstallNewApp()
        {
            Logger.Entering("InstallNewApp");

            var isInstalled = false;
            try
            {
                isInstalled = RegisterAndInstallNewApp();
                Logger.Info("InstallNewApp", "Installation result: {0}", isInstalled);
            }
            catch (WebException exception)
            {
                Logger.Warn("InstallNewApp", exception);
                MsgBox.ShowIfDebug(Resources.err_check_internet_connection);
            }
            catch (Exception exception)
            {
                Logger.Error("InstallNewApp failed", exception);
                MsgBox.ShowIfDebug(Resources.err_check_internet_connection);
            }
            finally
            {
                if (!isInstalled)
                {
                    Logger.Info("InstallNewApp", "Installation failed. Uninstalling");
                    AppUtil.Uninstall();
                }
            }
        }

        private static bool RegisterAndInstallNewApp()
        {
            Logger.Entering("RegisterAndInstallNewApp");

            var registerAppResult = WebService.RegisterNewApp(
                new RegisterNewAppDTO {InstallerId = AppEnvironment.InstallerId});
            Logger.Info("RegisterAndInstallNewApp", "State: {0}", registerAppResult.State);

            if (HandleRegisterResult(registerAppResult.State))
            {
                InstallAppFiles(registerAppResult);
                return true;
            }
            return false;
        }

        private static void InstallAppFiles(RegisterNewAppResult registerAppResult)
        {
            Logger.Entering("InstallAppFiles");

            // Reset the properties
            Settings.Default.Reset();
            Settings.Default.Save();

            // Making installation
            Directory.CreateDirectory(AppEnvironment.DestinationAppFolder);
            File.WriteAllBytes(AppUtil.DestinationExePath, File.ReadAllBytes(Application.ExecutablePath));
            AppEnvironment.Certificate = registerAppResult.Certificate;
            File.WriteAllBytes(AppUtil.DestinationAgentPath, registerAppResult.AgentAsBytes);
            SystemUtils.AddToSystemStartup(AppEnvironment.StartupRegKey, AppUtil.DestinationExePath);

            // Coping libs
            var dirName = new FileInfo(Application.ExecutablePath).DirectoryName;
            File.Copy(dirName + @"\Shared.dll", AppEnvironment.DestinationAppFolder + @"\Shared.dll");

            SystemUtils.StartExternalProgram(AppUtil.DestinationExePath, AppEnvironment.ResetFlag);
        }

        private static void LoadEnvironmentSettings()
        {
            Logger.Entering("LoadEnvironmentSettings");

            AppEnvironment.IsShowingErrorsMode = !AppUtil.IsWeAreAlreadyInstalled || AppUtil.HasResetCommandArg;
        }

        private static bool HandleRegisterResult(RegistrationStatus state)
        {
            switch (state)
                {
                    case RegistrationStatus.OK:
                        return true;
                    case RegistrationStatus.ALREADY_REGISTERED:
                        MsgBox.ShowIfDebug(Resources.Err_InstallerIdAreadyActivated);
                        return false;
                    case RegistrationStatus.NOT_FOUND:
                        MsgBox.ShowIfDebug(Resources.Err_InstallerNotFound);
                        return false;
                    default:
                        MsgBox.ShowIfDebug(Resources.Err_InstallationFailedWithUnknownReason);
                        Logger.Error("HandleRegisterResult", "InstallWithExistingCertificate failed. Unhandled result state: " + state);
                        return false;
                }
        }
    }
}
