﻿using System;
using System.Windows.Forms;
using System.Linq;
using Shared;

namespace msctrlsysadm.util
{
    public static class AppUtil
    {
        public static void Uninstall()
        {
            SystemUtils.DeleteFilesWithNoException(DestinationExePath, AppEnvironment.DestinationCertificatePath,
                        DestinationAgentPath);
            SystemUtils.RemoveFromStartup(AppEnvironment.StartupRegKey);
        }

        public static bool HasResetCommandArg
        {
            get { return Environment.GetCommandLineArgs().Contains(AppEnvironment.ResetFlag); }
        }

        public static bool IsWeAreAlreadyInstalled
        {
            get { return string.Compare(Application.ExecutablePath, DestinationExePath, StringComparison.OrdinalIgnoreCase) == 0; }
        }

        public static string DestinationLockerPath
        {
            get { return AppEnvironment.DestinationAppFolder + "locker.lock"; }
        }

        public static string DestinationAgentPath
        {
            get { return AppEnvironment.DestinationAppFolder + "agent_" + Properties.Settings.Default.agentDllVersion + ".dll"; }
        }

        public static string DestinationExePath
        {
            get { return AppEnvironment.DestinationAppFolder + "msloader.exe"; }
        }
    }
}
