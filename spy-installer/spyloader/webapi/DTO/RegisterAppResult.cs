﻿

namespace msctrlsysadm.webapi.DTO
{
    public class RegisterAppResult
    {
        public RegResultState State { get; set; }

        public string Certificate { get; set; }
    }

    public enum RegResultState
    {
        OK, ALREADY_REGISTERED, INSTALLER_ID_NOT_FOND
    }
}
