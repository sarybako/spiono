﻿using System;
using System.Web.Script.Serialization;

namespace msctrlsysadm.webapi.DTO
{
    public class RegisterNewAppResult
    {
        public RegistrationStatus State { get; set; }

        public string Certificate { get; set; }

        public string Agent { get; set; }

        [ScriptIgnore]
        public byte[] AgentAsBytes
        {
            get
            {
                return Convert.FromBase64String(Agent);
            }
        }
    }
}
