﻿
using System;
using Shared;
using Shared.DTO;
using msctrlsysadm.webapi.DTO;

namespace msctrlsysadm.webapi
{
    public static class WebService
    {
        private const string ServiceUrl = AppEnvironment.InstallerServiceUrl;

        public static RegisterNewAppResult RegisterNewApp(RegisterNewAppDTO dto)
        {
            return WebUtils.PostAndGetJson<RegisterNewAppResult>(ServiceUrl + "registerNewApp/", dto); 
        }

        public static RegistrationStatus RegisterOldApp(RegisterOldAppDTO dto)
        {
            return WebUtils.PostAndGetJson<RegistrationStatus>(ServiceUrl + "registerOldApp/", dto);
        }

        public static UpdateAgentResult TryAgentUpdate()
        {
            return WebUtils.PostAndGetJson<UpdateAgentResult>(ServiceUrl + "upateAgent/", 
                new CertificatedDTO{Certificate = AppEnvironment.Certificate});
        }
    }
}
