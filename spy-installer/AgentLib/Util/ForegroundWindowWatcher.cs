﻿

using System;
using System.Diagnostics;
using System.Threading;
using Shared.Logging;

namespace AgentLib.Util
{
    class ForegroundWindowWatcher : IDisposable
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (ForegroundWindowWatcher));

        private IntPtr _handle = IntPtr.Zero;

        private string _text;

        private string _processName;

        private readonly Timer _timer;

        private event OnForegroundWindowChanged _onForegroundWindowChanged;

        public event OnForegroundWindowChanged OnForegroundWindowChanged
        {
            add { _onForegroundWindowChanged += value; }
            remove { _onForegroundWindowChanged -= value; }
        }

        public ForegroundWindowWatcher()
        {
            Logger.Entering("ForegroundWindowWatcher");

            _timer = new Timer(state => UpdateCurrentForegroundWindow());
            _timer.Change(1000, 1000);
        }

        public IntPtr Handle
        {
            get
            {
                EnsureHanleExists();
                return _handle;
            }
            set { SetHandle(value); }
        }

        public string Text
        {
            get
            {
                _text = _text ?? Win32Api.GetWindowText(Handle);
                return _text;
            }
        }

        public string ProcessName
        {
            get
            {
                if (_processName != null) return _processName;
                foreach (var process in Process.GetProcesses())
                {
                    if (process.MainWindowHandle == Handle)
                    {
                        _processName = process.ProcessName;
                        break;
                    }
                }
                return _processName;
            }
        }

        public string GetFocusedControlText()
        {
            string result = null;
            var remoteThreadId = Win32Api.GetWindowThreadProcessId(Handle.ToInt32(), 0);
            var currentThreadId = Win32Api.GetCurrentThreadId();
            var attached = Win32Api.AttachThreadInput(currentThreadId, remoteThreadId, true);
            if (attached > 0)
            {
                result = Win32Api.GetWindowText(Win32Api.GetFocus());
                Win32Api.AttachThreadInput(currentThreadId, remoteThreadId, false);
            }
            return result;
        }

        private void EnsureHanleExists()
        {
            if (_handle == IntPtr.Zero) UpdateCurrentForegroundWindow();
        }

        public void UpdateCurrentForegroundWindow()
        {
            SetHandle(Win32Api.GetForegroundWindow());
        }

        private void SetHandle(IntPtr handle)
        {
            var notInitlized = _handle == IntPtr.Zero;
            _handle = handle;

            _text = null;
            _processName = null;
            if (!notInitlized && _onForegroundWindowChanged != null) _onForegroundWindowChanged.Invoke(_handle, this);
        }

        public void Dispose()
        {
            Logger.Entering("Dispose");
            _timer.Dispose();
        }
    }

    internal delegate void OnForegroundWindowChanged(IntPtr handle, ForegroundWindowWatcher watcher);
}
