﻿using System;

namespace AgentLib.Util
{
    public static class DateTimeExt
    {

        public static long UtcNowInUnixTime
        {
            get
            {
                return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds; 
            }
        }
    }
}
