﻿using System.Collections.Generic;

namespace AgentLib.Util.DTO
{
    public class StatisticsLogDTO
    {
        public IList<StatisticsDataDTO> StatisticsData { get; set; }
        public string Certificate { get; set; }
    }
}
