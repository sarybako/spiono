﻿using System;
using System.Windows.Forms;
using AgentLib.Util;
using AgentLib.Util.DTO;
using Shared.Logging;
using Shared;

namespace AgentLib.Spy.Statistics
{
    public class StatisticsSpy : IDisposable
    {
        private const int ExplicitSendInterval = 60 * 15; // Every 15 min

        private static readonly ILogger Logger = LogFactory.GetLogger(typeof(StatisticsSpy));

        private readonly Timer _timer = new Timer { Interval = 1000, Enabled = true };

        private readonly StatisticsWin32API _statisticsWin32Api = new StatisticsWin32APIImpl();

        private readonly CurrentProcessHolder _currentProcessHolder;

        private int _sendIntervalTick = 0;

        public StatisticsSpy()
        {
            Logger.Entering("StatisticsSpy()");

            _currentProcessHolder = new CurrentProcessHolder(_statisticsWin32Api);    
        }

        public void Start()
        {
            Logger.Entering("Start()");

            _timer.Tick += OnTimerTick;
            _currentProcessHolder.CurrentProcessChangedEvent += SendData;
            _sendIntervalTick = 0;
        }

        public void Dispose()
        {
            _currentProcessHolder.RaiseCurrentProcessInfoAndStartRecordingNew();

            _timer.Tick -= OnTimerTick;
            _currentProcessHolder.CurrentProcessChangedEvent -= SendData;

            _timer.DisposeQuitely();
            _sendIntervalTick = 0;
        }

        void OnTimerTick(object sender, EventArgs e)
        {
            _currentProcessHolder.UpdateCurrentProcessInfo();
            if (_sendIntervalTick > ExplicitSendInterval)
            {
                _currentProcessHolder.RaiseCurrentProcessInfoAndStartRecordingNew();
            }
        }

        private void SendData(StatisticsActiveProcessInfo processInfo, long startDate)
        {
            try
            {
                var statistics = new StatisticsDataDTO
                                     {
                                         ProcessName = processInfo.Name,
                                         ProcessDescription = processInfo.Description,
                                         StartDate = startDate,
                                         EndDate = DateTimeExt.UtcNowInUnixTime,
                                     };
#if DEBUG
                Logger.Info("SendData", "{0} {1} {2} {3}", processInfo.Name, processInfo.Description, startDate, DateTimeExt.UtcNowInUnixTime);
#endif
                WebService.SendStatistics(statistics); 
            }
            catch(Exception e)
            {
                Logger.Error("SendData", e);
            }
            finally
            {
                _sendIntervalTick = 0;
            }
        }
    }
}
