﻿

using System.Diagnostics;
using AgentLib.Util;

namespace AgentLib.Spy.Statistics
{
    public interface StatisticsWin32API
    {
        StatisticsActiveProcessInfo GetActiveProcessInfo();
    }

    public class StatisticsWin32APIImpl : StatisticsWin32API
    {
        public StatisticsActiveProcessInfo GetActiveProcessInfo()
        {
            var result = new StatisticsActiveProcessInfo();
            var process = Win32Api.GetForegroundAppProcess();
            if (process != null)
            {
                FillResult(ref result, process);
            }
            return result;
        }

        private void FillResult(ref StatisticsActiveProcessInfo info, Process process)
        {
            info.Name = process.ProcessName; 
            try
            {
                info.Description = process.MainModule.FileVersionInfo.FileDescription;
            }
            catch
            {   // Have no permissions. Just do nothing
            }
        }
    }

    public struct StatisticsActiveProcessInfo
    {
        public string Name;
        public string Description;

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var o = (StatisticsActiveProcessInfo) obj;
            return Name == o.Name && Description == o.Description;
        }
    }
}
