﻿using System;
using AgentLib.Util;


namespace AgentLib.Spy.Statistics
{
    public delegate void CurrentProcessChangedEvent(StatisticsActiveProcessInfo processInfo, long timeStarted);

    public class CurrentProcessHolder
    {
        private readonly StatisticsWin32API _win32Api;
        private StatisticsActiveProcessInfo _currentProcessInfo;
        private long _currentProceessStartTime;
        private event CurrentProcessChangedEvent _currentProcessChangedEvent;

        public event CurrentProcessChangedEvent CurrentProcessChangedEvent
        {
            add { _currentProcessChangedEvent += value; }
            remove { _currentProcessChangedEvent -= value; }
        }

        public CurrentProcessHolder(StatisticsWin32API win32Api)
        {
            _win32Api = win32Api;
            StartRecordingCurrentProcessInfo(win32Api.GetActiveProcessInfo());
        }

        public void UpdateCurrentProcessInfo()
        {
            var newProcessInfo = _win32Api.GetActiveProcessInfo();
            if (!newProcessInfo.Equals(_currentProcessInfo))
            {
                RaiseCurrentProcessInfoAndStartRecordingNew(newProcessInfo);
            }
        }

        public void RaiseCurrentProcessInfoAndStartRecordingNew()
        {
            RaiseCurrentProcessInfoAndStartRecordingNew(_win32Api.GetActiveProcessInfo());
        }

        private void RaiseCurrentProcessInfoAndStartRecordingNew(StatisticsActiveProcessInfo newProcessInfo)
        {
            try
            {
                if (_currentProcessChangedEvent != null)
                {
                    _currentProcessChangedEvent(_currentProcessInfo, _currentProceessStartTime);
                }
            }    
            finally
            {
                StartRecordingCurrentProcessInfo(newProcessInfo);
            }
        }

        private void StartRecordingCurrentProcessInfo(StatisticsActiveProcessInfo processInfo)
        {
            _currentProcessInfo = processInfo;
            _currentProceessStartTime = DateTimeExt.UtcNowInUnixTime;
        }
    }
}
