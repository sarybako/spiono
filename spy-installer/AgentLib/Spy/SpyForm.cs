﻿
using System.Drawing;
using System.Windows.Forms;
using Shared.Util;

namespace AgentLib.Spy
{
    public class SpyForm
    {
        private readonly Form _form = new Form
                                          {
                                              Opacity = 0, 
                                              Size = new Size(0, 0),
                                              Visible = false,
                                              ShowInTaskbar = false,
                                              FormBorderStyle = FormBorderStyle.SizableToolWindow
                                          };

        private event Delegates.VoidDelegate _onStartSpyServices;

        private event Delegates.VoidDelegate _onStopSpyServices;

        public event Delegates.VoidDelegate OnStartSpyServices
        {
            add { _onStartSpyServices += value; }
            remove { _onStartSpyServices -= value; }
        }

        public event Delegates.VoidDelegate OnStopSpyServices
        {
            add { _onStopSpyServices += value; }
            remove { _onStopSpyServices -= value; }
        }

        public SpyForm()
        {
            _form.Shown += (formSender, formArgs) =>
                               {
                                   if (_onStartSpyServices != null) _onStartSpyServices.Invoke();
                               };

            _form.Closing += (formSender, formArgs) =>
                                 {
                                     if (_onStopSpyServices != null) _onStopSpyServices.Invoke();
                                 };
        }

        public void StartSpyForm()
        {
            _form.ShowDialog();
        }

        public void StopSpyForm()
        {
            _form.Invoke(new Delegates.VoidDelegate(() => _form.Close()));
        }

        public Control UiThreadControl
        {
            get { return _form; }
        }
    }
}
