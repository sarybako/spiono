﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shared.Util
{
    public static class StringUtil
    {
        public static string Crop(this string @string, int maxLength)
        {
            if (@string == null) return null;
            if (maxLength < 4) throw new ArgumentException("Argument maxLength can not be less than 4");
            return @string.Length <= maxLength ? @string : @string.Substring(0, maxLength - 3) + "...";
        }
    }
}
