﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using Shared.Logging;

namespace Shared
{
    public static class SystemUtils
    {
        private static readonly ILogger Logger = LogFactory.GetLogger(typeof (SystemUtils));

        public const string RegistryStartupPath = @"Software\Microsoft\Windows\CurrentVersion\Run";

        public static bool AddToSystemStartup(string keyName, string path)
        {
            var key = Registry.CurrentUser.OpenSubKey(RegistryStartupPath, true);
            if (key == null)
            {
                return false;
            }
            key.SetValue(keyName, path);
            return true;
        }

        public static bool RemoveFromStartup(string keyName)
        {
            var key = Registry.CurrentUser.OpenSubKey(RegistryStartupPath, true);
            if (key == null)
            {
                return false;
            }
            key.DeleteValue(keyName, false);
            return true;
        }

        public static int DeleteFilesWithNoException(params string[] names)
        {
            return names.Count(DeleteFileWithNoException);
        }

        public static bool DeleteFileWithNoException(string name)
        {
            try
            {
                File.Delete(name);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void StartExternalProgram(string path, string commandLineArg)
        {
            new Process {StartInfo = {FileName = path, Arguments = commandLineArg}}.Start();
        }

        public static void DisposeQuitely(this IDisposable disposable)
        {
            try
            {
                if (disposable != null) disposable.Dispose();
            }
            catch(Exception e)
            {
                Logger.Info("DisposeQuitely", "The disposition exception occured: {0}", e);
            }
        }
    }
}
