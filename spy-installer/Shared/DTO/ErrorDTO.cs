﻿

namespace Shared.DTO
{
    public class ErrorDTO
    {
        public string Message { get; set; }

        public string InstallerId { get; set; }

        public string Certificate { get; set; }

        public string LogFileTail { get; set; }

        public bool IsShowingErrorsMode { get; set; }
    }
}
