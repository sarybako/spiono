﻿

using System;

namespace Shared.Logging
{
    public interface ILogger
    {
        event OnLogEvent OnLog;

        void Error(string method, string description);

        void Error(string method, Exception exception);

        void Warn(string method, Exception exception);

        void Info(string method, string format, params object[] args);

        void Info(string method, string message);

        void Entering(string method);

        void Log(string method, LogLevel level, string format, params object[] args);

        void Log(string method, LogLevel level, string message);
    }

    public interface ILogMessage
    {
        Type Type { get; set; }

        string Method { get; set; }

        LogLevel Level { get; set; }

        string Message { get; set; }

        string FormatMessage();
    }

    public delegate void OnLogEvent(ILogMessage logMessage);
}
