﻿using System;

namespace Shared.Logging
{
    class Logger : ILogger
    {
        private readonly Type _type;

        public Logger(Type type)
        {
            _type = type;
        }

        private event OnLogEvent _onLog;

        public event OnLogEvent OnLog { add { _onLog += value; } remove { _onLog -= value; } }

        public void Error(string method, string description)
        {
            Log(method, LogLevel.Error, description);
        }

        public void Error(string method, Exception exception)
        {
            Log(method, LogLevel.Error, exception.ToString());
        }

        public void Warn(string method, Exception exception)
        {
            Log(method, LogLevel.Warning, exception.ToString());
        }

        public void Info(string method, string format, params object[] args)
        {
#if DEBUG
            Log(method, LogLevel.Info, format, args);
#endif
        }

        public void Info(string method, string message)
        {
#if DEBUG
            Log(method, LogLevel.Info, message);
#endif
        }

        public void Entering(string method)
        {
#if DEBUG
            Log(method, LogLevel.Info, "Entering");
#endif
        }

        public void Log(string method, LogLevel level, string format, params object[] args)
        {
            Log(method, level, string.Format(format, args));
        }

        public void Log(string method, LogLevel level, string message)
        {
            if (_onLog != null)
            {
                _onLog(new LogMessage{ Level = level, Message = message, Method = method, Type = _type});
            }
        }
    }

    class LogMessage : ILogMessage
    {
        public Type Type { get; set; }

        public string Method { get; set; }

        public LogLevel Level { get; set; }

        public string Message { get; set; }

        public string FormatMessage()
        {
            return string.Format("{0} {1}\t{2}.{3}: {4}", DateTime.Now.ToLongTimeString(), Level, Type, Method, Message);
        }
    }
}
