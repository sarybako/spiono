package com.volvo.platform.spring;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BCryptUtilTest {

    @Test
    public void bcryptHashPositiveTest() throws Exception {
        String actualHash = BCryptUtil.bcryptHash("password");
        assertTrue(BCrypt.checkpw("password", actualHash));
    }

    @Test
    public void bcryptHashNegativeTest() throws Exception {
        String actualHash = BCryptUtil.bcryptHash("password_1");
        assertFalse(BCrypt.checkpw("password", actualHash));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void bcryptHashNPETest() {
        BCryptUtil.bcryptHash(null);
    }
}
