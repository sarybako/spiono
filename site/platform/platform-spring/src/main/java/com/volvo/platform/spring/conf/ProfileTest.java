package com.volvo.platform.spring.conf;


import org.springframework.context.annotation.Profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.volvo.platform.spring.conf.ProfileNames.TESTING;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Profile(TESTING)
public @interface ProfileTest {
    String name = ProfileNames.TESTING;
}
