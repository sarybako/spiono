package com.volvo.platform.spring;

import org.springframework.security.crypto.bcrypt.BCrypt;

import static com.google.common.base.Preconditions.checkNotNull;

public class BCryptUtil {

    public static String bcryptHash(String password) {
        return BCrypt.hashpw(checkNotNull(password), BCrypt.gensalt());
    }
}