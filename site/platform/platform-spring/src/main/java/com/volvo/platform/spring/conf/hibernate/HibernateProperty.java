package com.volvo.platform.spring.conf.hibernate;


import java.util.Properties;

public enum HibernateProperty {

    Dialect("hibernate.dialect"),
    FormatSQL("hibernate.format_sql"),
    NamingStrategy("hibernate.ejb.naming_strategy"),
    ShowSQL("hibernate.show_sql"),
    SearchProvider("hibernate.search.default.directory_provider"),
    SearchDir("hibernate.search.default.indexBase");

    private final String name;

    public static Properties addHibernateProperty(Properties properties, HibernateProperty property, String value) {
        properties.put(property.name, value);
        return properties;
    }

    private HibernateProperty(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
