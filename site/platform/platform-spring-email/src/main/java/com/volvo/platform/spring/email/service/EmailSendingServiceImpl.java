package com.volvo.platform.spring.email.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static org.springframework.util.Assert.notNull;


@Service
@Slf4j
class EmailSendingServiceImpl implements EmailSendingService, ApplicationEventPublisherAware {

    @Autowired
    private MailSender mailSender;

    private ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    @Override
    public void send(SimpleMailMessage message) {
        doSend(message);
    }

    @Async
    @Override
    public void sendAsync(SimpleMailMessage message) {
        doSend(message);
    }

    private void doSend(SimpleMailMessage message) {
        notNull(message);
        publisher.publishEvent(new SimpleMailSendEvent(this, message));
        mailSender.send(message);
    }
}
