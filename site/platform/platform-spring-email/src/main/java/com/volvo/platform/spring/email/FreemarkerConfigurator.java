package com.volvo.platform.spring.email;

import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.ui.freemarker.SpringTemplateLoader;

import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FreemarkerConfigurator {

    public static Configuration createWithDefaultObjectWrapper(String freemarkerClassPath) throws IOException {
        freemarker.template.Configuration configuration = new freemarker.template.Configuration();
	    configuration.setDefaultEncoding("UTF-8");
	    configuration.setObjectWrapper(new DefaultObjectWrapper());
        ResourceLoader resourceLoader = new FileSystemResourceLoader();
        TemplateLoader templateLoader = new SpringTemplateLoader(resourceLoader, freemarkerClassPath);
        configuration.setTemplateLoader(templateLoader);
        return configuration;
    }
}
