package com.volvo.platform.spring.email;

import freemarker.template.TemplateModelException;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

import static com.volvo.platform.spring.email.FreemarkerConfigurator.createWithDefaultObjectWrapper;

public abstract class FreemarkerEmalingConfigBase {

    @Bean
    public freemarker.template.Configuration freemarkerConfiguration() throws IOException, TemplateModelException {
        return createWithDefaultObjectWrapper(getTemplatesPackageClasspath());
    }

    protected abstract String getTemplatesPackageClasspath();
}
