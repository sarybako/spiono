package com.volvo.platform.spring.email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import static org.springframework.util.ReflectionUtils.rethrowRuntimeException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FreemarkerUtils {

    public static FreemarkerEmailTemplate loadEmailTemplate(Configuration config, EmailLetter emailLetter, Locale locale) {
        Template subject = loadTemplateQuietly(config, emailLetter.getSubjectPath(), locale);
        Template body = loadTemplateQuietly(config, emailLetter.getBodyPath(), locale);
        return new FreemarkerEmailTemplate(subject, body);
    }

    public static Template loadTemplateQuietly(Configuration config, String template, Locale locale) {
        try {
            return config.getTemplate(template, locale);
        } catch (IOException e) {
            rethrowRuntimeException(e);
            return null;
        }
    }

    public static String processToStringQuietly(Object rootMap, Template template) {
        try {
            return processToString(rootMap, template);
        } catch (IOException e) {
            rethrowRuntimeException(e);
        } catch (TemplateException e) {
            rethrowRuntimeException(e);
        }
        return null;
    }

    public static String processToString(Object rootMap, Template template) throws IOException, TemplateException {
        Writer out = new StringWriter();
        template.process(rootMap, out);
        return out.toString();
    }
}
