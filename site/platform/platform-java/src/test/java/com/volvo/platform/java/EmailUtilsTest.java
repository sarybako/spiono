package com.volvo.platform.java;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EmailUtilsTest {

    @DataProvider(name = "positiveTestData")
    public static Object[][] positiveTestData() {
        return new Object[][]{
            { "gcc@bk.ru", "bk.ru" },
            { "gcc@bk.ru@gmail.com", "gmail.com" },
            { "@bk.ru", "bk.ru" },
            { "gcc@", "" },
            { "@", "" },
        };
    }

    @Test(dataProvider = "positiveTestData")
    public void getEmailDomainPartPositiveTest(String input, String expectedOutput) {
        assertEquals(EmailUtils.getEmailDomainPart(input), expectedOutput);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getEmailDomainPartNPETest() {
        EmailUtils.getEmailDomainPart(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void getEmailDomainPartIllegalArgTest() {
        EmailUtils.getEmailDomainPart("hello");
    }

    @Test(dataProvider = "positiveTestData")
    public void getEmailSitenamePositiveTest(String input, String expectedOutput) {
        assertEquals(EmailUtils.getEmailSitename(input), "http://www." + expectedOutput);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getEmailSitenameNPETest() {
        EmailUtils.getEmailSitename(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void getEmailSitenameIllegalArgTest() {
        EmailUtils.getEmailSitename("hello");
    }
}
