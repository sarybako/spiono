package com.volvo.platform.java;

import com.google.common.base.Function;
import org.testng.annotations.Test;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CollectionsTransformerTest {
    private final static String[] fromArray = {"1", "2", "3"};
    private final static List<String> fromList = Arrays.asList(fromArray);

    private final static Function<String, Integer> strToIntConverter = new Function<String, Integer>() {
        @Nullable
        @Override
        public Integer apply(@Nullable String str) {
            return Integer.parseInt(str);
        }
    };

    @Test
    public void transformToArrayPositiveTest() {
        Integer[] result = CollectionsTransformer.transformToArray(fromArray, new Integer[3], strToIntConverter);
        assertEquals(result.length, 3);
        assertEquals(result[0], Integer.valueOf(1));
        assertEquals(result[1], Integer.valueOf(2));
        assertEquals(result[2], Integer.valueOf(3));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowNpeIfFromArrayIsNull() {
        CollectionsTransformer.transformToArray(null, new Integer[3], strToIntConverter);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowNpeIfDestArrayIsNull() {
        CollectionsTransformer.transformToArray(fromArray, null, strToIntConverter);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowNpeIfConverterIsNull() {
        CollectionsTransformer.transformToArray(fromArray, new Integer[3], null);
    }

    @Test
    public void shouldWorkWellIfDestArrayLargerThatInputArray() {
        Integer[] result = CollectionsTransformer.transformToArray(fromArray, new Integer[fromArray.length + 1],
                strToIntConverter);
        assertEquals(result.length, 4);
        assertEquals(result[0], Integer.valueOf(1));
        assertEquals(result[1], Integer.valueOf(2));
        assertEquals(result[2], Integer.valueOf(3));
        assertEquals(result[3], null);
    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    public void shoultThrowIndexOfBoundIfDestArraySmallerThatInputArray() {
        CollectionsTransformer.transformToArray(fromArray, new Integer[fromArray.length -1], strToIntConverter);
    }

    @Test
    public void shouldDoNothingWithEmptyArray() {
        Integer[] result = CollectionsTransformer.transformToArray(new String[0], new Integer[0], strToIntConverter);
        assertEquals(result.length, 0);
    }

    @Test
    public void transformToListPositiveTest() {
        List<Integer> result = CollectionsTransformer.transformToList(fromList, strToIntConverter);
        assertEquals(result.size(), 3);
        assertEquals(result.get(0), Integer.valueOf(1));
        assertEquals(result.get(1), Integer.valueOf(2));
        assertEquals(result.get(2), Integer.valueOf(3));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldTransformToListThrowNpeIfSrcListIsNull() {
        CollectionsTransformer.transformToList(null, strToIntConverter);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldTransformToListThrowNpeIfConverterIsNull() {
        CollectionsTransformer.transformToList(fromList, null);
    }

    @Test
    public void shouldTransformToListConvertEmptyListWell() {
        List<Integer> result = CollectionsTransformer.transformToList(Collections.<String>emptyList(), strToIntConverter);
        assertTrue(result.isEmpty());
    }
}
