package com.volvo.platform.java;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ArrayIterableTest {

    private static final Integer[] testArray = new Integer[]{0, 1};

    @Test
    public void shouldConstructorStoreArray() {
        ArrayIterable<Object> iterable = new ArrayIterable<Object>(testArray);
        assertSame(iterable.array, testArray);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowNpeWhenConstructorStoresNullArray() {
        ArrayIterable<Object> iterable = new ArrayIterable<Object>(null);
        assertNull(iterable.array);
    }

    @Test
    public void iteratorPositiveTest() {
        ArrayIterable<Integer> iterable = new ArrayIterable<Integer>(testArray);

        Integer count = 0;
        for (Integer integer : iterable) {
            assertEquals(integer, count);
            ++count;
        }
        assertEquals(count, Integer.valueOf(2));
    }
}
