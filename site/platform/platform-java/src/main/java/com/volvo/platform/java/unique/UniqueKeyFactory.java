package com.volvo.platform.java.unique;

import com.volvo.platform.java.unique.UUID.UUIDKeyGenerator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.NONE)
public class UniqueKeyFactory {

    public static UniqueKeyGenerator<String> asStringGenerator(final UniqueKeyGenerator<?> generator) {
        return new UniqueKeyGenerator<String>() {
            @Override
            public String generateNew() {
                return String.valueOf(generator.generateNew());
            }
        };
    }

    public static UniqueKeyGenerator<UUID> uuidKeyGenerator() {
        return new UUIDKeyGenerator();
    }

    public static <TKeyType> UniqueKeyGenerator<TKeyType> uniqueKeyGenerator(final UniqueKeyGenerator<TKeyType> generator, final UniqueKeyChecker<TKeyType> keyChecker) {
        return new UniqueKeyGenerator<TKeyType>() {
            @Override
            public TKeyType generateNew() {
                TKeyType result;
                do {
                    result = generator.generateNew();
                } while (!keyChecker.isKeyUnique(result));
                return result;
            }
        };
    }

    public static<TKeyType> TKeyType generateNew(UniqueKeyGenerator<TKeyType> generator) {
        return generator.generateNew();
    }
}

