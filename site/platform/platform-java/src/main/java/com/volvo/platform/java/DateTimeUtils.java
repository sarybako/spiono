package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.joda.time.DateTime.now;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class DateTimeUtils {

    public static Date nowMinusDays(int days) {
        return now().minusDays(days).toDate();
    }

    public static Date shiftToTheEndOfDay(Date date){
        checkNotNull(date);
        return new DateTime(date).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
    }

    public static Date shiftToBeginOfDay(Date date){
        checkNotNull(date);
        return new DateTime(date).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate();
    }
}
