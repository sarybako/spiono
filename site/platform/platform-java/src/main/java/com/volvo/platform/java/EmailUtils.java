package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class EmailUtils {

    public static String getEmailSitename(String email) {
        return "http://www." + getEmailDomainPart(email);
    }

    public static String getEmailDomainPart(String email) {
        int doggyIndex = email.lastIndexOf("@");
        if (doggyIndex == -1) {
            throw new IllegalArgumentException("The symbol @ is not found in email");
        }
        return email.substring(doggyIndex + 1);
    }
}
