package com.volvo.platform.java;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.io.Files.toByteArray;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class IOUtils {
    public static final String DIR_SEPARATOR_UNIX = String.valueOf(org.apache.commons.io.IOUtils.DIR_SEPARATOR_UNIX);

    public static String streamToUtf8String(InputStream stream) throws IOException {
        return org.apache.commons.io.IOUtils.toString(stream, UTF_8.name());
    }

    public static void writeStringAsUtf8ToStream(String string, OutputStream stream) throws IOException {
        stream.write(stringToUtf8Bytes(string));
    }

    public static byte[] stringToUtf8Bytes(String string) {
        return string.getBytes(UTF_8);
    }

    public static String fileToBase64String(String fileName) throws IOException {
        return fileToBase64String(new File(fileName));
    }

    public static String fileToBase64String(File file) throws IOException {
        return bytesToBase64String(readFileToByteArray(file));
    }

    public static byte[] readFileToByteArray(String fileUrl) throws IOException {
        return readFileToByteArray(new File(fileUrl));
    }

    public static byte[] readFileToByteArray(File file) throws IOException {
        return toByteArray(file);
    }

    public static String bytesToBase64String(byte[] bytes) {
        return new Base64().encodeToString(bytes);
    }
}
