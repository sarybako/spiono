package com.volvo.platform.java.unique;

public interface UniqueKeyGenerator<TKeyType> {

    TKeyType generateNew();
}
