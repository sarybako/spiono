package com.volvo.platform.java;

import com.google.common.base.Function;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Lists.newArrayListWithCapacity;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class CollectionsTransformer {

    public static <TO, FROM> TO[] transformToArray(FROM[] from, TO[] to, Function<FROM, TO> converter) {
        for (int j = 0; j < from.length; ++j) {
            to[j] = converter.apply(from[j]);
        }
        return to;
    }

    public static <TO, FROM> List<TO> transformToList(Collection<FROM> from, Function<FROM, TO> converter) {
        List<TO> result = newArrayListWithCapacity(from.size());
        for (FROM fromItem: from) {
            result.add(converter.apply(fromItem));
        }
        return result;
    }
}
