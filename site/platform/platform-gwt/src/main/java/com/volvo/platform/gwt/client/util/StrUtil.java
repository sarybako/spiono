package com.volvo.platform.gwt.client.util;


public final class StrUtil {

	private StrUtil() {
	}

    public static String defaultIfBlank(String s, String def) {
        return nullOrEmpty(s) ? def : s;
    }

	public static boolean nullOrEmpty(String s) {
		return s == null || s.isEmpty();
	}

	public static char[] getDigits(String s) {
		if (nullOrEmpty(s)) {
			return new char[0];
		}

		int length = s.length();
		char[] buffer = new char[length];
		int position = 0;
		for (int j = 0; j < length; ++j) {
			char c = s.charAt(j);
			if (Character.isDigit(c)) {
				buffer[position++] = c;
			}
		}

		char[] result = new char[position];
		for (int j = 0; j < position; ++j) {
			result[j] = buffer[j];
		}

		return result;
	}
}
