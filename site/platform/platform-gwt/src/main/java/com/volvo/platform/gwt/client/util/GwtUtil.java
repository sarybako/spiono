package com.volvo.platform.gwt.client.util;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import java.util.Date;

import static com.google.gwt.core.client.GWT.getHostPageBaseURL;

public final class GwtUtil {
    private static final long ONE_DAY_IN_MS = 24 * 60 * 60 * 1000;

    private GwtUtil() {}

    public static String getFullPath(String relativePath) {
        return getHostPageBaseURL() + relativePath;
    }

    public static void addOneDayToDate(Date date) {
        date.setTime(date.getTime() + ONE_DAY_IN_MS);
    }


    public static HandlerRegistration callActivityOnStopWhenOnPlaceChangeEvent(final Activity activity, final EventBus eventBus) {
        HandlerRegistration handlerRegistration = eventBus.addHandler(PlaceChangeEvent.TYPE, new PlaceChangeEvent.Handler() {
            @Override
            public void onPlaceChange(PlaceChangeEvent event) {
                activity.onStop();
            }
        });
        return handlerRegistration;
    }
}
