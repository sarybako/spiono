package com.volvo.platform.gwt.shared.remerror;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

public interface RemoteErrorBeanFactory extends AutoBeanFactory {

    AutoBean<RemoteErrorDTO> remoteErrorBean();
}
