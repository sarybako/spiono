package com.volvo.platform.gwt.client.http;

public interface HttpRequestCallback extends HttpRequestFailureCallback, HttpRequestSuccessCallback {

}
