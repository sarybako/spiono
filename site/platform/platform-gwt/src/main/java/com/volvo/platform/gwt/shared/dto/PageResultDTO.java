package com.volvo.platform.gwt.shared.dto;

public interface PageResultDTO extends PageableDTO, GwtDTO {

    void setTotalPages(int totalPages);
    int getTotalPages();

    void setNumberOfElements(int numberOfElements);
    int getNumberOfElements();

    void setTotalElements(long totalElements);
    long getTotalElements();

    void setHasPreviousPage(boolean hasPreviousPage);
    boolean isHasPreviousPage();

    void setHasNextPage(boolean hasNextPage);
    boolean isHasNextPage();

    void setIsFirstPage(boolean isFirstPage);
    boolean getIsFirstPage();

    void setIsLastPage(boolean isLastPage);
    boolean getIsLastPage();
}
