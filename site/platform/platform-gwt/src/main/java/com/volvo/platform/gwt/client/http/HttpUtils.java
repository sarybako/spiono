package com.volvo.platform.gwt.client.http;

import com.google.gwt.http.client.*;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

public final class HttpUtils {

    public static final int HTTP_TIMEOUT = 1000 * 30;

    private HttpUtils() {}

    public static boolean responseAsBoolean(Response response) {
        String text = response.getText();
        return text.equals("true") || text.equals("\"true\"");
    }

    public static<T> T responseAsAutoBean(Response response, Class<T> clazz, AutoBeanFactory factory) {
        return AutoBeanCodex.decode(factory, clazz, response.getText()).as();
    }

    public static void sendJSON(String url, String data, HttpRequestCallback callback) {
        RequestBuilder requestBuilder = createJsonRequestBuilder(url, data);
        sendRequestBuilder(requestBuilder, callback);
    }

    public static RequestBuilder createJsonRequestBuilder(String url, String data) {
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, URL.encode(url));
        requestBuilder.setHeader("Content-Type", "application/json");
        requestBuilder.setRequestData(data);
        requestBuilder.setTimeoutMillis(HTTP_TIMEOUT);
        return requestBuilder;
    }

    public static void sendRequestBuilder(RequestBuilder requestBuilder, final HttpRequestCallback callback) {
        try {
            requestBuilder.setCallback(new RequestCallback() {
                @Override
                public void onResponseReceived(Request request, Response response) {
                    if (response.getStatusCode() == HttpStatus.OK.value()) {
                        callback.onSuccess(request, response);
                    }
                    else {
                        callback.onFailure(RequestFailure.wrongStatusCode(request, response));
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    callback.onFailure(RequestFailure.callbackError(request, exception));
                }
            });
            requestBuilder.send();
        } catch (RequestException exception) {
            callback.onFailure(RequestFailure.requestException(exception));
        }
    }
}
