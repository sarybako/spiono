package com.volvo.platform.gwt.client.http;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;

public interface HttpRequestSuccessCallback {

    void onSuccess(Request request, Response response);
}
