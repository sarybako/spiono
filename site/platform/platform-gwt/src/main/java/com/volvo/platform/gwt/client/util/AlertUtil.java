package com.volvo.platform.gwt.client.util;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.AlertType;

public final class AlertUtil {

    private AlertUtil() {}

    public static void alert(String title, String text, AlertType type) {
        Alert alert = new Alert();
        Modal modal = new Modal();
        alert.setText(text);
        alert.setClose(false);
        alert.setType(type);
        modal.setTitle(title);
        modal.add(alert);
        modal.show();
    }
}
