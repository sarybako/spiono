package com.volvo.platform.gwt.client.http;

public enum RequestFailureReason {

    REQUEST_TIMEOUT_EXCEPTION,
    REQUEST_PERMISSION_EXCEPTION,
    REQUEST_GENERAL_EXCEPTION,
    CALLBACK_ERROR,
    WRONG_STATUS_CODE
}
