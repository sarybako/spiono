package com.volvo.platform.gwt.shared.dto;

import javax.validation.constraints.NotNull;

public interface PageableDTO extends GwtDTO {

    @NotNull
    //@Min(SharedGwtConsts.MIN_PAGE_NUMBER)
    int getPageNumber();
    void setPageNumber(int pageNumber);

    @NotNull
    //@Max(SharedGwtConsts.MAX_PAGE_SIZE)
    //@Min(SharedGwtConsts.MIN_PAGE_SIZE)
    int getPageSize();
    void setPageSize(int pageSize);

}
