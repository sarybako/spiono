package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.PageableDTO;

import java.util.Date;

public interface GetKeyLogDataDTO extends PageableDTO {

    long ALL_AGENTS = -1;

	long getAgentId();
	void setAgentId(long agentId);

    Date getStartDate();
    void setStartDate(Date startDate);

    Date getEndDate();
    void setEndDate(Date endDate);

    String getSearchingString();
    void setSearchingString(String searchingString);
}
