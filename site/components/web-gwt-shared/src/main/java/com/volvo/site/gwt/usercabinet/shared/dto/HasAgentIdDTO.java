package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

public interface HasAgentIdDTO extends GwtDTO {

    long getAgentId();

    void setAgentId(long agentId);
}
