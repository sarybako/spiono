package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

public interface CreateAgentResultDTO extends HasAgentIdDTO, GwtDTO {

    String getDownloadKey();

    void setDownloadKey(String downloadKey);
}
