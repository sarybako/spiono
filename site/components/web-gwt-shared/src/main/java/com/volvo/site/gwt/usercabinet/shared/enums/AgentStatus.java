package com.volvo.site.gwt.usercabinet.shared.enums;

public enum AgentStatus {
    NOT_REGISTERED,
    ONLINE,
    OFFLINE,
    ON_PAUSE,
    DELETED;

    @Override
    public String toString() {
        return name().replace("_", " ");
    }

    public boolean canBeDownloaded() {
        return this == NOT_REGISTERED;
    }

    public boolean canBeDeleted() {
        return this != DELETED;
    }

    public boolean canBeResumed() {
        return this == ON_PAUSE;
    }

    public boolean canBePaused() {
        return this == ONLINE || this == OFFLINE;
    }
}
