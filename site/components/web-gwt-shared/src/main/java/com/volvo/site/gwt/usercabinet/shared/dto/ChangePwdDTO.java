package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts.PWD_LEN_MAX;
import static com.volvo.site.gwt.usercabinet.shared.constants.SharedGwtConsts.PWD_LEN_MIN;

public interface ChangePwdDTO extends GwtDTO {

    @NotNull
    @Length(min = PWD_LEN_MIN, max = PWD_LEN_MAX)
    String getOldPassword();

    void setOldPassword(String oldPassword);

    @NotNull
    @Length(min = PWD_LEN_MIN, max = PWD_LEN_MAX)
    String getNewPassword();

    void setNewPassword(String newPassword);
}
