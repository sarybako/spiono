package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

import java.util.List;

public interface KeyLogDataListResultDTO extends GwtDTO {

	List<KeyLogDataDTO> getKeyLogData();
	void setKeyLogData(List<KeyLogDataDTO> keyLogData);

    List<AgentDTO> getAvailableAgents();
    void setAvailableAgents(List<AgentDTO> availableAgents);
}
