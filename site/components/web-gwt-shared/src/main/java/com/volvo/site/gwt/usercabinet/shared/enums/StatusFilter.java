package com.volvo.site.gwt.usercabinet.shared.enums;

public enum StatusFilter {
    ALL_AGENTS,
    REGISTERED,
    NOT_REGISTERED,
    DELETED
}
