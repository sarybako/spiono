package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.PageableDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;

public interface GetAgentsDTO extends PageableDTO {

    String getSearchingString();
    void setSearchingString(String searchingString);

    StatusFilter getStatusFilter();
    void setStatusFilter(StatusFilter filter);

}
