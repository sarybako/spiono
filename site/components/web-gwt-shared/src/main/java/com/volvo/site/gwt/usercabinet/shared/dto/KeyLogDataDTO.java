package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

import java.util.Date;

public interface KeyLogDataDTO extends GwtDTO {

	String getText();
	void setText(String text);

	Date getStartDate();
	void setStartDate(Date startDate);

	Date getEndDate();
	void setEndDate(Date endDate);

	long getHWND();
	void setHWND(long HWND);

	String getWindowText();
	void setWindowText(String windowText);

	String getProcessName();
	void setProcessName(String processName);

}
