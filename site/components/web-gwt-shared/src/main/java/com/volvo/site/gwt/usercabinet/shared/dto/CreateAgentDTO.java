package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

public interface CreateAgentDTO extends GwtDTO {
    void setName(String name);
    String getName();
}
