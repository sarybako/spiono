package com.volvo.site.gwt.usercabinet.shared.dto;

import com.volvo.platform.gwt.shared.dto.GwtDTO;

public interface GetUserNameDTO extends GwtDTO {

    void setEmail(String email);
    String getEmail();

}
