package com.volvo.site.gwt.usercabinet.shared.constants;

public final class SharedGwtConsts {

    private SharedGwtConsts(){}

    public static final String GWT_DISPATCHER_RELATIVE_PATH = "dispatcher";

    public static final String DIV_WITH_USERNAME = "username-div-id";

    public static final int PWD_LEN_MIN = 5;
    public static final int PWD_LEN_MAX = 30;

    public static final int MIN_PAGE_NUMBER = 0;
    public static final int MIN_PAGE_SIZE = 1;
    public static final int MAX_PAGE_SIZE = 100;
}
