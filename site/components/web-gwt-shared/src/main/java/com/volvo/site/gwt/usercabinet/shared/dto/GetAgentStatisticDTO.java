package com.volvo.site.gwt.usercabinet.shared.dto;

import java.util.Date;

public interface GetAgentStatisticDTO extends HasAgentIdDTO {

	Date getStartDate();
	void setStartDate(Date startDate);

	Date getEndDate();
	void setEndDate(Date endDate);
}
