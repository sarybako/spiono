package com.volvo.site.gwt.usercabinet.shared.dto;

public interface SetAgentOnPauseDTO extends HasAgentIdDTO {

    boolean getIsOnPause();
    void setIsOnPause(boolean onPause);
}
