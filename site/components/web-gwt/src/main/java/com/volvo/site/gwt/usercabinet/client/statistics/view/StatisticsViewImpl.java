package com.volvo.site.gwt.usercabinet.client.statistics.view;

import com.github.gwtbootstrap.client.ui.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.volvo.platform.gwt.client.util.GwtUtil;
import com.volvo.platform.gwt.client.util.HasLoading;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsView;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.volvo.platform.gwt.client.util.UiUtil.makeAgentStatusTooltip;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.commonMessages;
import static com.volvo.site.gwt.usercabinet.client.util.AgentUtil.*;

public class StatisticsViewImpl extends Composite implements StatisticsView {
    interface LogsViewUiBinder extends UiBinder<Widget, StatisticsViewImpl> {	}
    private static LogsViewUiBinder uiBinder = GWT.create(LogsViewUiBinder.class);

	@UiField
	HTMLPanel agentStatusPanel;

    @UiField
    DropdownButton agentName;

    @UiField
    DateRangeFilter dateRangeFilter;

	@UiField
	Button refreshButton;

    @UiField
    Button showStatisticsButton;

	@UiField
	Button othersButton;

	@UiField
	Paragraph infoParagraph;

    @UiField
    WellForm statisticsWidget;

	@UiField
	WellForm otherStatisticsWidget;

	@UiField
	WellForm info;

    @UiField
    HTMLPanel searchPanel;

	private long agentId;
    private StatisticPresenter presenter;
	private Label agentStatus = new Label();
	private TreeMap<String,Integer> othersMap = new TreeMap<String, Integer>();

	private static final int DANGER_PERCENT = 50;
	private static final int WARNING_PERCENT = 30;
	private static final int SUCCESS_PERCENT = 20;
	private static final int DEFAULT_PERCENT = 10;
	private static final int INFO_PERCENT = 5;

	public StatisticsViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

    @Override
    public void setPresenter(StatisticPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public void setAgents(List<AgentDTO> listAgents) {
        boolean agentHasBeenSet = false;
        for (AgentDTO agent :  listAgents) {
            if (agent.getAgentId() == agentId) {
                setCurrentAgent(agent);
                agentHasBeenSet = true;
            }
            NavLink agentLink = new NavLink(agent.getName());
            agentLink.setHref("#statistics:" + agent.getAgentId());
            agentName.add(agentLink);
        }

        if (!agentHasBeenSet && !listAgents.isEmpty()) {
            setCurrentAgent(listAgents.get(0));
        }
    }

    @Override
    public void onStop() {
		statisticsWidget.clear();
        dateRangeFilter.reset();
        agentName.clear();
    }

	@Override
    public void setStatistics(Map<String, Integer> statisticsMap) {
		otherStatisticsWidget.setVisible(false);
		othersButton.setVisible(false);

		if (statisticsMap.isEmpty() && !dateRangeFilter.isChecked()) {
			statisticsWidget.setVisible(false);
			searchPanel.setVisible(false);
            info.setVisible(true);
			return;
		}

		statisticsWidget.clear();
		statisticsWidget.setVisible(true);
        info.setVisible(false);
		searchPanel.setVisible(true);

		if (statisticsMap.isEmpty()) {
			statisticsWidget.add(new com.google.gwt.user.client.ui.Label(commonMessages.noResultBySearch()));
			return;
		}

		StatisticsComparator comparator =  new StatisticsComparator(statisticsMap);
		TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(comparator);
		sortedMap.putAll(statisticsMap);

		for(Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			ProgressBar progress = new ProgressBar();
			progress.setText(entry.getValue() + "%");
			if (entry.getValue() > INFO_PERCENT) {
				progress.setPercent(entry.getValue());
				progress.setHeight("30px");
				progress.setColor(getColor(entry.getValue()));
				statisticsWidget.add(new com.google.gwt.user.client.ui.Label(entry.getKey()));
				statisticsWidget.add(progress);
			}
			else {
				othersMap.put(entry.getKey(), entry.getValue());
			}
		}

		if (!othersMap.isEmpty()) {
			othersButton.setVisible(true);
			othersButton.setText(commonMessages.btnOthersShow());
		}
    }

    private void setCurrentAgent(AgentDTO agent) {
        agentName.setText(agent.getName());
	    updateAgentStatus(agent.getStatus());
    }

    @UiHandler("showStatisticsButton")
    void onShowStatisticsButtonClicked(ClickEvent event) {
		setStatistics();
    }

	@UiHandler("refreshButton")
	void onRefreshButtonClicked(ClickEvent event) {
		setStatistics();
	}

	@UiHandler("othersButton")
	void onOthersButtonClicked(ClickEvent event) {
		if (otherStatisticsWidget.isVisible()) {
			otherStatisticsWidget.setVisible(false);
			othersButton.setText(commonMessages.btnOthersShow());
			return;
		}

		otherStatisticsWidget.setVisible(true);
		otherStatisticsWidget.clear();

		StatisticsComparator comparator =  new StatisticsComparator(othersMap);
		TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(comparator);
		sortedMap.putAll(othersMap);

		for(Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			ProgressBar progress = new ProgressBar();
			progress.setText(entry.getValue() + "%");

			progress.setPercent(entry.getValue() * 20);
			progress.setHeight("30px");
			progress.setColor(getOthersColor(entry.getValue()));
			otherStatisticsWidget.add(new com.google.gwt.user.client.ui.Label(entry.getKey()));
			otherStatisticsWidget.add(progress);
		}
		othersButton.setText(commonMessages.btnOthersHide());
	}

	@Override
	public HasLoading refreshing() {
		return UiUtil.hasLoadingForButtons(refreshButton);
	}

	@Override
	public HasLoading searching() {
		return UiUtil.hasLoadingForButtons(showStatisticsButton);
	}

	private void updateAgentStatus(AgentStatus status){
		agentStatusPanel.remove(agentStatus);
		agentStatus = new Label(getLabelTextByStatus(status));
		agentStatus.setType(getLabelTypeByStatus(status));
		agentStatus.setText(getLabelTextByStatus(status));
		agentStatus.getElement().setAttribute("statusTooltipSelector", getLabelTextByStatus(status));
		agentStatusPanel.add(agentStatus);
		makeAgentStatusTooltip(getLabelTextByStatus(status), getTooltipTextByStatus(status));
	}

    private void setStatistics() {
        DateRangeFilter.DateRangeValue dateRange = dateRangeFilter.getNormalizedValue();
        if (dateRange.enabled && dateRange.endDate.compareTo(dateRange.startDate) >= 0) {
            GwtUtil.addOneDayToDate(dateRange.endDate);
        }
        presenter.setStatistics(dateRange);
    }

	private ProgressBar.Color getColor(Integer percent) {
		if (percent >= DANGER_PERCENT) {
			return ProgressBar.Color.DANGER;
		}
		else if	(percent >= WARNING_PERCENT) {
			return ProgressBar.Color.WARNING;
		}
		else if (percent >= SUCCESS_PERCENT) {
			return ProgressBar.Color.SUCCESS;
		}
		else if (percent > DEFAULT_PERCENT) {
			return ProgressBar.Color.DEFAULT;
		}
		else return ProgressBar.Color.INFO;
	}

	private ProgressBar.Color getOthersColor(Integer percent) {
		if (percent == 5) {
			return ProgressBar.Color.DANGER;
		}
		else if	(percent == 4) {
			return ProgressBar.Color.WARNING;
		}
		else if (percent == 3) {
			return ProgressBar.Color.SUCCESS;
		}
		else if (percent == 2) {
			return ProgressBar.Color.DEFAULT;
		}
		else return ProgressBar.Color.INFO;
	}

	private class StatisticsComparator implements Comparator<String> {
		Map<String, Integer> base;
		public StatisticsComparator(Map<String, Integer> base) {
			this.base = base;
		}

		@Override
		public int compare(String a, String b) {
			if (base.get(a) >= base.get(b)) {
				return -1;
			} else {
				return 1;
			}
		}
	}
}
