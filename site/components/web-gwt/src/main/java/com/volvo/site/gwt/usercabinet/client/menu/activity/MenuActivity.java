package com.volvo.site.gwt.usercabinet.client.menu.activity;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.site.gwt.usercabinet.client.menu.view.MenuView;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;
import com.volvo.site.gwt.usercabinet.shared.dto.GetUserNameDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.GetUserNameResultDTO;

import javax.inject.Inject;
import javax.inject.Provider;

import static com.volvo.platform.gwt.client.http.HttpUtils.responseAsAutoBean;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getBeanFactory;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getCommandDispatcher;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;

public class MenuActivity extends BaseActivity implements MenuView.MenuPresenter {

    private final MenuView view;
    private EventBus eventBus;

    private final Provider<ChangePasswordActivity> changePasswordActivityProvider;

    @Inject
    public MenuActivity(MenuView view, Provider<ChangePasswordActivity> changePasswordActivityProvider) {
        this.view = view;
        this.changePasswordActivityProvider = changePasswordActivityProvider;
    }

    @Override
    public void start(AcceptsOneWidget panel, EventBus eventBus) {
        this.eventBus = eventBus;
        view.setPresenter(this);
        panel.setWidget(view.asWidget());
        updateUserName();
    }

    @Override
    public void onChangePasswordClicked() {
        changePasswordActivityProvider.get().start(null, eventBus);
    }

    private void updateUserName() {
        AutoBean<GetUserNameDTO> getUserNameDTO = getBeanFactory().getUserNameDTO();
        getUserNameDTO.as().setEmail(view.getUserEmail());

        HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
            @Override
            public void onSuccess(Request request, Response response) {
                GetUserNameResultDTO resultDTO = responseAsAutoBean(response, GetUserNameResultDTO.class, getBeanFactory());
                view.setUserName(resultDTO.getName());
            }
        });
        getCommandDispatcher().dispatchCommand(getUserNameDTO, callback);
    }

}
