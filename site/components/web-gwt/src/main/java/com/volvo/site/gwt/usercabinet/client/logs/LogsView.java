package com.volvo.site.gwt.usercabinet.client.logs;


import com.google.gwt.user.client.ui.IsWidget;
import com.volvo.platform.gwt.client.util.HasLoading;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;

import java.util.List;

public interface LogsView extends IsWidget {
    void setPresenter(LogsPresenter presenter);
    void setAgentId(long agentId);
    void setAgents(List<AgentDTO> listAgents);
    void onStop();
    void setLogs(List<KeyLogDataDTO> keyLogData, boolean isSearch, int logsPageSize);
    HasLoading loadingNextLogs();
	HasLoading refreshing();
	HasLoading searching();

    public interface LogsPresenter {
        void setLogs(DateRangeFilter.DateRangeValue dateRangeValue, String keywords, int lastKeyLogId);
    }
}
