package com.volvo.site.gwt.usercabinet.client.statistics;


import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class StatisticsPlace extends Place {
    private static final String VIEW_HISTORY_TOKEN = "statistics";
    private String agentId;

    public String getAgentId() {
        return agentId;
    }

    public StatisticsPlace(String agentId) {
        this.agentId = agentId;
    }

    @Prefix(value = VIEW_HISTORY_TOKEN)
    public static class Tokenizer implements PlaceTokenizer<StatisticsPlace> {
        @Override
        public StatisticsPlace getPlace(String token) {
            return new StatisticsPlace(token);
        }

        @Override
        public String getToken(StatisticsPlace place) {
            return place.getAgentId();
        }
    }
}
