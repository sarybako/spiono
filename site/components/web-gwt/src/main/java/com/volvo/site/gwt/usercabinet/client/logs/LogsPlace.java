package com.volvo.site.gwt.usercabinet.client.logs;


import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class LogsPlace extends Place {
    private static final String VIEW_HISTORY_TOKEN = "logs";
    private String agentId;

    public String getAgentId() {
        return agentId;
    }

    public LogsPlace(String agentId) {
        this.agentId = agentId;
    }

    @Prefix(value = VIEW_HISTORY_TOKEN)
    public static class Tokenizer implements PlaceTokenizer<LogsPlace> {
        @Override
        public LogsPlace getPlace(String token) {
            return new LogsPlace(token);
        }

        @Override
        public String getToken(LogsPlace place) {
            return place.getAgentId();
        }
    }
}
