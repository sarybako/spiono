package com.volvo.site.gwt.usercabinet.client.widgets;

import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.datepicker.client.ui.DateBox;
import com.google.common.base.Objects;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

import java.util.Date;

public class DateRangeFilter extends Composite implements TakesValue<DateRangeFilter.DateRangeValue> {

    interface DateRangeFilterUiBinder extends UiBinder<HTMLPanel, DateRangeFilter> {
    }

    private static DateRangeFilterUiBinder ourUiBinder = GWT.create(DateRangeFilterUiBinder.class);

    public static final DateRangeValue DATE_RANGE_DISABLED = new DateRangeValue(false, null, null);

    public static class DateRangeValue {
        public boolean enabled;
        public Date startDate;
        public Date endDate;

        public DateRangeValue(boolean enabled, Date startDate, Date endDate) {
            this.enabled = enabled;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

    @UiField
    CheckBox checkBox;

    @UiField
    DateBox startDate;

    @UiField
    DateBox endDate;

    private String checkBoxText;

    private boolean isChecked;

    @UiConstructor
    public DateRangeFilter(String checkBoxText, boolean checked) {
        initWidget(ourUiBinder.createAndBindUi(this));

        this.checkBoxText = checkBoxText;
        this.isChecked = checked;
        updateUi();
    }

    public void reset() {
        Date date = new Date();
        isChecked = false;
        startDate.setValue(date);
        endDate.setValue(date);
        updateUi();
    }

    @Override
    public void setValue(DateRangeValue value) {
        this.isChecked = value.enabled;
        this.startDate.setValue(value.startDate, false);
        this.endDate.setValue(value.endDate, false);

        updateUi();
        normalizeDates();
    }

    public DateRangeValue getNormalizedValue() {
        normalizeDates();
        return getValue();
    }

    @Override
    public DateRangeValue getValue() {
        return new DateRangeValue(checkBox.getValue(), startDate.getValue(), endDate.getValue());
    }

    public String getCheckBoxText() {
        return checkBoxText;
    }

    public void setCheckBoxText(String checkBoxText) {
        if (!Objects.equal(this.checkBox, checkBoxText)) {
            this.checkBoxText = checkBoxText;
            updateUi();
        }
    }

    public void setChecked(boolean checked) {
        if (isChecked != checked) {
            isChecked = checked;
            updateUi();
        }
    }

    public boolean isChecked() {
        return isChecked;
    }

    @UiHandler({"startDate", "endDate"})
    void onDatesValueChange(ValueChangeEvent<Date> event) {
        normalizeDates(event.getSource());
    }

    @UiHandler("checkBox")
    void onCheckBoxClick(ClickEvent event){
        setChecked(checkBox.getValue());
    }

    private void normalizeDates() {
        normalizeDates(null);
    }

    private void normalizeDates(Object source) {
        if (startDate.getValue().getTime() <= endDate.getValue().getTime()) {
            return;
        }
        if (source == startDate) {
            endDate.setValue(startDate.getValue(), false);
        }
        else  {
            startDate.setValue(endDate.getValue(), false);
        }
    }

    private void updateUi() {
        checkBox.setText(checkBoxText);
        checkBox.setValue(isChecked);
        startDate.setEnabled(isChecked);
        endDate.setEnabled(isChecked);
    }
}