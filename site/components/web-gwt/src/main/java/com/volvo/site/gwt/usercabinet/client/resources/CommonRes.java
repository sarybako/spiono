package com.volvo.site.gwt.usercabinet.client.resources;

import com.google.gwt.resources.client.ClientBundle;

public interface CommonRes extends ClientBundle {

    @Source("CommonCss.css")
    CommonCss commonCss();

    @Source("KeyLogsCss.css")
    KeyLogsCss keylogsCss();
}
