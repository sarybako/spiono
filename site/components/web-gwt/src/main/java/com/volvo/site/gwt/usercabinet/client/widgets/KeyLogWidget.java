package com.volvo.site.gwt.usercabinet.client.widgets;

import com.github.gwtbootstrap.client.ui.Hero;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.AppWorkflow;
import com.volvo.site.gwt.usercabinet.client.resources.KeyLogsCss;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;

import java.util.Date;

import static com.volvo.platform.gwt.client.util.UiUtil.formatDateMedium;
import static com.volvo.platform.gwt.client.util.UiUtil.formatTimeShort;

public class KeyLogWidget extends Hero {

    private static final KeyLogsCss css = AppWorkflow.commonRes.keylogsCss();

    private KeyLogDataDTO lastLog;

    public KeyLogWidget(KeyLogDataDTO keyLogData) {
        setStyleName(css.heroStyle());
        addHead(keyLogData);
        addLog(keyLogData);
    }

    public boolean canLogBeAppended(KeyLogDataDTO keyLogData) {
        return keyLogData.getHWND() == lastLog.getHWND() &&
                keyLogData.getProcessName().equals(lastLog.getProcessName()) &&
                keyLogData.getWindowText().equals(lastLog.getWindowText()) &&
                formatDateMedium(keyLogData.getStartDate()).equals(formatDateMedium(lastLog.getStartDate()));
    }

    public void addLog(KeyLogDataDTO keyLogData) {
        HTMLPanel panel = new HTMLPanel("");
        if (hasTimeChanged(keyLogData.getStartDate())) {
            panel.add(createTime(keyLogData.getStartDate()));
        }
        panel.add(createText(keyLogData.getText()));
        insert(panel, 1);
        this.lastLog = keyLogData;
    }

    private void addHead(KeyLogDataDTO keyLog) {
        HTMLPanel panel = new HTMLPanel("");
        panel.add(createDate(keyLog));
        panel.add(createProcessAndWindowName(keyLog));
        add(panel);
    }

    private boolean hasTimeChanged(Date newDate) {
        if (lastLog == null) {
            return true;
        }
        return !formatTimeShort(lastLog.getStartDate()).equals(formatTimeShort(newDate));
    }

    private static Widget createTime(Date startDate) {
        Widget time =  new Label(formatTimeShort(startDate));
        time.setStyleName(css.floatRight());
        return time;
    }

    private static Widget createText(String text) {
        Widget textWidget = new Label(text);
        textWidget.setStyleName(css.fontStyleItalic());
        return textWidget;
    }

    private static Widget createDate(KeyLogDataDTO keyLog) {
        Widget date = new Label(UiUtil.formatDateMedium(keyLog.getStartDate()));
        date.setStyleName(css.floatRight());
        return date;
    }

    private static Widget createProcessAndWindowName(KeyLogDataDTO keyLog) {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(keyLog.getProcessName()).append("] ");
        sb.append("\"").append(keyLog.getWindowText()).append("\"");
        Widget processAndWindowName = new Label(sb.toString());
        processAndWindowName.setStyleName(css.boldText());
        return processAndWindowName;
    }
}
