package com.volvo.site.gwt.usercabinet.client.gin;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsActivity;
import com.volvo.site.gwt.usercabinet.client.logs.LogsActivity;
import com.volvo.site.gwt.usercabinet.client.menu.activity.MenuActivity;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsActivity;

import javax.inject.Provider;

@GinModules(ClientFactoryModule.class)
public interface ClientFactoryGinjector extends Ginjector {
    Provider<AgentsActivity> agentsActivity();
    Provider<LogsActivity> logsActivity();
    Provider<StatisticsActivity> statisticActivity();

    MenuActivity menuActivity();
}
