package com.volvo.site.gwt.usercabinet.client.logs.view;

import com.github.gwtbootstrap.client.ui.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.volvo.platform.gwt.client.util.GwtUtil;
import com.volvo.platform.gwt.client.util.HasLoading;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.client.widgets.KeyLogWidget;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;

import java.util.List;

import static com.volvo.platform.gwt.client.util.UiUtil.makeAgentStatusTooltip;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.commonMessages;
import static com.volvo.site.gwt.usercabinet.client.util.AgentUtil.*;

public class LogsViewImpl extends Composite implements com.volvo.site.gwt.usercabinet.client.logs.LogsView {
    interface LogsViewUiBinder extends UiBinder<Widget, LogsViewImpl> {	}

    private static LogsViewUiBinder uiBinder = GWT.create(LogsViewUiBinder.class);
    private int logsPageSize;
	private long agentId;
    private int keyLogCount = 0;
    private KeyLogDataDTO lastKeyLogDataDTO = null;

	@UiField
	HTMLPanel agentStatusPanel;

    @UiField
    DropdownButton agentName;

    @UiField
    DateRangeFilter dateRangeFilter;

    @UiField
    Button searchButton;

    @UiField
    Button refreshButton;

    @UiField
    HTMLPanel logsPanel;

    @UiField
    TextBox searchKeywords;

    @UiField
    Icon removeSearchIcon;

    @UiField
    Button nextLogsButton;

    @UiField
    WellForm info;

    @UiField
    Paragraph infoParagraph;

    @UiField
    HTMLPanel searchPanel;

    private LogsPresenter presenter;
	private Label agentStatus = new Label();
    private KeyLogWidget lastKeyLog;

    public LogsViewImpl() {
        initWidget(uiBinder.createAndBindUi(this));
        searchKeywordsKeyDownHandler();
        removeSearchIconClickHandler();
    }

    @Override
    public void setPresenter(LogsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public void setAgents(List<AgentDTO> listAgents) {
        boolean agentHasBeenSet = false;
        for (AgentDTO agent :  listAgents) {
            if (agent.getAgentId() == agentId) {
                setCurrentAgent(agent);
                agentHasBeenSet = true;
            }
            NavLink agentLink = new NavLink(agent.getName());
            agentLink.setHref("#logs:" + agent.getAgentId());
            agentName.add(agentLink);
        }

        if (!agentHasBeenSet && !listAgents.isEmpty()) {
            setCurrentAgent(listAgents.get(0));
        }
    }

    @Override
    public void onStop() {
        logsPanel.clear();
        keyLogCount = 0;
        searchKeywords.setText("");
        dateRangeFilter.reset();
        agentName.clear();
    }

    @Override
    public HasLoading loadingNextLogs() {
        return UiUtil.hasLoadingForButtons(nextLogsButton);
    }

	@Override
	public HasLoading refreshing() {
		return UiUtil.hasLoadingForButtons(refreshButton);
	}

	@Override
	public HasLoading searching() {
		return UiUtil.hasLoadingForButtons(searchButton);
	}

	@Override
    public void setLogs(List<KeyLogDataDTO> keyLogData, boolean isSearch, int logsPageSize) {
        info.setVisible(false);
		this.logsPageSize = logsPageSize;

        nextLogsButton.setVisible(false);
        if(keyLogData.size() == 0 && lastKeyLogDataDTO == null) {
            logsPanel.clear();
            keyLogCount = 0;
            showInfo(isSearch);
            return;
        }

        searchPanel.setVisible(true);

        if (keyLogData.size() == logsPageSize) {
            nextLogsButton.setVisible(true);
        }

        lastKeyLog = null;
        for (KeyLogDataDTO keyLog : keyLogData) {
            addLog(keyLog);
        }
    }

    private void addLog(KeyLogDataDTO keyLog) {
        if (lastKeyLog == null || !lastKeyLog.canLogBeAppended(keyLog)) {
            lastKeyLog = new KeyLogWidget(keyLog);
            logsPanel.add(lastKeyLog);
        }
        else {
            lastKeyLog.addLog(keyLog);
        }
    }

    private void showInfo(boolean show) {
        if(show) {
            infoParagraph.setText(commonMessages.noResultBySearch());
            refreshButton.setVisible(false);
        }
        else {
            searchPanel.setVisible(false);
            infoParagraph.setText(commonMessages.noLogs());
            refreshButton.setVisible(true);
        }

        info.setVisible(true);
    }

    private void setCurrentAgent(AgentDTO agent) {
        agentName.setText(agent.getName());
	    updateAgentStatus(agent.getStatus());
    }

    @UiHandler("nextLogsButton")
    void onNextLogsButtonClicked(ClickEvent event) {
        keyLogCount += logsPageSize;
        doSearch();
    }

    @UiHandler("refreshButton")
    void onRefreshButtonClicked(ClickEvent event) {
        doSearch();
    }

    @UiHandler("searchButton")
    void onSearchButtonClicked(ClickEvent event) {
        clearTableAndSearch();
    }

	private void updateAgentStatus(AgentStatus status){
		agentStatusPanel.remove(agentStatus);
		agentStatus = new Label(getLabelTextByStatus(status));
		agentStatus.setType(getLabelTypeByStatus(status));
		agentStatus.setText(getLabelTextByStatus(status));
		agentStatus.getElement().setAttribute("statusTooltipSelector", getLabelTextByStatus(status));
		agentStatusPanel.add(agentStatus);
		makeAgentStatusTooltip(getLabelTextByStatus(status), getTooltipTextByStatus(status));
	}

    private void searchKeywordsKeyDownHandler() {
        searchKeywords.addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() ==  KeyCodes.KEY_ENTER) {
                    clearTableAndSearch();
                }
            }
        });
    }

    private void removeSearchIconClickHandler() {
        removeSearchIcon.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                searchKeywords.setText("");
            }
        }, ClickEvent.getType());
    }

    private void clearTableAndSearch() {
        keyLogCount = 0;
        logsPanel.clear();
        nextLogsButton.setVisible(false);
        doSearch();
    }

    private void doSearch() {
        DateRangeFilter.DateRangeValue dateRange = dateRangeFilter.getNormalizedValue();
        if (dateRange.enabled && dateRange.endDate.compareTo(dateRange.startDate) >= 0) {
            GwtUtil.addOneDayToDate(dateRange.endDate);
        }
        presenter.setLogs(dateRange, searchKeywords.getText(), keyLogCount);
    }
}