package com.volvo.site.gwt.usercabinet.client.agents.view;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.HelpBlock;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.google.common.base.Objects;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.volvo.platform.gwt.client.util.StrUtil;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.widgets.PopoverPanel;

import static com.google.gwt.core.client.Scheduler.ScheduledCommand;
import static com.volvo.platform.gwt.client.util.UiUtil.*;

public class NameEditor extends PopupPanel {
    interface NameEditorUiBinder extends UiBinder<HTMLPanel, NameEditor> {
    }

    public interface NameEditorCallback {
        void onNameEdited(String lastName, String newName);
    }

    private static NameEditorUiBinder ourUiBinder = GWT.create(NameEditorUiBinder.class);

    @UiField
    TextBox agentName;

    @UiField
    ControlGroup nameControlGroup;

    @UiField
    HelpBlock errBlock;

    @UiField
    PopoverPanel titlePanel;

    @UiField
    Button saveButton;

    private final String initialName;

    private final NameEditorCallback callback;

    public NameEditor(String initialName, NameEditorCallback callback) {
        super(true, false);
        setWidget(ourUiBinder.createAndBindUi(this));

        agentName.setValue(initialName);
        hidePopupOnEscape(this);
        setFocus(agentName);

        this.initialName = initialName;
        this.callback = callback;
    }

    public void showWithCoordinates(final int left, final int top) {
        UiUtil.scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                setPopupPositionAndShow(new PopupPanel.PositionCallback() {
                    @Override
                    public void setPosition(int offsetWidth, int offsetHeight) {
                        setPopupPosition(left, top);
                    }
                });
            }
        });
    }

    public void setTitle(String title) {
        titlePanel.setTitle(title);
    }

    public String getTitle() {
        return titlePanel.getTitle();
    }

    public void setSaveButtonText(String text) {
        saveButton.setText(text);
    }

    public String getSaveButtonText() {
        return saveButton.getText();
    }

    @Override
    public void show() {
        super.show();
        selectAndFocus();

        agentName.addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() ==  KeyCodes.KEY_ENTER) {
                    onSaveButtonClick(null);
                }
            }
        });
    }

    @UiHandler("cancelButton")
    void onCancelButtonClick(ClickEvent event) {
        hide();
    }

    @UiHandler("saveButton")
    void onSaveButtonClick(ClickEvent event) {
        String newValue = agentName.getValue();
        boolean isTextEmpty = StrUtil.nullOrEmpty(newValue);
        errBlock.setVisible(isTextEmpty);
        nameControlGroup.setType(isTextEmpty ? ControlGroupType.ERROR : ControlGroupType.NONE);
        if (isTextEmpty) {
            selectAndFocus();
            return;
        }

        if (!Objects.equal(initialName, newValue) && !StrUtil.nullOrEmpty(newValue)) {
            callback.onNameEdited(initialName, newValue);
        }
        hide();
    }

    private void selectAndFocus() {
        scheduleDeferred(new ScheduledCommand() {
            @Override
            public void execute() {
                agentName.setFocus(true);
                agentName.setCursorPos(agentName.getValue().length());
                agentName.selectAll();
            }
        });
    }
}