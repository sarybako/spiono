package com.volvo.site.gwt.usercabinet.client.statistics;


import com.google.gwt.user.client.ui.IsWidget;
import com.volvo.platform.gwt.client.util.HasLoading;
import com.volvo.site.gwt.usercabinet.client.widgets.DateRangeFilter;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;

import java.util.List;
import java.util.Map;

public interface StatisticsView extends IsWidget {
    void setPresenter(StatisticPresenter presenter);
    void setAgentId(long agentId);
    void setAgents(List<AgentDTO> listAgents);
    void onStop();
    void setStatistics(Map<String, Integer> statisticsMap);
	HasLoading refreshing();
	HasLoading searching();

    public interface StatisticPresenter {
        void setStatistics(DateRangeFilter.DateRangeValue dateRange);
    }
}
