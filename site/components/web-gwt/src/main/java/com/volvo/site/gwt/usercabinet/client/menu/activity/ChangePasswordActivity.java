package com.volvo.site.gwt.usercabinet.client.menu.activity;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.platform.gwt.client.util.AlertUtil;
import com.volvo.site.gwt.usercabinet.client.menu.view.ChangePasswordView;
import com.volvo.site.gwt.usercabinet.client.mvp.activity.BaseActivity;
import com.volvo.site.gwt.usercabinet.shared.dto.ChangePwdDTO;

import javax.inject.Inject;

import static com.volvo.platform.gwt.client.http.HttpUtils.responseAsBoolean;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.*;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.appendHasLoadingResetToCallback;
import static com.volvo.site.gwt.usercabinet.client.util.HttpRequestCallbackBuilder.newCallbackWithDefaultErrorHandler;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView.ChangePasswordPresenter {

    private final ChangePasswordView view;

    @Inject
    public ChangePasswordActivity(ChangePasswordView view) {
        this.view = view;
    }

    @Override
    public void start(AcceptsOneWidget popup, EventBus eventBus) {
        view.setPresenter(this);
        view.showModal();
    }

    @Override
    public void onSaveChangesButtonClicked(String oldPassword, String newPassword) {
        AutoBean<ChangePwdDTO> changePwd = getBeanFactory().changePwd();
        changePwd.as().setOldPassword(oldPassword);
        changePwd.as().setNewPassword(newPassword);

        HttpRequestCallback callback = newCallbackWithDefaultErrorHandler(new HttpRequestSuccessCallback() {
            @Override
            public void onSuccess(Request request, Response response) {
                if (responseAsBoolean(response)) {
					view.close();
					AlertUtil.alert(commonMessages.modalChangePassword(), getCommonMessages().infoPasswordsChanged(), AlertType.SUCCESS);
                } else {
                    view.showIncorrectPassword();
                }
            }
        });
        callback = appendHasLoadingResetToCallback(callback, view.loadingState());

        view.loadingState().loading();
        getCommandDispatcher().dispatchCommand(changePwd, callback);
    }
}

