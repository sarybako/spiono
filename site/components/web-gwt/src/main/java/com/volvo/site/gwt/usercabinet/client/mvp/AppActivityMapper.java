package com.volvo.site.gwt.usercabinet.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsPlace;
import com.volvo.site.gwt.usercabinet.client.gin.ClientFactoryGinjector;
import com.volvo.site.gwt.usercabinet.client.logs.LogsActivity;
import com.volvo.site.gwt.usercabinet.client.logs.LogsPlace;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsActivity;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsPlace;

import javax.inject.Provider;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getClientFactory;

public class AppActivityMapper implements ActivityMapper {

    private final static ClientFactoryGinjector clientFactory = getClientFactory();

    private final static Map<Class<? extends Place>, Provider<? extends Activity>> activities = newHashMap();

    static {
        activities.put(AgentsPlace.class, clientFactory.agentsActivity());
        activities.put(LogsPlace.class, clientFactory.logsActivity());
        activities.put(StatisticsPlace.class, clientFactory.statisticActivity());
    }

	@Override
	public Activity getActivity(Place place) {
        if (place instanceof LogsPlace) {
            LogsActivity logsActivity = (LogsActivity)activities.get(place.getClass()).get();
            logsActivity.setAgentId(((LogsPlace) place).getAgentId());
            return logsActivity;
        }
		if (place instanceof StatisticsPlace) {
			StatisticsActivity statisticsActivity = (StatisticsActivity)activities.get(place.getClass()).get();
			statisticsActivity.setAgentId(((StatisticsPlace) place).getAgentId());
			return statisticsActivity;
		}
		if (place instanceof StatisticsPlace) {
			StatisticsActivity statisticsActivity = (StatisticsActivity)activities.get(place.getClass()).get();
			statisticsActivity.setAgentId(((StatisticsPlace) place).getAgentId());
			return statisticsActivity;
		}
        return activities.get(place.getClass()).get();
	}
}
