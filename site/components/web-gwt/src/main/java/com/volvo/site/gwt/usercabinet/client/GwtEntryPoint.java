package com.volvo.site.gwt.usercabinet.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.web.bindery.event.shared.EventBus;
import com.volvo.platform.gwt.client.remerror.RemoteErrorUtils;
import com.volvo.platform.gwt.client.remerror.RemoteUncaughtExceptionHandler;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsPlace;
import com.volvo.site.gwt.usercabinet.client.layout.AppLayout;
import com.volvo.site.gwt.usercabinet.client.mvp.AppActivityMapper;
import com.volvo.site.gwt.usercabinet.client.mvp.AppPlaceHistoryMapper;

import static com.volvo.platform.gwt.client.util.UiUtil.scheduleDeferred;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getClientFactory;

public class GwtEntryPoint implements EntryPoint {

    private final AgentsPlace defaultPlace = new AgentsPlace();
    private final AppLayout mainLayout = new AppLayout();

	@Override
	public void onModuleLoad() {
        RemoteUncaughtExceptionHandler.setUp(AppWorkflow.getCommandDispatcher());

        scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                init();
	            removeModalPreloader();
            }
        });
    }

    private void init() {
        AcceptsOneWidget containerWidget = mainLayout.getAppContentHolder();
        EventBus eventBus = AppWorkflow.getEventBus();
        PlaceController placeController = AppWorkflow.placeController;

        //activate activity manager and init display
        ActivityMapper activityMapper = new AppActivityMapper();
        ActivityManager activityManager = new ActivityManager(activityMapper, eventBus);
        activityManager.setDisplay(containerWidget);

        //display default view with activated history processing
        AppPlaceHistoryMapper historyMapper = GWT.create(AppPlaceHistoryMapper.class);
        PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
        historyHandler.register(placeController, eventBus, defaultPlace);

        History.fireCurrentHistoryState();

        getClientFactory().menuActivity().start(mainLayout.getTopMenuHolder(), eventBus);
        RootPanel.get().add(mainLayout);
    }

	private void removeModalPreloader(){
		try {
			DOM.getElementById("modalPreloader").removeFromParent();
		} catch (JavaScriptException ex){
			RemoteErrorUtils.sendRemoteErrorCommand(AppWorkflow.getCommandDispatcher(), ex);
		}
	}
}