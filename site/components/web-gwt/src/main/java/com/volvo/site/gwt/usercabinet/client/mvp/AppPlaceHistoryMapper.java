package com.volvo.site.gwt.usercabinet.client.mvp;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsPlace;
import com.volvo.site.gwt.usercabinet.client.logs.LogsPlace;
import com.volvo.site.gwt.usercabinet.client.statistics.StatisticsPlace;

@WithTokenizers({AgentsPlace.Tokenizer.class, LogsPlace.Tokenizer.class, StatisticsPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
