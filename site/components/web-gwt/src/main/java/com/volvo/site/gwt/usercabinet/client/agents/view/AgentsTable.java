package com.volvo.site.gwt.usercabinet.client.agents.view;

import com.github.gwtbootstrap.client.ui.CellTable;
import com.github.gwtbootstrap.client.ui.TooltipCellDecorator;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.Placement;
import com.google.gwt.cell.client.AbstractSafeHtmlCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.SimpleSafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.volvo.platform.gwt.client.util.StrUtil;
import com.volvo.platform.gwt.client.util.UiUtil;
import com.volvo.site.gwt.usercabinet.client.agents.AgentsView;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;

import static com.volvo.platform.gwt.client.util.UiUtil.*;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.commonMessages;
import static com.volvo.site.gwt.usercabinet.client.util.AgentUtil.*;

public class AgentsTable extends CellTable<AgentDTO> {

    interface LocalHtmlTemplates extends SafeHtmlTemplates {
        @Template("<label statusTooltipSelector='{3}' class=\"label {0} {1}\"/>{2}</label>")
        SafeHtml messageWithLink(String type, String cssClass, Object text, String tooltipSelector);

        @Template("<span style='cursor: pointer'>{0} <i class=\"{1} {2}\"></i></span>")
        SafeHtml textWithEdit(String content, String iconClass, String css);

        @Template("<a href='#logs:{0}' class='{1}'>{2}</a>" )
        SafeHtml viewLogsLink(long agentId, String cssClass, String caption);

		@Template("<a href='#statistics:{0}' class='{1}'>{2}</a>" )
		SafeHtml viewStatisticsLink(long agentId, String cssClass, String caption);

        @Template("<i class='{0} {1}' iconType='{0}'></i>")
        SafeHtml iconWithStyle(String icon, String cssClass);

        @Template("<div style='clear: both;'></div>")
        SafeHtml resetFloatDiv();
    }

    public interface Resources extends ClientBundle {

        @Source("table.css")
        Css css();
    }

    public interface Css extends CssResource {
        String tableIcon();

        String viewLogsLink();

        String downloadAgentIcon();

        String agentStatusMessage();
    }

    private static final LocalHtmlTemplates templates = GWT.create(LocalHtmlTemplates.class);

    private static final Resources localRes = GWT.create(Resources.class);

    private static final Css css = localRes.css();

    private AgentsView.AgentsPresenter presenter;

    static {
        css.ensureInjected();
    }

    public AgentsTable() {
        // STATUS
        AbstractSafeHtmlCell statusCell = new AbstractSafeHtmlCell<String>(SimpleSafeHtmlRenderer.getInstance()){
            @Override
            protected void render(Context context, SafeHtml data, SafeHtmlBuilder sb) {
                AgentDTO model = (AgentDTO)context.getKey();

	            sb.append(templates.messageWithLink(getLabelTypeByStatus(model.getStatus()).get(), css.agentStatusMessage(),
			            getLabelTextByStatus(model.getStatus()), getLabelTextByStatus(model.getStatus())));
	            makeAgentStatusTooltip(getLabelTextByStatus(model.getStatus()), getTooltipTextByStatus(model.getStatus()));
            }
        };
        Column<AgentDTO, String> statusColumn = new Column<AgentDTO, String>(statusCell) {
            @Override
            public String getValue(AgentDTO object) {
                return object.getStatus().toString();
            }
        };

        // NAME
        AbstractSafeHtmlCell nameEditIconDecorator = new AbstractSafeHtmlCell<String>(SimpleSafeHtmlRenderer.getInstance(), "click") {
            @Override
            protected void render(Context context, SafeHtml data, SafeHtmlBuilder sb) {
                String content = StrUtil.defaultIfBlank(data == null ? null : data.asString(), "[click to set name]");
                sb.append(templates.textWithEdit(content, IconType.PENCIL.get(), css.tableIcon()));
            }

            @Override
            public void onBrowserEvent(Context context, final Element parent, String value, NativeEvent event, ValueUpdater<String> valueUpdater) {
                super.onBrowserEvent(context, parent, value, event, valueUpdater);
                UiUtil.hideTooltip(Element.as(event.getEventTarget()));
                final AgentDTO model = (AgentDTO)context.getKey();
                final Element showTo = parent.getFirstChildElement();

                final NameEditor panel = new NameEditor(model.getName(), new NameEditor.NameEditorCallback() {
                    @Override
                    public void onNameEdited(String lastName, String newName) {
                        model.setName(newName);
                        presenter.onRenameAgent(model.getAgentId(), newName);
                    }
                });
                panel.setTitle(commonMessages.msgRenameAgentTitle());
                int left = showTo.getAbsoluteRight() - 145;
                int top = showTo.getAbsoluteTop() + 15;
                panel.showWithCoordinates(left, top);
            }
        };
        TooltipCellDecorator<String> nameTooltipDecorator = tooltipCellDecorator(nameEditIconDecorator, commonMessages
				.tooltipTextEdit(), Placement.RIGHT);
        Column<AgentDTO, String> nameColumn = new Column<AgentDTO, String>(nameTooltipDecorator) {
            @Override
            public String getValue(AgentDTO model) {
                return model.getName();
            }
        };

        // ACTIONS
        AbstractSafeHtmlCell actionsCell = new AbstractSafeHtmlCell<String>(SimpleSafeHtmlRenderer.getInstance(), "click") {
            @Override
            protected void render(Context context, SafeHtml data, SafeHtmlBuilder sb) {
                AgentDTO model = (AgentDTO)context.getKey();
                AgentStatus status = model.getStatus();

                sb.append(templates.viewLogsLink(model.getAgentId(), css.viewLogsLink(), commonMessages.linkViewLogs()));
                sb.append(templates.viewStatisticsLink(model.getAgentId(), css.viewLogsLink(), commonMessages.linkViewStatistics()));
				if (status.canBeDeleted()) {
                    sb.append(templates.iconWithStyle(IconType.TRASH.get(), css.downloadAgentIcon()));
                }
                if (status.canBeResumed()) {
                    sb.append(templates.iconWithStyle(IconType.PLAY.get(), css.downloadAgentIcon()));
                }
                else if (status.canBePaused()) {
                    sb.append(templates.iconWithStyle(IconType.PAUSE.get(), css.downloadAgentIcon()));
                }
                if (status.canBeDownloaded()) {
                    sb.append(templates.iconWithStyle(IconType.DOWNLOAD.get(), css.downloadAgentIcon()));
                }
                sb.append(templates.resetFloatDiv());

                addToolTip(IconType.TRASH, commonMessages.tooltipTextRemove());
                addToolTip(IconType.PLAY, commonMessages.tooltipTextStart());
                addToolTip(IconType.PAUSE, commonMessages.tooltipTextPause());
                addToolTip(IconType.DOWNLOAD, commonMessages.tooltipTextDownload());
            }

            @Override
            public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event, ValueUpdater valueUpdater) {
                super.onBrowserEvent(context, parent, value, event, valueUpdater);

                if (!Element.is(event.getEventTarget())) {
                    return;
                }
                Element element = Element.as(event.getEventTarget());
                if (!element.hasAttribute("iconType")) {
                    return;
                }
                UiUtil.hideTooltip(element);

                AgentDTO model = (AgentDTO)context.getKey();
                switch (UiUtil.getIconTypeByClass(element.getAttribute("iconType"))) {
                    case TRASH:
                        presenter.deleteAgent(model.getAgentId());
                        break;
                    case PLAY:
                        presenter.setAgentOnPause(false, model.getAgentId());
                        break;
                    case PAUSE:
                        presenter.setAgentOnPause(true, model.getAgentId());
                        break;
                    case DOWNLOAD:
                        presenter.downloadAgent(model.getAgentId(), model.getClientKey());
                        break;
                }
            }

            private void addToolTip(IconType icon, String text) {
                makeToolTip("i[iconType='" + icon.get() + "']", text);
            }
        };
        Column<AgentDTO, String> actionsColumn = new Column<AgentDTO, String>(actionsCell) {
            @Override
            public String getValue(AgentDTO model) {
                return "View Logs";
            }
        };

        addColumn(statusColumn, commonMessages.tblHeaderStatus());
        addColumn(nameColumn, commonMessages.tblHeaderAgentName());
        addColumn(actionsColumn, commonMessages.tblHeaderActions());
        setBordered(true);
        setStriped(true);
        setCondensed(true);
        setColumnWidth(statusColumn, 19.0, com.google.gwt.dom.client.Style.Unit.PCT);
        setColumnWidth(nameColumn, 31.0, com.google.gwt.dom.client.Style.Unit.PCT);
        setColumnWidth(actionsColumn, 50.0, com.google.gwt.dom.client.Style.Unit.PCT);
    }

    public void setPresenter(AgentsView.AgentsPresenter presenter) {
        this.presenter = presenter;
    }
}
