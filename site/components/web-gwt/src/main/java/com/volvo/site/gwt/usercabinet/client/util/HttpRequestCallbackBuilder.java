package com.volvo.site.gwt.usercabinet.client.util;

import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.volvo.platform.gwt.client.http.HttpRequestCallback;
import com.volvo.platform.gwt.client.http.HttpRequestSuccessCallback;
import com.volvo.platform.gwt.client.http.RequestFailure;
import com.volvo.platform.gwt.client.util.HasLoading;

import static com.volvo.platform.gwt.client.util.AlertUtil.alert;
import static com.volvo.site.gwt.usercabinet.client.AppWorkflow.getCommonMessages;

public final class HttpRequestCallbackBuilder {

    private HttpRequestCallbackBuilder(){}

    public static HttpRequestCallback appendHasLoadingResetToCallback(final HttpRequestCallback requestCallback,
                                                                      final HasLoading... loadings) {
        return new HttpRequestCallback() {
            @Override
            public void onFailure(RequestFailure failure) {
                try {
                    requestCallback.onFailure(failure);
                }
                finally {
                    for (HasLoading loading: loadings) {
                        loading.reset();
                    }
                }
            }

            @Override
            public void onSuccess(Request request, Response response) {
                try {
                    requestCallback.onSuccess(request, response);
                }
                finally {
                    for (HasLoading loading: loadings) {
                        loading.reset();
                    }
                }
            }
        };
    }

    public static HttpRequestCallback newCallbackWithDefaultErrorHandler(final HttpRequestSuccessCallback successCallback) {
        return new HttpRequestCallback() {
            @Override
            public void onSuccess(Request request, Response response) {
                successCallback.onSuccess(request, response);
            }

            @Override
            public void onFailure(RequestFailure failure) {
                if (failure.isAccessDenied()) {
                    Window.Location.assign("/login");
                }
                else {
                    alert(getCommonMessages().errServerCallTitle(), getCommonMessages().infoServerCallFailure(), AlertType.ERROR);
                }
            }
        };
    }
}
