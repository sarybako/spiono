package com.volvo.site.gwt.usercabinet.client.menu.view;

import com.google.gwt.user.client.ui.IsWidget;
import com.volvo.platform.gwt.client.util.HasLoading;

public interface ChangePasswordView extends IsWidget {

    void showModal();

    void close();

    void setPresenter(ChangePasswordPresenter presenter);

    HasLoading loadingState();

    void showIncorrectPassword();

    public interface ChangePasswordPresenter {
        void onSaveChangesButtonClicked(String oldPassword, String newPassword);
    }
}
