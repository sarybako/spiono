package com.volvo.site.mailing.templates;


import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PackageInfoTest {
    @Test
    public void packageNameTest()  {
        assertEquals(PackageInfo.PACKAGE_NAME, "com.volvo.site.mailing.templates");
        assertEquals(PackageInfo.PACKAGE_PATH, "com/volvo/site/mailing/templates");
    }
}
