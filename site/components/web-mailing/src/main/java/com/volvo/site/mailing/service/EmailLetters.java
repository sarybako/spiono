package com.volvo.site.mailing.service;

import com.volvo.platform.spring.email.EmailLetter;
import lombok.Getter;
import lombok.ToString;

@ToString
enum EmailLetters implements EmailLetter {

    REGISTRATION("registration"),
    FORGOT_PASSWORD("forgot_password");

    private static final String BODY_POSTFIX = "_body";

    private static final String SUBJECT_POSTFIX = "_header";

    private static final String FTL_POSTFIX = ".ftl";

    @Getter
    private final String name;

    @Getter
    private final String bodyPath;

    @Getter
    private final String subjectPath;

    EmailLetters(String name) {
        this.name = name;
        this.bodyPath = name + BODY_POSTFIX + FTL_POSTFIX;
        this.subjectPath = name + SUBJECT_POSTFIX + FTL_POSTFIX;
    }
}
