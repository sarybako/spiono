package com.volvo.site.mailing.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
final class ConfigFilePaths {

    public static final String CONF_DEV_PATH = "com/volvo/site/mailing/config/conf-development.properties";
    public static final String CONF_PROD_PATH = "com/volvo/site/mailing/config/conf-production.properties";
}
