package com.volvo.site.mailing.service;


import org.springframework.mail.SimpleMailMessage;

import java.util.Locale;

public interface EmailTemplateService {

    SimpleMailMessage registrationEmail(String to, String unique_key, Locale locale);

    SimpleMailMessage forgotPasswordEmail(String to, String unique_key, Locale locale);

}
