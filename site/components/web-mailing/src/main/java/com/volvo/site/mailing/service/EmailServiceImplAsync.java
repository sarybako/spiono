package com.volvo.site.mailing.service;

import com.volvo.platform.spring.qualifiers.AsyncImplementation;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
@AsyncImplementation
class EmailServiceImplAsync extends EmailServiceImplBase {
}
