package com.volvo.site.mailing.service;

import java.util.Locale;

public interface EmailService {

    void sendRegistrationEmail(String to, String unique_key, Locale locale);

    void sendForgotPasswordEmail(String to, String uniqueKey, Locale locale);
}
