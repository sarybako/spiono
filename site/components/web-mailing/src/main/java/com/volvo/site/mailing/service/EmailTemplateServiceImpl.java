package com.volvo.site.mailing.service;


import com.volvo.platform.spring.email.FreemarkerEmailTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.spring.email.FreemarkerUtils.loadEmailTemplate;
import static java.util.Collections.singletonMap;


@Service
class EmailTemplateServiceImpl implements EmailTemplateService {

    @Autowired
    private freemarker.template.Configuration configuration;

    @Override
    public SimpleMailMessage registrationEmail(String to, String unique_key, Locale locale) {
        return sendLetterWithUniqueKey(EmailLetters.REGISTRATION, to, unique_key, locale);
    }

    // todo: sergey(tests)
    @Override
    public SimpleMailMessage forgotPasswordEmail(String to, String unique_key, Locale locale) {
        return sendLetterWithUniqueKey(EmailLetters.FORGOT_PASSWORD, to, unique_key, locale);
    }

    private SimpleMailMessage sendLetterWithUniqueKey(EmailLetters letter, String to, String unique_key, Locale locale) {
        checkNotNull(letter);
        checkNotNull(to);
        checkNotNull(unique_key);

        Object model = singletonMap("unique_key", unique_key);
        FreemarkerEmailTemplate template = loadEmailTemplate(configuration, letter, locale);
        SimpleMailMessage message = template.process(model, new SimpleMailMessage());
        message.setTo(to);
        return message;
    }
}
