package com.volvo.site.mailing.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PackageInfo {
    public static final String PACKAGE_NAME = "com.volvo.site.mailing.service";
}
