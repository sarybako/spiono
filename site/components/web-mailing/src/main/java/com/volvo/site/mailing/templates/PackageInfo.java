package com.volvo.site.mailing.templates;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static org.springframework.util.ClassUtils.classPackageAsResourcePath;
import static org.springframework.util.ClassUtils.getPackageName;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

@NoArgsConstructor(access = AccessLevel.NONE)
public class PackageInfo {

    public static final String PACKAGE_NAME = getPackageName(PackageInfo.class);

    public static final String PACKAGE_PATH = classPackageAsResourcePath(PackageInfo.class);

    public static final String PACKAGE_CLASSPATH = CLASSPATH_URL_PREFIX + PACKAGE_PATH;
}
