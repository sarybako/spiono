package com.volvo.site.mailing.config;

import com.volvo.platform.spring.conf.ProfileProduction;
import com.volvo.platform.spring.email.conf.SmtpMailSenderBase;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

@Configuration
@PropertySource(CLASSPATH_URL_PREFIX + ConfigFilePaths.CONF_PROD_PATH)
@ProfileProduction
public class ProdMailSenderConfig extends SmtpMailSenderBase {
}
