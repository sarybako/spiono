package com.volvo.site.mailing.config;

import static org.springframework.util.ClassUtils.getPackageName;

public class PackageInfo {

    public static final String PACKAGE_NAME = getPackageName(PackageInfo.class);
}
