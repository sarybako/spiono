Hello,

If you want to confirm your registration on ${sitename}, please, open this url during next 48 hours:
http://${hostname}/register/registration-confirm/${unique_key}/

if this registration on ${sitename} is something mistake or spam, ignore this letter, and registration request will be deleted automatically afrer 48 hours.



This letter was sent by a robot, please, do not reply.

Thank you,
${sitename} team.