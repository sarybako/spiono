Hello,

You have requested the password recovery on ${sitename}.

To recover a lost password, please open following url during next 48 hours:
http://${hostname}/restore/change-password/${unique_key}/

If the above link does not open, copy it to the clipboard paste in the address bar and press enter.
This letter was sent by a robot, please, do not reply.

Thank you,
${sitename} team.