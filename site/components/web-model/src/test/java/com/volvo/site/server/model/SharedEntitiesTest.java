package com.volvo.site.server.model;

import org.springframework.util.SerializationUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.Entity;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import static com.volvo.platform.java.JavaUtils.getClassesFromPackageWithAnnotation;
import static org.apache.commons.lang3.reflect.ConstructorUtils.invokeConstructor;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SharedEntitiesTest {

    private Set<Class<?>> entities;

    @BeforeClass
    public void beforeClass() {
        entities = getClassesFromPackageWithAnnotation("com.volvo.site.server.model.entity", Entity.class);
        assertFalse(entities.isEmpty());
    }

    @Test
    public void testAllEntitiesAreSerializable() throws IOException {
        for (Class<?> clazz : entities) {
            assertTrue(Serializable.class.isAssignableFrom(clazz), "Entity class is not serializable: " + clazz);
        }
    }

    @Test
    public void invokeNullArgsContructor() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        for (Class<?> clazz : entities) {
            invokeConstructor(clazz);
        }
    }

    @Test
    public void serizalizeTest() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        for (Class<?> clazz : entities) {
            Serializable entity = (Serializable) invokeConstructor(clazz);
            byte[] serialized = SerializationUtils.serialize(entity);
            SerializationUtils.deserialize(serialized);
        }
    }

    @Test
    public void toStringRecursionTest() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        for (Class<?> clazz : entities) {
            invokeConstructor(clazz).toString();
        }
    }

    @Test
    public void equalsAndHashcodeRecursionTest() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        for (Class<?> clazz : entities) {
            Object entity = invokeConstructor(clazz);
            entity.equals(invokeConstructor(clazz));
            entity.hashCode();
        }
    }
}
