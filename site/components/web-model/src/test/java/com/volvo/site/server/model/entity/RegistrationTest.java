package com.volvo.site.server.model.entity;

import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static com.volvo.platform.java.testing.TestUtils.*;
import static org.testng.Assert.*;

public class RegistrationTest {

    @Test(expectedExceptions = NullPointerException.class)
    public void of1NpeTest1() {
        Registration.of(null, randomPwd());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of1NpeTest2() {
        Registration.of(randomEmail(), null);
    }

    @Test
    public void of1Test() {
        String email = randomEmail();
        String pwd = randomPwd();
        Registration registration = Registration.of(email, pwd);

        assertNull(registration.getId());
        assertEquals(registration.getEmail(), email);
        assertEquals(registration.getPassword(), pwd);
        assertNull(registration.getCrDate());
        assertNull(registration.getStatus());
        assertValidUUID(registration.getUniqueKey());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of2NpeTest1() {
        Registration.of(null, randomPwd(), randomUUIDAsString());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of2NpeTest2() {
        Registration.of(randomEmail(), null, randomUUIDAsString());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void of2NpeTest3() {
        Registration.of(randomEmail(), randomPwd(), null);
    }

    @Test
    public void of2Test() {
        String email = randomEmail();
        String pwd = randomPwd();
        String uuid = randomUUIDAsString();
        Registration registration = Registration.of(email, pwd, uuid);

        assertNull(registration.getId());
        assertEquals(registration.getEmail(), email);
        assertEquals(registration.getPassword(), pwd);
        assertNull(registration.getCrDate());
        assertNull(registration.getStatus());
        assertEquals(registration.getUniqueKey(), uuid);
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        Registration registration = Registration.of(randomEmail(), randomPwd());
        assertNull(registration.getCrDate());
        assertNull(registration.getStatus());

        invokePrivateMethod(Registration.class, registration, "prePersist");

        assertNotNull(registration.getCrDate());
        assertEquals(registration.getStatus(), Registration.RegistrationStatus.PENDING);
    }

    @Test
    public void preUpdateTest() throws InvocationTargetException, IllegalAccessException {
        Registration registration = Registration.of(randomEmail(), randomPwd());
        assertNull(registration.getCrDate());
        invokePrivateMethod(Registration.class, registration, "preUpdate");
        assertNotNull(registration.getCrDate());
    }

    @Test
    public void resetCreationDate() {
        Registration registration = Registration.of(randomEmail(), randomPwd());
        assertNull(registration.getCrDate());
        registration.resetCreationDate();
        assertNotNull(registration.getCrDate());
    }
}
