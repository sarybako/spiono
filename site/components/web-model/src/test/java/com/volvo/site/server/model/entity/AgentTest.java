package com.volvo.site.server.model.entity;


import com.volvo.site.server.model.ModelConstants;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static com.volvo.platform.java.JavaUtils.setPrivateFieldValue;
import static org.joda.time.DateTime.now;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class AgentTest {

    @Mock
    private User user;

    @Mock
    private ClientInstaller clientInstaller;

    @BeforeClass
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void ofTest() {
        Agent agent = Agent.of(user, clientInstaller);
        assertEquals(agent.getUser(), user);
        assertEquals(agent.getClientInstaller(), clientInstaller);
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        Agent agent = Agent.of(user, clientInstaller);

        invokePrivateMethod(Agent.class, agent, "prePersist");

        assertEquals(agent.getStatus(), Agent.AgentStatus.AGENT_CREATED);
        assertEquals(ModelConstants.clientInstallerKeyLen, agent.getClientKey().length());
    }

    @Test
    public void generateCertificateTest() {
        Agent agent = Agent.of(user, clientInstaller);

        assertNull(agent.getCertificate());
        agent.generateCertificate();

        String certificate = agent.getCertificate();
        assertEquals(certificate.length(), ModelConstants.agentCertificateLen);

        agent.generateCertificate();
        assertEquals(agent.getCertificate(), certificate);
    }

    @Test
    public void updateHeartBeatTimeTest() {
        Agent agent = Agent.of(user, clientInstaller);
        assertNull(agent.getHearbeatTime());

        agent.updateHeartBeatTime();

        assertTrue(agent.getHearbeatTime().getTime() <= System.currentTimeMillis());
    }

    @Test
    public void hasHeartBeatsAtTheLastMinuteTest() throws IllegalAccessException {
        Agent agent = Agent.of(user, clientInstaller);
        assertFalse(agent.hasHeartBeatsAtTheLastMinute());

        agent.updateHeartBeatTime();
        assertTrue(agent.hasHeartBeatsAtTheLastMinute());

        setPrivateFieldValue(Agent.class, agent, "hearbeatTime", now().minusSeconds(61).toDate());
        assertFalse(agent.hasHeartBeatsAtTheLastMinute());
    }

    @Test
    public void isDeletedTest() {
        Agent agent = Agent.of(user, clientInstaller);
        for (Agent.AgentStatus status: Agent.AgentStatus.values()) {
            agent.setStatus(status);
            assertEquals(agent.isDeleted(), Agent.AgentStatus.AGENT_DELETED.equals(status));
        }
    }
}
