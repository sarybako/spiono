package com.volvo.site.server.model.entity;

import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

import static com.volvo.platform.java.JavaUtils.invokePrivateMethod;
import static com.volvo.platform.java.testing.TestUtils.assertValidUUID;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class ForgotPasswordTest {

    @Mock
    private User user;

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void ofTest() {
        ForgotPassword fp = ForgotPassword.of(user);
        assertNull(fp.getId());
        assertNull(fp.getCrDate());
        assertNull(fp.getPwdResetDate());
        assertValidUUID(fp.getUniqueKey());
        assertEquals(fp.getUserId(), 0);
        assertEquals(fp.getUser(), user);
        assertEquals(fp.getStatus(), ForgotPassword.ForgotPasswordStatus.CREATED);
    }

    @Test
    public void prePersistTest() throws InvocationTargetException, IllegalAccessException {
        ForgotPassword fp = ForgotPassword.of(user);
        fp.setStatus(ForgotPassword.ForgotPasswordStatus.PWD_CHANGED);

        assertNull(fp.getCrDate());
        assertEquals(fp.getStatus(), ForgotPassword.ForgotPasswordStatus.PWD_CHANGED);
        String oldKey = fp.getUniqueKey();
        assertValidUUID(oldKey);

        invokePrivateMethod(ForgotPassword.class, fp, "prePersist");

        assertNotNull(fp.getCrDate());
        assertEquals(fp.getStatus(), ForgotPassword.ForgotPasswordStatus.CREATED);
        String newKey = fp.getUniqueKey();
        assertValidUUID(newKey);
        assertNotEquals(oldKey, newKey);
    }

    @Test
    public void resetCreationDateTest() {
        ForgotPassword fp = ForgotPassword.of(user);
        assertNull(fp.getCrDate());
        fp.resetCreationDate();
        assertNotNull(fp.getCrDate());
    }
}
