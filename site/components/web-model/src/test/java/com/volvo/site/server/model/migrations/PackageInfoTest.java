package com.volvo.site.server.model.migrations;

import com.google.common.base.Predicate;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.testng.annotations.Test;

import javax.annotation.Nullable;
import java.util.Set;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;

public class PackageInfoTest {

    @Test
    public void findScriptsTest() {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(PackageInfo.PACKAGE_NAME))
                .setScanners(new ResourcesScanner()));
        Set<String> resources = reflections.getResources(new Predicate<String>() {
            @Override
            public boolean apply(@Nullable String input) {
                if (!input.endsWith(".sql")) {
                    return false;
                }

                if (!input.startsWith("V")) {
                    fail("Invalid sql script name: " + input);
                }

                int __indexOf = input.indexOf("__");
                if (__indexOf < 0) {
                    fail("Invalid sql script name: " + input);
                }

                String number = input.substring(1, __indexOf);
                Integer.parseInt(number);
                return true;
            }
        });

        assertFalse(resources.isEmpty());
    }
}
