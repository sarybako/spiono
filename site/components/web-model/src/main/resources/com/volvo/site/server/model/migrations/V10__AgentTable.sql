CREATE TABLE `agent_installer` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`agent` MEDIUMBLOB NOT NULL,
	`cr_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`active` BIT NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `active` (`active`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;