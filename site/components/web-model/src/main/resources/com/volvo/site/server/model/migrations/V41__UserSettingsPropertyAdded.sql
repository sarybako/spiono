ALTER TABLE `user_settings`
	ADD COLUMN `logsview_selected_agent_id` INT(11) NULL DEFAULT '-1' AFTER `agentsview_agent_status`;

ALTER TABLE `user_settings`
	CHANGE COLUMN `agentsview_quick_search` `agentsview_quick_search` VARCHAR(50) NULL DEFAULT '' AFTER `user_id`,
	CHANGE COLUMN `agentsview_agent_status` `agentsview_agent_status` VARCHAR(20) NULL DEFAULT '' AFTER `agentsview_quick_search`;