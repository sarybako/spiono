ALTER TABLE `agent` 
 CHANGE COLUMN `hearbeat_time` `hearbeat_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `certificate`; 

UPDATE `agent`
SET agent.is_on_pause=0 WHERE agent.is_on_pause is null; 