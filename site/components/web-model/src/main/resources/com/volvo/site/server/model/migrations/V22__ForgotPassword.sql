CREATE TABLE `forgot_pwd` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`cr_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`pwd_reset_date` TIMESTAMP NULL DEFAULT NULL,
	`status` VARCHAR(50) NOT NULL,
	`unique_key` VARCHAR(50) NOT NULL,
	`user` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `unique_key_2` (`unique_key`),
	INDEX `unique_key` (`unique_key`),
	INDEX `user_id_fk` (`user`),
	CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
