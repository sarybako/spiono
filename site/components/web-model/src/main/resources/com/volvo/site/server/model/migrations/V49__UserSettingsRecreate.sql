DROP TABLE `user_settings`;

CREATE TABLE `user_settings` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`last_logs_search_phrase` VARCHAR(50) NULL DEFAULT NULL,
	`last_logs_search_agent_status` VARCHAR(50) NULL DEFAULT NULL,
	`last_logs_search_agent_id` INT(11) NULL DEFAULT NULL,
	`last_stat_agent_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_last_logs_search_agent_id` FOREIGN KEY (`last_logs_search_agent_id`) REFERENCES `agent` (`id`) ON UPDATE NO ACTION ON DELETE SET NULL,
	CONSTRAINT `FK_last_stat_agent_id` FOREIGN KEY (`last_stat_agent_id`) REFERENCES `agent` (`id`) ON UPDATE NO ACTION ON DELETE SET NULL
)
COLLATE=`utf8_general_ci`
ENGINE=InnoDB;

ALTER TABLE `user_settings`
	ADD COLUMN `user_id` INT(11) NOT NULL AFTER `id`,
	ADD CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
