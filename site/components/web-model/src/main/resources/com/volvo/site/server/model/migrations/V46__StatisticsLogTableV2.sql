ALTER TABLE `statistics_log`
	ALTER `start_date` DROP DEFAULT,
	ALTER `end_date` DROP DEFAULT;
ALTER TABLE `statistics_log`
	CHANGE COLUMN `start_date` `start_date` TIMESTAMP NOT NULL AFTER `agent_id`,
	CHANGE COLUMN `end_date` `end_date` TIMESTAMP NOT NULL AFTER `start_date`;