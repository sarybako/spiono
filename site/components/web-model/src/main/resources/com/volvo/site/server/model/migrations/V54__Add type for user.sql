ALTER TABLE `users`
	ADD COLUMN `type` VARCHAR(20) NOT NULL AFTER `password`,
	ADD INDEX `type` (`type`);

update users us set us.type = "CORE_SPIONO" where us.id not in (select vk.spiono_user from vk_users vk);
update users us set us.type = "VK_USER" where us.id in (select vk.spiono_user from vk_users vk);