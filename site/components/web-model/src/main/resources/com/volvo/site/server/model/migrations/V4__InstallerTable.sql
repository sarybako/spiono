CREATE TABLE `client_installer` (
	`id` INT(11) NOT NULL,
	`installer` MEDIUMBLOB NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;