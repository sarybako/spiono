ALTER TABLE `statistics_log`
	ALTER `process_name` DROP DEFAULT;
ALTER TABLE `statistics_log`
	CHANGE COLUMN `process_name` `process_name` VARCHAR(100) NOT NULL AFTER `end_date`;

ALTER TABLE `statistics_log`
	ADD COLUMN `program_name` VARCHAR(100) NULL AFTER `process_name`;