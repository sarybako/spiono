CREATE TABLE `key_log` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`agent_id` INT(11) NOT NULL,
	`text` VARCHAR(150) NOT NULL,
	`start_date` TIMESTAMP NOT NULL,
	`end_date` TIMESTAMP NOT NULL,
	`hwnd` INT NOT NULL,
	`window_text` VARCHAR(100) NULL,
	`process_name` VARCHAR(100) NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;
