CREATE TABLE `vk_users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`vk_user_id` BIGINT(20) NOT NULL,
	`spiono_user` INT(11) NOT NULL,
	`expires_in` INT(10) NOT NULL,
	`access_token` VARCHAR(100) NOT NULL,
	`cr_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`user_name` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `spiono_user` (`vk_user_id`),
	CONSTRAINT `fk_vkuser_to_user_id` FOREIGN KEY (`spiono_user`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
