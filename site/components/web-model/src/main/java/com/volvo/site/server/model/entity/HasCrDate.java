package com.volvo.site.server.model.entity;


import java.util.Date;

public interface HasCrDate {

    Date getCrDate();
}
