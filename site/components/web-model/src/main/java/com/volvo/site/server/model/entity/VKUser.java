package com.volvo.site.server.model.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import static com.volvo.platform.spring.BCryptUtil.bcryptHash;

@Entity
@Table(name = "vk_users")
@NoArgsConstructor()
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class VKUser implements Serializable {

    private static final String VK_EMAIL_SUFFIX = "@vk.com";

    public static VKUser of(long userId, int expiresIn, String accessToken){
        VKUser vkUser = new VKUser();
        vkUser.setVkUserId(userId);
        vkUser.setExpiresIn(expiresIn);
        vkUser.setAccessToken(accessToken);

        User newUser = User.of(String.valueOf(vkUser.getVkUserId()) + VK_EMAIL_SUFFIX, accessToken, User.UserType.VK_USER);
        newUser.setActive(true);
        vkUser.setSpionoUser(newUser);

        return vkUser;
    }

    public static void update(VKUser vkUser, int expiresIn, String accessToken){
        vkUser.getSpionoUser().setPassword(bcryptHash(accessToken));
        vkUser.updateAccessToken(accessToken);
        vkUser.setExpiresIn(expiresIn);
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    private Long vkUserId;

    @Column
    @NotNull
    private Integer expiresIn; // in seconds

    @Column(length = 300)
    @NotNull
    private String accessToken;

    @Column
    private String userName;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @OneToOne(cascade = CascadeType.ALL)
    private User spionoUser;

    public void updateAccessToken(String newAccessToken){
        this.accessToken = newAccessToken;
    }

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
