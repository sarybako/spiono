package com.volvo.site.server.model.entity;

import com.volvo.site.server.model.ModelConstants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public interface Installer extends HasCrDate, HasLongId {
    @NotNull
    @Size(min = 1, max = ModelConstants.mysqlMediumBlobMaxSize)
    byte[] getData();

    @NotNull
    @Size(min = 1, max = ModelConstants.mysqlMediumBlobMaxSize)
    void setData(byte[] data);

    boolean isActive();

    void setActive(boolean active);
}
