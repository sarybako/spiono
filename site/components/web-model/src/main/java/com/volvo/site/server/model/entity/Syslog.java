package com.volvo.site.server.model.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "syslog")
@NoArgsConstructor(access = AccessLevel.NONE)
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Syslog  implements Serializable {

    public static Syslog of(String text){
        Syslog syslog = new Syslog();
        syslog.setText(text);
        return syslog;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date date;

    @Column(length = 1000, nullable = false)
    @NotNull
    private String text;

    @PrePersist
    private void prePersist() {
        date = new Date();
    }

}
