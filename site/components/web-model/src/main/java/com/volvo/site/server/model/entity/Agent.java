package com.volvo.site.server.model.entity;

import com.volvo.site.server.model.ModelConstants;
import lombok.*;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static com.volvo.platform.java.JavaUtils.newArrayListIfNull;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Entity
@Table(name = Agent.TABLE_NAME)
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"user", "keyLogs"})
@EqualsAndHashCode(exclude = {"user","agentInstaller","clientInstaller", "keyLogs"})
public class Agent implements Serializable {

    public static final String TABLE_NAME = "agent";

    public static Agent of(User user, ClientInstaller clientInstaller){
        Agent agent = new Agent();
        agent.setUser(user);
        agent.setClientInstaller(clientInstaller);
        return agent;
    }

    public Agent(Long id, String name, AgentStatus status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    @Size(max = 50)
    private String name;

    @Column(name = "user", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    private long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private AgentInstaller agentInstaller;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private ClientInstaller clientInstaller;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AgentStatus status;

    @Column(nullable = false, length = 25)
    @Size(min = ModelConstants.clientInstallerKeyLen,max = ModelConstants.clientInstallerKeyLen)
    @Setter(AccessLevel.NONE)
    private String clientKey;

    @Column(nullable = true, length = 100)
    @Size(min = ModelConstants.agentCertificateLen, max = ModelConstants.agentCertificateLen)
    @Setter(AccessLevel.NONE)
    private String certificate;

    @Column(nullable = true)
    @Setter(AccessLevel.NONE)
    private Date hearbeatTime;

    @Column(nullable = true)
    private boolean isOnPause;

    @OneToMany(targetEntity = KeyLog.class, fetch = FetchType.LAZY, mappedBy = "agent")
    @Setter(AccessLevel.NONE)
    private List<KeyLog> keyLogs;

    @PrePersist
    private void prePersist() {
        status = AgentStatus.AGENT_CREATED;
        clientKey = randomAlphanumeric(ModelConstants.clientInstallerKeyLen);
    }

    public void generateCertificate() {
        if (certificate == null) {
            certificate = randomAlphabetic(ModelConstants.agentCertificateLen);
        }
    }

    public void updateHeartBeatTime() {
        hearbeatTime = new Date();
    }

    public boolean hasHeartBeatsAtTheLastMinute() {
        if (hearbeatTime == null) {
            return false;
        }
        return DateTime.now().minusMinutes(1).getMillis() < hearbeatTime.getTime();
    }

    public boolean isDeleted() {
        return AgentStatus.AGENT_DELETED.equals(status);
    }

    public List<KeyLog> getKeyLogs() {
        return keyLogs = newArrayListIfNull(keyLogs);
    }

    public enum AgentStatus {
        AGENT_CREATED,
        AGENT_DOWNLOADED,
        AGENT_REGISTERED,
        AGENT_DELETED
    }
}
