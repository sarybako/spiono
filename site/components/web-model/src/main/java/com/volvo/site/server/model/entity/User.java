package com.volvo.site.server.model.entity;

import com.volvo.platform.spring.BCryptUtil;
import com.volvo.site.server.model.ModelConstants;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.java.JavaUtils.newArrayListIfNull;
import static com.volvo.platform.spring.BCryptUtil.bcryptHash;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class User implements Serializable {

    public static User of(Registration registration) {
        checkNotNull(registration);
        return of(registration.getEmail(), registration.getPassword());
    }

    public static User of(String email, String password) {
        return of(email, password, UserType.CORE_SPIONO);
    }

    public static User of(String email, String password, UserType userType) {
        User user = new User();
        user.setEmail(checkNotNull(email));
        user.setPassword(bcryptHash(password));
        user.setType(userType);
        return user;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 50, nullable = false)
    @Email
    @NotNull
    @Size(max= ModelConstants.emailMaxLength)
    private String email;

    @Column(length = 65, nullable = false)
    @NotNull
    @Size(min = ModelConstants.bCryptHashSize, max = ModelConstants.bCryptHashSize)
    private String password;

    @Column(nullable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @Column(nullable = false)
    private boolean active;

    @Column(nullable = false)
    @Min(0)
    @Setter(AccessLevel.NONE)
    private long errLogins;

    @OneToMany(targetEntity = ForgotPassword.class, fetch = FetchType.LAZY, mappedBy = "user")
    @Setter(AccessLevel.NONE)
    private List<ForgotPassword> listOfForgotPasswordRequests;

    @OneToMany(targetEntity = Agent.class, fetch = FetchType.LAZY, mappedBy = "user")
    @Setter(AccessLevel.NONE)
    private List<Agent> listOfAgents;

    @OneToOne(mappedBy="spionoUser")
    public VKUser vkUser;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserType type;

    @PrePersist
    private void prePersist() {
        resetErrLogins();
        crDate = new Date();
        normalizeEmail();
    }

    @PreUpdate
    private void preUpdate() {
        normalizeEmail();
    }

    private void normalizeEmail() {
        email = email.toLowerCase();
    }

    public boolean addForgotPassword(ForgotPassword forgotPassword){
        checkNotNull(forgotPassword);
        return getForgotPasswords().add(forgotPassword);
    }

    public List<ForgotPassword> getForgotPasswords(){
        return listOfForgotPasswordRequests = newArrayListIfNull(listOfForgotPasswordRequests);
    }

    public boolean addAgent(Agent agent){
        checkNotNull(agent);
        return getAgents().add(agent);
    }

    public List<Agent> getAgents(){
        return listOfAgents = newArrayListIfNull(listOfAgents);
    }

    public long incrementErrLogins() {
        return ++errLogins;
    }

    public void resetErrLogins() {
        errLogins = 0;
    }

    public boolean tryUpdatePassword(String oldPassword, String newPassword) {
        checkNotNull(oldPassword);
        checkNotNull(newPassword);

        if (BCrypt.checkpw(oldPassword, password)) {
            password = BCryptUtil.bcryptHash(newPassword);
            return true;
        }
        return false;
    }

    public enum UserType {
        CORE_SPIONO,
        VK_USER,
        FACEBOOK_USER,
        TWITTER_USER
    }
}
