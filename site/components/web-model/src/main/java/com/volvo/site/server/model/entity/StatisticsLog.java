package com.volvo.site.server.model.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.*;
import java.util.*;

import static com.volvo.site.server.model.ModelConstants.*;

@Entity
@Data
@EqualsAndHashCode(exclude = {"agentId", "startDate", "endDate", "processName", "processDescription", "timeOfUse"})
public class StatisticsLog  implements Serializable {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private long agentId;

	@Column(nullable = false)
	@NotNull
	private Date startDate;

	@Column(nullable = false)
	@NotNull
	private Date endDate;

	@Column(nullable = false)
	private long timeOfUse;

	@Column(nullable = false)
	@Size(max = spyItemMaxWindowTextLen)
	private String processName;

	@Column(nullable = true)
	@Size(max = spyItemMaxProcessDescriptionTextLen)
	private String processDescription;
}
