package com.volvo.site.server.model.entity;


import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name=AgentInstaller.TABLE_NAME)
@Data
@NoArgsConstructor
public class AgentInstaller implements Serializable, Installer {

    public static final String TABLE_NAME = "agent_installer";

    public static AgentInstaller of(byte[] data) {
        return of(data, null);
    }

    public static AgentInstaller of(byte[] data, ClientInstaller clientInstaller) {
        return of(data, clientInstaller, false);
    }

    public static AgentInstaller of(byte[] data, ClientInstaller clientInstaller, boolean isActive) {
        AgentInstaller installer = new AgentInstaller();
        installer.setData(data);
        installer.setClientInstaller(clientInstaller);
        installer.setActive(isActive);
        return installer;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private byte[] data;

    @Column(nullable = false, insertable = false)
    @Setter(AccessLevel.NONE)
    private Date crDate;

    @Column(nullable = false)
    private boolean active;

    @Column(name = "client_installer", insertable = false, updatable = false)
    @Setter(AccessLevel.NONE)
    private long clientInstallerId;

    @ManyToOne(targetEntity = ClientInstaller.class, fetch = FetchType.LAZY)
    @NotNull
    private ClientInstaller clientInstaller;

    @PrePersist
    private void prePersist() {
        crDate = new Date();
    }
}
