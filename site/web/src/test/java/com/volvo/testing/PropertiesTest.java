package com.volvo.testing;


import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.util.ResourceUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.util.Collection;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.platform.java.PropertiesUtils.undecorateProperty;
import static com.volvo.platform.java.testing.TestUtils.getStaticFinalStringValues;
import static com.volvo.platform.java.testing.TestUtils.isStringsAreUnique;
import static java.util.Collections.unmodifiableCollection;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public abstract class PropertiesTest<T> {

    private final Class<T> clazz;

    private final String[] filesToTest;

    private final Collection<String> properties = newArrayList();

    public PropertiesTest(Class<T> clazz, String ... filesToTest) {
        this.clazz = clazz;
        this.filesToTest = filesToTest;
    }

    @Test
    public void testPropertiesListNotEmpty() {
        assertFalse(properties.isEmpty());
    }

    @Test
    public void testPropertiesAreUnique() {
        assertTrue(isStringsAreUnique(getProperties()));
    }

    @Test
    public void testPropertiesAreExistsInFiles() throws FileNotFoundException, ConfigurationException {
        for (String file: filesToTest) {
            checkPropertiesAreExistsInFile(file);
        }
    }

    @BeforeClass
    protected void beforeClass() {
        for (String property: getStaticFinalStringValues(clazz)) {
            properties.add(undecorateProperty(property));
        }
    }

    protected Collection<String> getProperties() {
        return unmodifiableCollection(properties);
    }

    protected static Configuration getConfiguration(String fileName) throws FileNotFoundException, ConfigurationException {
        return new PropertiesConfiguration(ResourceUtils.getFile(fileName));
    }

    private void checkPropertiesAreExistsInFile(String fileName) throws FileNotFoundException, ConfigurationException {
        Configuration configuration = getConfiguration(fileName);
        for (String property: getProperties()) {
            assertTrue(configuration.containsKey(property));
        }
    }
}
