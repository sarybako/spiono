package com.volvo.testing.others;


import org.springframework.security.crypto.bcrypt.BCrypt;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class BCryptTest {

    @Test
    public void testBCrypt() {
        String hash = BCrypt.hashpw("password", BCrypt.gensalt());
        assertTrue(BCrypt.checkpw("password", hash));
    }
}
