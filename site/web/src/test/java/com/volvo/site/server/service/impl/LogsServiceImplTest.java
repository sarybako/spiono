package com.volvo.site.server.service.impl;


import com.google.common.base.Preconditions;
import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.util.LuceneLogsSearchBuilder;
import org.apache.lucene.search.Query;
import org.hibernate.search.SearchFactory;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.EntityContext;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.dsl.QueryContextBuilder;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.security.util.FieldUtils.setProtectedFieldValue;
import static org.springframework.test.util.ReflectionTestUtils.invokeMethod;
import static org.testng.Assert.assertEquals;

@PrepareForTest({
        LogsServiceImpl.class,
        Preconditions.class,
        Search.class,
        Collections.class,
        LuceneLogsSearchBuilder.class
})
public class LogsServiceImplTest extends PowerMockTestCase {

    @Mock
    EntityManager entityManager;

    @Mock
    AgentJpaRepository agentJpaRepository;

    @Mock
    KeyLogJpaRepository keyLogJpaRepository;

    @Mock
    LuceneLogsSearchBuilder luceneLogsSearchBuilder;

    @Mock
    GetKeyLogDataDTO filter;

    LogsServiceImpl logsService;

    @BeforeMethod
    public void beforeMethod() {
        initMocks(this);

        logsService = PowerMockito.spy(new LogsServiceImpl(agentJpaRepository, keyLogJpaRepository));
        setProtectedFieldValue("entityManager", logsService, entityManager);

        PowerMockito.mockStatic(Preconditions.class);
        PowerMockito.mockStatic(LogsServiceImpl.class);
        PowerMockito.mockStatic(Collections.class);
    }

    @Test
    public void searchTest() throws Exception {
        // Given
        FullTextEntityManager fullTextEntityManager = mock(FullTextEntityManager.class);

        PowerMockito.mockStatic(Search.class);
        PowerMockito.when(Search.getFullTextEntityManager(entityManager)).thenReturn(fullTextEntityManager);

        SearchFactory searchFactory = mock(SearchFactory.class);
        when(fullTextEntityManager.getSearchFactory()).thenReturn(searchFactory);

        QueryContextBuilder queryContextBuilder = mock(QueryContextBuilder.class);
        when(searchFactory.buildQueryBuilder()).thenReturn(queryContextBuilder);

        EntityContext entityContext = mock(EntityContext.class);
        when(queryContextBuilder.forEntity(KeyLog.class)).thenReturn(entityContext);

        QueryBuilder queryBuilder = mock(QueryBuilder.class);
        when(entityContext.get()).thenReturn(queryBuilder);

        Query query = mock(Query.class);
        PowerMockito.when(logsService, "createSearchQuery", queryBuilder, filter).thenReturn(query);

        FullTextQuery fullTextQuery = mock(FullTextQuery.class);
        when(fullTextEntityManager.createFullTextQuery(query, KeyLog.class)).thenReturn(fullTextQuery);

        List<KeyLog> expectedResult = mock(List.class);
        when(fullTextQuery.getResultList()).thenReturn(expectedResult);

        // When
        List<KeyLog> result = logsService.search(filter);

        // Then
        assertEquals(result, expectedResult);

        PowerMockito.verifyStatic();
        invokeMethod(logsService, "setPaginationForQuery", fullTextQuery, filter);
        Collections.reverse(expectedResult);
    }

    @Test
    public void createSearchQueryTest() throws Exception {
        // Given
        QueryBuilder queryBuilder = mock(QueryBuilder.class);
        PowerMockito.whenNew(LuceneLogsSearchBuilder.class).withArguments(queryBuilder).thenReturn(
                luceneLogsSearchBuilder);

        Query query = mock(Query.class);
        when(luceneLogsSearchBuilder.build()).thenReturn(query);

        // When
        Object result = callLogsServiceRealMethod("createSearchQuery", queryBuilder, filter);

        // Then
        assertEquals(result, query);

        PowerMockito.verifyStatic();
        invokeMethod(logsService, "addAgentFilter", luceneLogsSearchBuilder, filter);
        invokeMethod(logsService, "addPhraseFilter", luceneLogsSearchBuilder, filter);
        invokeMethod(logsService, "addStartDateFilter", luceneLogsSearchBuilder, filter);
        invokeMethod(logsService, "addEndDateFilter", luceneLogsSearchBuilder, filter);
    }

    @Test
    public void addAgentFilterTestWithAllAgents() throws Exception {
        // Given
        when(filter.getAgentId()).thenReturn(GetKeyLogDataDTO.ALL_AGENTS);

        // When
        callLogsServiceRealMethod("addAgentFilter", luceneLogsSearchBuilder, filter);

        // Then
        verifyZeroInteractions(luceneLogsSearchBuilder);
    }

    @Test
    public void addAgentFilterTestWithSpecifiedAgent() throws Exception {
        // Given
        when(filter.getAgentId()).thenReturn(5L);

        // When
        callLogsServiceRealMethod("addAgentFilter", luceneLogsSearchBuilder, filter);

        // Then
        verify(luceneLogsSearchBuilder).withAgent(anyLong());
    }

    @Test
    public void addPhraseFilterWithNullPhraseTest() throws Exception {
        // Given
        when(filter.getSearchingString()).thenReturn(null);

        // When
        callLogsServiceRealMethod("addPhraseFilter", luceneLogsSearchBuilder, filter);

        // Then
        verifyZeroInteractions(luceneLogsSearchBuilder);
    }

    @Test
    public void addPhraseFilterWithBlankPhraseTest() throws Exception {
        // Given
        when(filter.getSearchingString()).thenReturn("");

        // When
        callLogsServiceRealMethod("addPhraseFilter", luceneLogsSearchBuilder, filter);

        // Then
        verifyZeroInteractions(luceneLogsSearchBuilder);
    }

    @Test
    public void addPhraseFilterTest() throws Exception {
        // Given
        when(filter.getSearchingString()).thenReturn("searchPhrase");

        // When
        callLogsServiceRealMethod("addPhraseFilter", luceneLogsSearchBuilder, filter);

        // Then
        verify(luceneLogsSearchBuilder).withSearchString("searchPhrase");
    }

    @Test
    public void addStartDateFilterIfDateIsNullTest() throws Exception {
        // Given
        when(filter.getStartDate()).thenReturn(null);

        // When
        callLogsServiceRealMethod("addStartDateFilter", luceneLogsSearchBuilder, filter);

        // Then
        verifyZeroInteractions(luceneLogsSearchBuilder);
    }

    @Test
    public void addStartDateFilterTest() throws Exception {
        // Given
        Date date = new Date();
        when(filter.getStartDate()).thenReturn(date);

        // When
        callLogsServiceRealMethod("addStartDateFilter", luceneLogsSearchBuilder, filter);

        // Then
        verify(luceneLogsSearchBuilder).withStartDate(date);
    }

    @Test
    public void addEndDateFilterIfDateIsNullTest() throws Exception {
        // Given
        when(filter.getEndDate()).thenReturn(null);

        // When
        callLogsServiceRealMethod("addEndDateFilter", luceneLogsSearchBuilder, filter);

        // Then
        verifyZeroInteractions(luceneLogsSearchBuilder);
    }

    @Test
    public void addEndDateFilterTest() throws Exception {
        // Given
        Date date = new Date();
        when(filter.getEndDate()).thenReturn(date);

        // When
        callLogsServiceRealMethod("addEndDateFilter", luceneLogsSearchBuilder, filter);

        // Then
        verify(luceneLogsSearchBuilder).withEndDate(date);
    }

    private Object callLogsServiceRealMethod(String name, Object ... args) throws Exception {
        PowerMockito.when(logsService, name, args).thenCallRealMethod();
        return invokeMethod(logsService, name, args);
    }
}
