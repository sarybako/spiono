package com.volvo.site.server.config.constants;

import com.volvo.testing.PropertiesTest;

public class AppPropertiesTest extends PropertiesTest<AppProperties> {
    public AppPropertiesTest() {
        super(AppProperties.class,
                ConfigFilePaths.APP_TEST_CONFIG_CLASSPATH,
                ConfigFilePaths.APP_DEV_CONFIG_CLASSPATH,
                ConfigFilePaths.APP_PROD_CONFIG_CLASSPATH);
    }
}
