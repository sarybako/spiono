package com.volvo.site.server.service;


import org.testng.annotations.Test;

import static org.springframework.util.ClassUtils.getPackageName;
import static org.testng.Assert.assertEquals;

public class PackageInfoTest {
    @Test
    public void testPackageName() {
        assertEquals(PackageInfo.PACKAGE_NAME, getPackageName(PackageInfo.class));
    }
}
