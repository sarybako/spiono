package com.volvo.site.server.util.converter;

import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;
import com.volvo.site.server.converter.AgentToAgentStatusDTOConverter;
import com.volvo.site.server.model.entity.Agent;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class AgentToAgentStatusDTOConverterTest {

    @Mock
    private Agent agent;

    private final BeanFactory beanFactoryHolder = AutoBeanFactorySource.create(BeanFactory.class);

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void convertAgentToAgentStatusTest(){
        AgentStatus status;

        returnOnlineFakeAgent();
        status = new AgentToAgentStatusDTOConverter().apply(agent);
        assertEquals(status, AgentStatus.ONLINE);

        returnOfflineFakeAgent();
        status = new AgentToAgentStatusDTOConverter().apply(agent);
        assertEquals(status, AgentStatus.OFFLINE);

        returnOnPauseFakeAgent();
        status = new AgentToAgentStatusDTOConverter().apply(agent);
        assertEquals(status, AgentStatus.ON_PAUSE);

        returnNotRegisteredFakeAgent();
        status = new AgentToAgentStatusDTOConverter().apply(agent);
        assertEquals(status, AgentStatus.NOT_REGISTERED);

        returnDeletedFakeAgent();
        status = new AgentToAgentStatusDTOConverter().apply(agent);
        assertEquals(status, AgentStatus.DELETED);
    }

    private void returnOnlineFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
        when(agent.isOnPause()).thenReturn(false);
        when(agent.hasHeartBeatsAtTheLastMinute()).thenReturn(true);
    }

    private void returnOfflineFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
        when(agent.isOnPause()).thenReturn(false);
        when(agent.hasHeartBeatsAtTheLastMinute()).thenReturn(false);
    }

    private void returnOnPauseFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
        when(agent.isOnPause()).thenReturn(true);
    }

    private void returnNotRegisteredFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_CREATED);
    }

    private void returnDeletedFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
    }

}
