package com.volvo.site.server.service.impl;

import com.google.common.collect.Sets;
import com.volvo.site.server.dto.HeartbeatResult;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import com.volvo.site.server.service.impl.util.UserUtilService;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.platform.java.testing.TestUtils.randomLong;
import static com.volvo.site.server.service.UserService.FILTER_AGENT_STATUS_OFF;
import static com.volvo.site.server.service.UserService.SEARCHING_NAME_ANY;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class AgentServiceImplTest {

    @Mock
    protected AgentUtilService agentUtilService;

    @Mock
    private AgentJpaRepository agentJpaRepository;

    @Mock
    private UserUtilService userUtilService;

    @Mock
    private WinAppInstallerServiceImpl winAppInstallerService;

    @Mock
    private ClientInstaller clientInstaller;

    @Mock
    private User user;

    @Mock
    private UserJpaRepository userJpaRepository;

    @Mock
    private Agent agent;

    @Mock
    private AgentInstaller agentInstaller;

    private AgentServiceImpl agentService;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);

        agentService = spy(new AgentServiceImpl(agentUtilService, agentJpaRepository, userUtilService, winAppInstallerService));
    }

    @Test
    public void deleteAgentTest(){
        Agent agent = mock(Agent.class);

        when(agentUtilService.findAgentById(1L)).thenReturn(agent);
        agentService.deleteAgent(1L);

        verify(agent).setStatus(Agent.AgentStatus.AGENT_DELETED);
        verify(agent).setOnPause(false);
        verify(agentUtilService).findAgentById(1L);
        verify(agentJpaRepository).save(agent);
    }

    @Test
    public void deleteAgentAndCreateNewOneTest() {
        Agent agent = mock(Agent.class);
        long userId = 1;
        long agentId = 2;

        doReturn(agent).when(agentService).createAgent(userId);

        Agent actualAgent = agentService.deleteAgentAndCreateNewOne(userId, agentId);

        assertEquals(actualAgent, agent);
        verify(agentJpaRepository).delete(agentId);
        verify(agentService).createAgent(userId);
    }

    @Test
    public void shouldCreateAgentCallItSelf() {
        mockReturningFakeUserFromJpaRepository();
        when(winAppInstallerService.findLastActiveClientInstaller()).thenReturn(clientInstaller);
        when(agentJpaRepository.save((Agent) anyObject())).thenReturn(Agent.of(user, clientInstaller));

        agentService.createAgent(0);
        verify(agentService).createAgent(0, null);
    }

    @Test
    public void shouldCreateAgentReturnAgentTest() {
        mockReturningFakeUserFromJpaRepository();
        when(winAppInstallerService.findLastActiveClientInstaller()).thenReturn(clientInstaller);
        when(agentJpaRepository.save((Agent) anyObject())).thenReturn(Agent.of(user, clientInstaller));

        Agent agent = agentService.createAgent(0);
        assertEquals(agent.getClientInstaller(), clientInstaller);
        assertEquals(agent.getUser(), user);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldCreateAgentThrowEntityNotFoundClientInstallerNotFoundTest() {
        mockReturningFakeUserFromJpaRepository();
        when(winAppInstallerService.findLastActiveClientInstaller()).thenThrow(EntityNotFoundException.class);
        agentService.createAgent(0);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldCreateAgentThrowEntityNotFoundWhenUserNotFoundTest() {
        mockGetActiveUserThrowsNotFound();
        agentService.createAgent(0);
    }

    @Test
    public void downloadAgentPositiveTest() {
        long agentId = 1;
        String clientKey = "key";
        long clientInstallerId = 5;
        byte[] expectedData = new byte[] {};

        when(agentJpaRepository.findByIdAndClientKey(agentId, clientKey)).thenReturn(agent);
        when(agent.getClientInstaller()).thenReturn(clientInstaller);
        when(clientInstaller.getId()).thenReturn(clientInstallerId);
        when(clientInstaller.getData()).thenReturn(expectedData);

        byte[] actualData = agentService.downloadAgent(agentId, clientKey);
        assertEquals(actualData, expectedData);

        verify(winAppInstallerService).setClientInstallerDownloaded(clientInstallerId);
        verify(agent).setStatus(Agent.AgentStatus.AGENT_DOWNLOADED);
        verify(agentJpaRepository).save(agent);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldDownloadAgentThrowNPEWhenClientKeyIsNull() {
        agentService.downloadAgent(0, null);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldDownloadAgentThrowEntityNotFoundWhenAgentNotFound() {
        when(agentJpaRepository.findByIdAndClientKey(anyLong(), anyString())).thenReturn(null);
        agentService.downloadAgent(0, "key");
    }

    @Test
    public void shouldIsAgentRegisteredReturnTrueIfAgentFound() {
        when(agentJpaRepository.findByIdAndStatus(0, Agent.AgentStatus.AGENT_REGISTERED)).thenReturn(agent);
        assertTrue(agentService.isAgentRegistered(0));
    }

    @Test
    public void shouldIsAgentRegisteredReturnFalseIfAgentNotFound() {
        when(agentJpaRepository.findByIdAndStatus(0, Agent.AgentStatus.AGENT_REGISTERED)).thenReturn(null);
        assertFalse(agentService.isAgentRegistered(0));
    }

    @Test
    public void registerAgentTest() {
        Agent agent = mock(Agent.class);
        String clientCode = randomAlphanumeric(ModelConstants.clientInstallerKeyLen);
        long clientInstallerId = 1;

        reset(agentJpaRepository);

        when(agentJpaRepository.findByClientKeyAndStatus(clientCode, Agent.AgentStatus.AGENT_DOWNLOADED)).thenReturn(agent);
        when(agent.getClientInstaller()).thenReturn(clientInstaller);
        when(clientInstaller.getId()).thenReturn(clientInstallerId);
        when(winAppInstallerService.findLastActiveAgentInstaller(clientInstallerId)).thenReturn(agentInstaller);

        agentService.registerAgent(clientCode);

        verify(agent).generateCertificate();
        verify(agent).setAgentInstaller(agentInstaller);
        verify(agent).setStatus(Agent.AgentStatus.AGENT_REGISTERED);
        verify(agentJpaRepository).save(agent);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldRegisterAgentThrowIllegalArgWhenArgLengthHigher() {
        agentService.registerAgent(randomAlphanumeric(ModelConstants.clientInstallerKeyLen + 1));
    }


    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldRegisterAgentThrowEntityNotFoundWhenAgentNotFound() {
        when(agentJpaRepository.findByClientKeyAndStatus(anyString(), eq(Agent.AgentStatus.AGENT_DOWNLOADED))).thenReturn(null);
        agentService.registerAgent(randomAlphanumeric(ModelConstants.clientInstallerKeyLen));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldRegisterAgentThrowNPEWhenArgIsNull() {
        agentService.registerAgent(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldRegisterAgentThrowIllegalArgWhenArgLengthLower() {
        agentService.registerAgent(randomAlphanumeric(ModelConstants.clientInstallerKeyLen - 1));
    }

    @Test
    public void setAgentNameTest() {
        Agent agent = mock(Agent.class);
        when(agentUtilService.findAgentById(0L)).thenReturn(agent);

        agentService.setAgentName(0, "agent-name");

        verify(agent).setName("agent-name");
        verify(agentUtilService).findAgentById(0L);
        verify(agentJpaRepository).save(agent);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldSetAgentNameThrowNPEIfNameIsNull() {
        agentService.setAgentName(0, null);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldThrowEntityNotFoundSetAgentNameIfAgentNotFound() {
        long agentId = randomLong();
        when(agentUtilService.findAgentById(agentId)).thenThrow(EntityNotFoundException.class);
        agentService.setAgentName(agentId, "agent-name");
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldMakeHeartBeatThrowNPEWhenCertificateIsNullTest() {
        agentService.makeHeartBeat(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldMakeHeartBeatThrowIllegalArgWhenCertificateLenIsLower() {
        agentService.makeHeartBeat(randomAlphanumeric(ModelConstants.agentCertificateLen - 1));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldMakeHeartBeatThrowIllegalArgWhenCertificateLenIsHigher() {
        agentService.makeHeartBeat(randomAlphanumeric(ModelConstants.agentCertificateLen + 1));
    }

    @Test
    public void shouldMakeHeartBeatReturnUninstallWhenAgentNotFound() {
        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(null);
        assertEquals(agentService.makeHeartBeat(randomAgentCertificate()), HeartbeatResult.UNINSTALL);
    }

    @Test
    public void shouldMakeHeartBeatReturnUninstallWhenAgentIsDeleted() {
        Agent agent = mock(Agent.class);

        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(agent);
        when(agent.isDeleted()).thenReturn(true);
        assertEquals(agentService.makeHeartBeat(randomAgentCertificate()), HeartbeatResult.UNINSTALL);
    }

    @Test
    public void shouldMakeHeartBeatReturnPauseWhenAgentOnPause() {
        Agent agent = mock(Agent.class);

        when(agentJpaRepository.findByCertificate(anyString())).thenReturn(agent);
        when(agent.isOnPause()).thenReturn(true);
        assertEquals(agentService.makeHeartBeat(randomAgentCertificate()), HeartbeatResult.PAUSE);
    }

    @Test
    public void makeHeartBeatTest() {
        Agent agent = mock(Agent.class);
        String certificate = randomAgentCertificate();

        when(agentJpaRepository.findByCertificate(certificate)).thenReturn(agent);
        when(agent.isDeleted()).thenReturn(false);

        assertEquals(agentService.makeHeartBeat(certificate), HeartbeatResult.OK);

        verify(agent).updateHeartBeatTime();
        verify(agentJpaRepository).save(agent);
    }

    @Test
    public void getAgentsNullSearchingNameNullStatusFilter(){
        reset(agentJpaRepository);
        agentService.getAgents(1, SEARCHING_NAME_ANY, FILTER_AGENT_STATUS_OFF, 1, 1);
        verify(agentJpaRepository).findAgentsForUser(1, SEARCHING_NAME_ANY, false, newArrayList(Agent.AgentStatus.values()),
                new PageRequest(1, 1));
    }

    @Test
    public void getAgentsNullSearchingNameEmptyStatusFilter(){
        reset(agentJpaRepository);
        agentService.getAgents(1, SEARCHING_NAME_ANY, new ArrayList<Agent.AgentStatus>(), 1, 1);
        verify(agentJpaRepository).findAgentsForUser(1, SEARCHING_NAME_ANY, false, newArrayList(Agent.AgentStatus.values()),
                new PageRequest(1, 1));
    }

    @Test
    public void getAgentsEmptySearchingNameNullStatusFilter(){
        reset(agentJpaRepository);
        agentService.getAgents(1, " ", FILTER_AGENT_STATUS_OFF, 1, 1);
        verify(agentJpaRepository).findAgentsForUser(1, SEARCHING_NAME_ANY, false, newArrayList(Agent.AgentStatus.values()),
                new PageRequest(1, 1));
    }

    @Test
    public void getAgentsEmptySearchingNameEmptyStatusFilter(){
        reset(agentJpaRepository);
        agentService.getAgents(1, " ", new ArrayList<Agent.AgentStatus>(), 1, 1);
        verify(agentJpaRepository).findAgentsForUser(1, SEARCHING_NAME_ANY, false, newArrayList(Agent.AgentStatus.values()),
                new PageRequest(1, 1));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void getAgentsNegativePageTest(){
        agentService.getAgents(1, SEARCHING_NAME_ANY, FILTER_AGENT_STATUS_OFF, -1, 1);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void getAgentsNullPageSizeTest(){
        agentService.getAgents(1, SEARCHING_NAME_ANY, FILTER_AGENT_STATUS_OFF, 1, 0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void getAgentsNegativePageSizeTest() {
        agentService.getAgents(1, SEARCHING_NAME_ANY, FILTER_AGENT_STATUS_OFF, 1, -1);
    }

    @Test
    public void setAgentOnPauseTest(){
        Agent agent = mock(Agent.class);
        assertFalse(agent.isOnPause());
        when(agentUtilService.findAgentById(agent.getId())).thenReturn(agent);
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);

        agentService.setAgentOnPause(agent.getId(), true);

        verify(agent).setOnPause(true);
        verify(agentJpaRepository).save(agent);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void setAgentOnPauseIllegalState(){
        Agent agent = mock(Agent.class);
        assertFalse(agent.isOnPause());
        when(agentUtilService.findAgentById(agent.getId())).thenReturn(agent);
        agentService.setAgentOnPause(agent.getId(), true);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void setAgentOnPauseAgentNotFoundTest(){
        long notExistedId = (new Random()).nextLong();
        when(agentUtilService.findAgentById(notExistedId)).thenThrow(EntityNotFoundException.class);
        agentService.setAgentOnPause(notExistedId, true);
    }

    @Test
    public void shouldGetAgentsCallRepository() {
        // Given
        long userId = 5;
        List<Agent.AgentStatus> statuses = newArrayList(Agent.AgentStatus.AGENT_REGISTERED);

        // When
        agentService.getAgents(userId, statuses);

        // Then
        verify(agentJpaRepository).findAgentsForUser(userId, true, statuses);
    }

    @Test
    public void shouldGetAgentsCallRepositoryWithAllStatuses() {
        // Given
        long userId = 5;
        List<Agent.AgentStatus> statuses = newArrayList();

        // When
        agentService.getAgents(userId, statuses);

        // Then
        assertThatFindAgentsForUserHasAllAgentStatuses(userId, false);
    }

    @Test
    public void shouldGetAgentsCallRepositoryWithNullStatuses() {
        // Given
        long userId = 5;

        // When
        agentService.getAgents(userId, null);

        // Then
        assertThatFindAgentsForUserHasAllAgentStatuses(userId, false);
    }

    private void assertThatFindAgentsForUserHasAllAgentStatuses(long userId, boolean useStatusFilter) {
        ArgumentMatcher<List<Agent.AgentStatus>> matcher = new ArgumentMatcher<List<Agent.AgentStatus>>() {
            @Override
            public boolean matches(Object arg) {
                Set<Agent.AgentStatus> statusSet = Sets.newHashSet((List<Agent.AgentStatus>) arg);
                statusSet.removeAll(newArrayList(Agent.AgentStatus.values()));
                return statusSet.isEmpty();
            }
        };
        verify(agentJpaRepository).findAgentsForUser(eq(userId), eq(useStatusFilter), argThat(matcher));
    }

    private void mockReturningFakeUserFromJpaRepository() {
        when(userJpaRepository.findActiveUserById(anyLong())).thenReturn(user);
    }

    private void mockGetActiveUserThrowsNotFound() {
        when(userUtilService.getActiveUser(anyLong())).thenThrow(new EntityNotFoundException(User.class, 1));
    }

    private static String randomAgentCertificate() {
        return randomAlphanumeric(ModelConstants.agentCertificateLen);
    }
}
