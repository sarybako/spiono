package com.volvo.site.server.util.converter;

import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.gwt.usercabinet.shared.enums.AgentStatus;
import com.volvo.site.server.converter.AgentToAgentsResultItemDTOConverter;
import com.volvo.site.server.model.entity.Agent;
import org.mockito.Mock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class AgentToAgentsResultItemDTOConverterTest {

    @Mock
    private Agent agent;

    private final BeanFactory beanFactoryHolder = AutoBeanFactorySource.create(BeanFactory.class);

    @BeforeClass
    public void beforeClass() {
        initMocks(this);
    }

    @Test
    public void agentToAgentResultItemDTOConvertTest(){
        //Given
        returnFakeAgent();

        //When
        AgentDTO resultItem = new AgentToAgentsResultItemDTOConverter
                (beanFactoryHolder).apply(agent);

        //Then
        assertEquals(resultItem.getStatus(), AgentStatus.DELETED);
        assertEquals(resultItem.getName(), "AgentName");
        assertEquals(resultItem.getAgentId(), 100);

    }

    private void returnFakeAgent(){
        when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
        when(agent.getId()).thenReturn(100L);
        when(agent.getName()).thenReturn("AgentName");
    }

}
