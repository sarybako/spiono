package com.volvo.site.server.controller.gwt;


import com.volvo.site.gwt.usercabinet.shared.dto.*;
import com.volvo.site.gwt.usercabinet.shared.enums.StatusFilter;
import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.component.GwtDispatchMethodInvoker;
import com.volvo.site.server.converter.KeyLogDataListResultDTOConverter;
import com.volvo.site.server.converter.PageAgentToGetAgentsResultListDTOConverter;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.LogsService;
import com.volvo.site.server.service.StatisticsService;
import com.volvo.site.server.service.UserService;
import com.volvo.site.server.service.facade.LogsSearchFacade;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.springframework.data.domain.Page;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.volvo.site.server.service.facade.LogsSearchFacade.PerformLogsSearchResult;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

@PrepareForTest({
        UserCabinet.class,
        Authenticator.class,
        PageAgentToGetAgentsResultListDTOConverter.class,
        KeyLogDataListResultDTOConverter.class})
public class UserCabinetTest extends PowerMockTestCase {

    UserCabinet userCabinet;

    @Mock
    GwtDispatchMethodInvoker gwtDispatchMethodInvoker;

    @Mock
    UserService userService;

    @Mock
    StatisticsService statisticsService;

    @Mock
    LogsService logsService;

    @Mock
    AgentService agentService;

    @Mock
    LogsSearchFacade logsSearchFacade;

    @Mock
    BeanFactory autoBeanFactory;

    final Long USER_ID = 5L;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);
        PowerMockito.mockStatic(Authenticator.class);
        PowerMockito.when(Authenticator.getCurrentUserId()).thenReturn(USER_ID);
        when(gwtDispatchMethodInvoker.getThreadLocalBeanFactory()).thenReturn(autoBeanFactory);
        userCabinet = PowerMockito.spy(new UserCabinet(gwtDispatchMethodInvoker, userService, statisticsService,
                logsService, agentService, logsSearchFacade));
    }

    @Test
    public void getUserAgentsTest() throws Exception {
        // Given
        final GetAgentsDTO command = mock(GetAgentsDTO.class);
        final Page<Agent> resultToConvert = mock(Page.class);
        final PagedAgentsListDTO result = mock(PagedAgentsListDTO.class);
        final PageAgentToGetAgentsResultListDTOConverter resultConverter =
                mock(PageAgentToGetAgentsResultListDTOConverter.class);
        PowerMockito.whenNew(PageAgentToGetAgentsResultListDTOConverter.class).withArguments(autoBeanFactory)
                .thenReturn(resultConverter);

        // When
        when(command.getSearchingString()).thenReturn("SearchString");
        when(command.getStatusFilter()).thenReturn(StatusFilter.ALL_AGENTS);
        when(command.getPageNumber()).thenReturn(1);
        when(command.getPageSize()).thenReturn(20);
        when(resultConverter.apply(resultToConvert)).thenReturn(result);
        when(agentService.getAgents(eq(USER_ID), eq("SearchString"),
                anyListOf(Agent.AgentStatus.class), eq(1), eq(20))).
                thenReturn(resultToConvert);

        // Then
        assertEquals(userCabinet.getUserAgents(command), result);
    }

    @Test
    public void getLogsForAgentTest() throws Exception {
        // Given
        final GetKeyLogDataDTO command = mock(GetKeyLogDataDTO.class);
        final PerformLogsSearchResult resultToConvert = mock(PerformLogsSearchResult.class);
        final KeyLogDataListResultDTOConverter converter = mock(KeyLogDataListResultDTOConverter.class);
        final KeyLogDataListResultDTO result = mock(KeyLogDataListResultDTO.class);

        // When
        PowerMockito.whenNew(KeyLogDataListResultDTOConverter.class).withArguments(autoBeanFactory)
                .thenReturn(converter);
        when(converter.apply(resultToConvert)).thenReturn(result);
        when(logsSearchFacade.performLogsSearch(USER_ID, command)).thenReturn(resultToConvert);

        // Then
        assertEquals(userCabinet.getLogsForAgent(command), result);
    }
}
