package com.volvo.site.server.util;


import org.apache.lucene.search.Query;
import org.hibernate.search.query.dsl.*;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.security.util.FieldUtils.getProtectedFieldValue;
import static org.springframework.security.util.FieldUtils.setProtectedFieldValue;
import static org.testng.Assert.assertEquals;

public class LuceneLogsSearchBuilderTest {

    @Mock
    QueryBuilder queryBuilder;

    @Mock
    BooleanJunction<BooleanJunction> booleanJunction;

    @Mock
    Query query;

    LuceneLogsSearchBuilder builder;

    @BeforeMethod
    public void beforeMethod() {
        initMocks(this);
        when(queryBuilder.bool()).thenReturn(booleanJunction);
        builder = spy(new LuceneLogsSearchBuilder(queryBuilder));
    }

    @Test
    public void constructorTest() {
        // Given
        reset(queryBuilder);
        when(queryBuilder.bool()).thenReturn(booleanJunction);

        // When
        LuceneLogsSearchBuilder builder = new LuceneLogsSearchBuilder(queryBuilder);

        // Then
        verify(queryBuilder).bool();
        assertEquals(getProtectedFieldValue("queryBuilder", builder), queryBuilder);
    }

    @Test
    public void buildEmptyBuilderTest() {
        AllContext allContext = mock(AllContext.class);
        MustJunction mustJunction = mock(MustJunction.class);
        when(queryBuilder.all()).thenReturn(allContext);
        when(allContext.createQuery()).thenReturn(query);
        when(booleanJunction.must(query)).thenReturn(mustJunction);
        when(mustJunction.createQuery()).thenReturn(query);

        // When
        Query result = builder.build();

        // Then
        assertEquals(result, query);
    }

    @Test
    public void buildTest() {
        // Given
        MustJunction mustJunction = mock(MustJunction.class);
        setProtectedFieldValue("mustJunction", builder, mustJunction);
        when(mustJunction.createQuery()).thenReturn(query);

        // When
        Query result = builder.build();

        // Then
        assertEquals(result, query);
    }

    @Test
    public void withAgentTest() {
        // Given
        TermContext termContext = mock(TermContext.class);
        when(queryBuilder.keyword()).thenReturn(termContext);
        TermMatchingContext termMatchingContext = mock(TermMatchingContext.class);
        when(termContext.onField("agentId")).thenReturn(termMatchingContext);
        TermTermination termTermination = mock(TermTermination.class);
        when(termMatchingContext.matching(5L)).thenReturn(termTermination);
        when(termTermination.createQuery()).thenReturn(query);
        LuceneLogsSearchBuilder expectedResult = new LuceneLogsSearchBuilder(queryBuilder);
        doReturn(expectedResult).when(builder).addMust(query);

        // When
        LuceneLogsSearchBuilder actualResult = builder.withAgent(5L);

        // Then
        assertEquals(actualResult, expectedResult);
    }

    @Test
    public void withSearchStringTest() {
        // Given
        TermContext termContext = mock(TermContext.class);
        when(queryBuilder.keyword()).thenReturn(termContext);
        TermMatchingContext termMatchingContext = mock(TermMatchingContext.class);
        when(termContext.onFields("text", "windowText", "processName")).thenReturn(termMatchingContext);
        TermTermination termTermination = mock(TermTermination.class);
        when(termMatchingContext.matching("searchString")).thenReturn(termTermination);
        when(termTermination.createQuery()).thenReturn(query);
        LuceneLogsSearchBuilder expectedResult = new LuceneLogsSearchBuilder(queryBuilder);
        doReturn(expectedResult).when(builder).addMust(query);

        // When
        LuceneLogsSearchBuilder actualResult = builder.withSearchString("searchString");

        // Then
        assertEquals(actualResult, expectedResult);
    }

    @Test
    public void withStartDateTest() {
        // Given
        Date date = new Date();
        RangeContext rangeContext = mock(RangeContext.class);
        when(queryBuilder.range()).thenReturn(rangeContext);
        RangeMatchingContext rangeMatchingContext = mock(RangeMatchingContext.class);
        when(rangeContext.onField("startDate")).thenReturn(rangeMatchingContext);
        RangeTerminationExcludable rangeTerminationExcludable = mock(RangeTerminationExcludable.class);
        when(rangeMatchingContext.above(date)).thenReturn(rangeTerminationExcludable);
        when(rangeTerminationExcludable.createQuery()).thenReturn(query);
        LuceneLogsSearchBuilder expectedResult = new LuceneLogsSearchBuilder(queryBuilder);
        doReturn(expectedResult).when(builder).addMust(query);

        // When
        LuceneLogsSearchBuilder actualResult = builder.withStartDate(date);

        // Then
        assertEquals(actualResult, expectedResult);
    }

    @Test
    public void withEndDateTest() {
        // Given
        Date date = new Date();
        RangeContext rangeContext = mock(RangeContext.class);
        when(queryBuilder.range()).thenReturn(rangeContext);
        RangeMatchingContext rangeMatchingContext = mock(RangeMatchingContext.class);
        when(rangeContext.onField("endDate")).thenReturn(rangeMatchingContext);
        RangeTerminationExcludable rangeTerminationExcludable = mock(RangeTerminationExcludable.class);
        when(rangeMatchingContext.below(date)).thenReturn(rangeTerminationExcludable);
        when(rangeTerminationExcludable.createQuery()).thenReturn(query);
        LuceneLogsSearchBuilder expectedResult = new LuceneLogsSearchBuilder(queryBuilder);
        doReturn(expectedResult).when(builder).addMust(query);

        // When
        LuceneLogsSearchBuilder actualResult = builder.withEndDate(date);

        // Then
        assertEquals(actualResult, expectedResult);
    }

    @Test
    public void addMustIfMustJunctionIsNullTest() {
        // Given
        MustJunction mustJunction = mock(MustJunction.class);
        when(booleanJunction.must(query)).thenReturn(mustJunction);

        // When
        builder.addMust(query);

        // Then
        assertEquals(getProtectedFieldValue("mustJunction", builder), mustJunction);
    }

    @Test
    public void addMustIfMustJunctionIsNotNullTest() {
        // Given
        MustJunction mustJunction = mock(MustJunction.class);
        setProtectedFieldValue("mustJunction", builder, mustJunction);
        MustJunction expectedMustJunction = mock(MustJunction.class);
        when(mustJunction.must(query)).thenReturn(expectedMustJunction);

        // When
        builder.addMust(query);

        // Then
        assertEquals(getProtectedFieldValue("mustJunction", builder), expectedMustJunction);
    }
}
