package com.volvo.site.server.util;


import com.volvo.platform.spring.I18nUtils;
import com.volvo.site.server.config.I18nConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.getStaticFinalStringValues;
import static com.volvo.platform.java.testing.TestUtils.isStringsAreUnique;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@ContextConfiguration(classes = I18nConfig.class)
public class MessagesConstsTest extends AbstractTestNGSpringContextTests {

    @Autowired
    MessageSource messageSource;

    @Test
    public void testValuesAreUnique() {
        assertTrue(isStringsAreUnique(getStaticFinalStringValues(MessagesConsts.class)));
    }

    @Test
    public void testMessageSourceWorks() {
        assertNotNull(messageSource);
    }

    @Test(dataProvider = "simpleMessages")
    public void testSimpleMessages(String message) {
        assertNotNull(I18nUtils.getMessage(messageSource, message));
    }
}
