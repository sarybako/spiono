package com.volvo.site.server.service.impl;

import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.repository.TwitterUserJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;


public class UserServiceImplTest {

    private UserServiceImpl userService;

    @Mock
    private UserJpaRepository userJpaRepository;

    @Mock
    private com.volvo.site.server.repository.VKUserJpaRepository vkUserJpaRepository;

    @Mock
    private User user;

    @Mock
    private AgentJpaRepository agentJpaRepository;

    @Mock
    private ClientInstaller clientInstaller;

    @Mock
    private AgentInstaller agentInstaller;

    @Mock
    private KeyLogJpaRepository keyLogJpaRepository;

	@Mock
    private AgentUtilService agentUtilService;

    @Mock
    private com.volvo.site.server.repository.FBUserJpaRepository fbUserJpaRepository;

    @Mock
    private TwitterUserJpaRepository twitterUserJpaRepository;

    @BeforeMethod
    public void beforeTest() {
        initMocks(this);

        userService = spy(new UserServiceImpl(userJpaRepository, vkUserJpaRepository, fbUserJpaRepository, twitterUserJpaRepository));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEInChangePasswordWhenOldPasswordIsNull() throws Exception {
        userService.changePassword(0, null, "password");
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldBeNPEInChangePasswordWhenNewPasswordIsNull() throws Exception {
        userService.changePassword(0, "password", null);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldBeEntityNotFoundExceptionInChangePasswordWhenUserNotFound() {
        mockReturningNullFakeUser();
        userService.changePassword(-1, "password", "password");
    }

    @Test
    public void shouldReturnFalseInChangePasswordWhenOldPasswordIsWrong() {
        mockReturningFakeUserFromJpaRepository();
        mockTryingUpdatePasswordInUserRetutns(false);
        assertFalse(userService.changePassword(0, "oldPwd", "newPwd"));
    }

    @Test
    public void changePasswordPositiveTest() {
        mockReturningFakeUserFromJpaRepository();
        mockTryingUpdatePasswordInUserRetutns(true);

        assertTrue(userService.changePassword(0, "oldPwd", "newPwd"));
        verify(userJpaRepository).save(user);
    }

    private void mockReturningNullFakeUser() {
        when(userJpaRepository.findActiveUserById(anyLong())).thenReturn(null);
    }

    private void mockReturningFakeUserFromJpaRepository() {
        when(userJpaRepository.findActiveUserById(anyLong())).thenReturn(user);
    }

    private void mockTryingUpdatePasswordInUserRetutns(boolean returns) {
        when(user.tryUpdatePassword(anyString(), anyString())).thenReturn(returns);
    }
}
