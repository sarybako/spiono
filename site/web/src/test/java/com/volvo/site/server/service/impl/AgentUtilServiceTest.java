package com.volvo.site.server.service.impl;


import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class AgentUtilServiceTest {

	private AgentUtilService agentUtilService;

	@Mock
    private AgentJpaRepository agentJpaRepository;
    @Mock
    private Agent agent;

	@BeforeMethod
	public void setUp() throws Exception {
		initMocks(this);

		agentUtilService = new AgentUtilService(agentJpaRepository);
	}

	@Test(expectedExceptions = EntityNotFoundException.class)
	public void shouldThrowExceptionIfNotExistedAgentFindAgentByIdTest(){
		when(agentJpaRepository.findOne(anyLong())).thenThrow(EntityNotFoundException.class);
		agentUtilService.findAgentById(1);
	}

	@Test
	public void shouldReturnAgentFindAgentByIdTest(){
		when(agentJpaRepository.findOne(anyLong())).thenReturn(agent);
		Agent founded = agentUtilService.findAgentById(1);

		assertNotNull(founded);
		verify(agentJpaRepository).findOne(1L);
	}

	@Test(expectedExceptions = NullPointerException.class)
	public void shouldThrowNPEIfCertificateIsNullCheckCertificateTest(){
		AgentUtilService.checkCertificate(null);
	}

	@Test(expectedExceptions = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionIfCertificateLengthIsBuggerThan80CheckCertificateTest(){
		AgentUtilService.checkCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen + 1));
	}

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionIfCertificateLengthIsLower80CheckCertificateTest(){
        AgentUtilService.checkCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen - 1));
    }

	@Test
	public void certificateOkCheckCertificateTest(){
		AgentUtilService.checkCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen));
	}

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowNPEIfAgentCertificateIsNullCheckCertificateTest(){
        agentUtilService.checkAgentCertificate(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionIfAgentCertificateLengthIsBuggerThan80CheckCertificateTest(){
        agentUtilService.checkAgentCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen + 1));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionIfAgentCertificateLengthIsLower80CheckCertificateTest(){
        agentUtilService.checkAgentCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen - 1));
    }

    @Test
    public void agentCertificateOkCheckCertificateTest(){
        agentUtilService.checkAgentCertificate(RandomStringUtils.random(ModelConstants.agentCertificateLen));
    }

	@Test
	public void cannotLogWithNullAgentCanAgentLogTest(){
		assertEquals(false, AgentUtilService.canAgentLog(null));
	}

	@Test
	public void cannotLogIfAgentOnPauseCanAgentLogTest(){
		when(agent.isOnPause()).thenReturn(true);
		assertEquals(false, AgentUtilService.canAgentLog(agent));
	}

	@Test
	public void cannotLogIfAgentStatusIsNotRegisteredCanAgentLogTest(){
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_CREATED);
		assertEquals(false, AgentUtilService.canAgentLog(agent));
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
		assertEquals(false, AgentUtilService.canAgentLog(agent));
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DOWNLOADED);
		assertEquals(false, AgentUtilService.canAgentLog(agent));
	}

	@Test
	public void canLogCanAgentLogTest(){
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_REGISTERED);
		assertEquals(true, AgentUtilService.canAgentLog(agent));
	}

	@Test(expectedExceptions = IllegalStateException.class)
	public void agentCreatedthrowIllegalStateExceptionIfNotRegisteredTest(){
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_CREATED);
		AgentUtilService.throwIllegalStateExceptionIfNotRegistered(agent);
	}

	@Test(expectedExceptions = IllegalStateException.class)
	public void agentDeletedthrowIllegalStateExceptionIfNotRegisteredTest(){
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DELETED);
		AgentUtilService.throwIllegalStateExceptionIfNotRegistered(agent);
	}

	@Test(expectedExceptions = IllegalStateException.class)
	public void agentDownloadedthrowIllegalStateExceptionIfNotRegisteredTest(){
		when(agent.getStatus()).thenReturn(Agent.AgentStatus.AGENT_DOWNLOADED);
		AgentUtilService.throwIllegalStateExceptionIfNotRegistered(agent);
	}

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void shouldFindByCertificateThrowEntityNotFound() {
        when(agentJpaRepository.findByCertificate("certificate")).thenReturn(null);
        agentUtilService.findByCertificate("certificate");
    }

    @Test
    public void findByCertificatePositiveTest() {
        when(agentJpaRepository.findByCertificate("certificate")).thenReturn(agent);
        Agent result = agentUtilService.findByCertificate("certificate");
        assertSame(result, agent);
    }
}
