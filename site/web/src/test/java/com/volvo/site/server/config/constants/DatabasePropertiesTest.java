package com.volvo.site.server.config.constants;

import com.volvo.testing.PropertiesTest;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.volvo.platform.java.PropertiesUtils.undecorateProperty;
import static com.volvo.site.server.config.constants.ConfigFilePaths.DB_DEV_CONFIG_CLASSPATH;
import static com.volvo.site.server.config.constants.ConfigFilePaths.DB_TEST_CONFIG_CLASSPATH;
import static com.volvo.site.server.config.constants.DatabaseProperties.HibernateFormatSql;
import static com.volvo.site.server.config.constants.DatabaseProperties.HibernateShowSql;

public class DatabasePropertiesTest extends PropertiesTest<DatabaseProperties> {

    public DatabasePropertiesTest() {
        super(DatabaseProperties.class, DB_DEV_CONFIG_CLASSPATH, DB_TEST_CONFIG_CLASSPATH,
                DB_TEST_CONFIG_CLASSPATH);
    }

    @Test
    public void checkBooleanFlags() throws FileNotFoundException, ConfigurationException {
        Configuration configuration = getConfiguration(DB_DEV_CONFIG_CLASSPATH);
        configuration.getBoolean(undecorateProperty(HibernateFormatSql));
        configuration.getBoolean(undecorateProperty(HibernateShowSql));
    }
}
