<%@ page import="com.volvo.site.server.form.ChangePasswordForm" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.IndexController.RESTORE" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="static com.volvo.platform.spring.MvcUtil.getUncapitalizedSimpleClassName" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="../includes/localized-jquery-validate.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/view/view-changepwd.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app/app-changepwd.js"></script>
<jsp:include page="../includes/localized-messages.jsp"/>

<div class="text-center">
<h1>
    <spring:message code="change_password.header" text="Change password"/>
</h1>

<c:if test="${not empty entityNotFoundException}">
    <div class="alert alert-error">
        <strong><spring:message code="change_password.text1" text="Bad link for changing the password"/></strong><br/><br/>
        <spring:message code="change_password.text2" text="Please make sure the link was correctly copied from the email"/>
        <spring:message code="change_password.text3" text="which we sent you after the password retrieval request"/>
        <ul>
            <li>
                <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= RESTORE + "/"%>"><spring:message code="change_password.text4" text="Try to restore password again"/></a>
            </li>
            <li>
                <a class="blue-underline-link" href="${pageContext.request.contextPath}<%= LOGIN + "/"%>"><spring:message code="label.signin" text="Sign in"/></a>
            </li>
        </ul>
    </div>
</c:if>

<c:if test="${not empty passwordForm}">

    <form:form
        cssClass="well well-small text-center"
        cssStyle="margin-left: 33%; margin-right: 33%"
        method="POST"
        action="<%= appendSlash(ControllersPaths.IndexController.CHANGE_PASSWORD)  %>"
        commandName="<%= getUncapitalizedSimpleClassName(ChangePasswordForm.class) %>">

        <fieldset>
            <jsp:include page="/WEB-INF/templates/includes/form-field-password.jsp" />
            <input type="hidden" id="resCode" name="resCode" class="required" value="${resCode}" />
        </fieldset>

        <button type="submit" class="btn btn-primary"><spring:message code="change_password.text6" text="Change password"/></button>
    </form:form>
</c:if>
</div>