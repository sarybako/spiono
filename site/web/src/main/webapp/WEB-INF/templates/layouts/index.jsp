<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<h1 class="text-center">
    <spring:message code="mainWelcomeOn" text="Welcome on Spiono"/><br>
    <small>
        <spring:message code="mainText" text="Find out what happens on any computers you care about free absolutely free"/>
    </small>
</h1>

<h5 class="pull-left" style="width:60%; margin-top: 2%; padding: 1%">
    <spring:message code="mainTitle1" text="Spiono welcomes the world's professional spy software for the WINDOWS operating system.
        It has the powerful monitoring features of PC Spy!"/>
    <br></br>
    <spring:message code="mainTitle2" text="Spiono will reveal the truth for any company or family using these types of PC.
        You can SILENTLY learn the truth about their emails, text messages and used programs
        by logging into your Spiono account from any web browser. You can view all activities taken by the phone too!" />
    <br></br>
    <spring:message code="mainTitle3" text="At this time, SOME antivirus can restrict the downloading and installing that application." />
</h5>

<div class="pull-right" style="width: 35%; margin-top: 2%; margin-left: 20px">
    <jsp:include page="/WEB-INF/templates/layouts/login.jsp" />
</div>

<h4 class="text-center" style="clear: both;">
    <spring:message code="programDescription" text="Program Description" />
    <br></br>
    <small>
        <spring:message code="mainDescription1" text="Monitoring with Spiono Software is a hybrid software/service
            which allows you to monitor any PC in real time.
            This unique system records the activities of anyone who uses windows devices.
            You install a small application. It starts at every boot of the PC but remains stealth." />
        <br></br>
        <spring:message code="mainDescription2" text="After the software is setup on your PC it will record text messages and all activities
            and then silently upload the data to your private Spiono account using the Internet." />
        <br></br>
        <spring:message code="mainDescription3" text="When you want to view results,
            simply login to the User Cabinet from any computer or phone web browser
            and enter your username and password to proceed or just use the social networking websites." />
    </small>
</h4>



