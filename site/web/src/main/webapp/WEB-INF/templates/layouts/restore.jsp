<%@ page import="com.volvo.site.server.form.RestoreForm" %>
<%@ page import="static com.volvo.site.server.util.ControllersPaths.appendSlash" %>
<%@ page import="com.volvo.site.server.util.ControllersPaths" %>
<%@ page import="static com.volvo.platform.spring.MvcUtil.getUncapitalizedSimpleClassName" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="../includes/localized-jquery-validate.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/view/view-restore.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app/app-restore.js"></script>

<h1 class="text-center">
    <spring:message code="restore.header" text="Restore password"/>
</h1>

<form:form
        cssClass="well well-small text-center"
        cssStyle="margin-left: 33%; margin-right: 33%"
        method="POST"
        action="<%= appendSlash(ControllersPaths.IndexController.RESTORE)  %>"
        commandName="<%= getUncapitalizedSimpleClassName(RestoreForm.class) %>">

    <c:set var="link_reused">
        <spring:message code="linkReused"
                        text="You have already used this link to reset your password. Please fill out the form below to reset your password"/>
    </c:set>
    <c:if test="${not empty uniqueKeyIsNotValidException}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text"
                       value="${link_reused}" />
        </jsp:include>
    </c:if>

    <c:set var="link_restore_expired">
        <spring:message code="lingRestoreExpired"
                        text="The unique link to change your password out of date. Please fill out the form below to reset your password"/>
    ></c:set>
    <c:if test="${not empty uniqueKeyTimedOutException}">
        <jsp:include page="/WEB-INF/templates/includes/form-error.jsp">
            <jsp:param name="form.error.text"
                       value="${link_restore_expired}" />
        </jsp:include>
    </c:if>

    <jsp:include page="/WEB-INF/templates/includes/form-field-email.jsp" />

    <button type="submit" class="btn btn-primary"><spring:message code="restore.btn" text="Restore"/></button>
</form:form>
