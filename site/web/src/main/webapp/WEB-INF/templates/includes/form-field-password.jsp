<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="password"><spring:message code="label.form_field_email.password" text="Password"/></c:set>

<spring:bind path="password">
    <c:set var="password_control_group_class" value="control-group" scope="request"/>
    <c:if test="${not empty status.errorMessages}">
        <c:set var="password_control_group_class" value="control-group error" scope="request" />
    </c:if>

    <div class="${password_control_group_class}">
        <div class="controls">
            <form:password path="password" autocomplete="off" cssClass="required" maxlength="30" placeholder="${password}" />
            <form:errors path="password" cssClass="help-block" />
        </div>
    </div>
</spring:bind>



