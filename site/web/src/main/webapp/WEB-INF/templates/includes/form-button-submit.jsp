<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<button type="submit" class="btn btn-primary"><spring:message code="login.submit" text="Login"/></button>