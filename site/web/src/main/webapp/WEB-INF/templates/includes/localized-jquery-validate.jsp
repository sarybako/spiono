<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${locale == 'en'}">
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.validate.js"></script>
    </c:when>
    <c:when test="${locale == 'ru'}">
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.validate_ru.js"></script>
    </c:when>
    <c:otherwise>
        <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery.validate.js"></script>
    </c:otherwise>
</c:choose>