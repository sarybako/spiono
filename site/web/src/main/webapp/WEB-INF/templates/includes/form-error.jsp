<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
     <strong><spring:message code="warning" text="Warning"/>&nbsp;</strong><%=request.getParameter("form.error.text")%>
</div>