<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /> - Spiono</title>
<meta name="description" content="<spring:message code="metaDescription"
    text=" Spiono - spy-service which lets you care about interesting computers."/>" >
<meta name="Keywords" content="<spring:message code="metaKeywords"
    text=" Spiono, spy, online spy, computer spy, computer, monitor, know, find out, password, logger, key logger,
        chat, communication, mystery"/>" >

<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-responsive.min.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css" type="text/css" />

<!-- Common JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/jquery-1.9.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/underscore.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/backbone.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/lib/common.js"></script>

</head>
<body>
	<tiles:insertAttribute name="header" />
    <div class="container" style="margin-top: 100px;">
        <tiles:insertAttribute name="content" />
    </div>
    <div class="container" style="clear: both;">
        <tiles:insertAttribute name="footer" />
    </div>
</body>
</html>