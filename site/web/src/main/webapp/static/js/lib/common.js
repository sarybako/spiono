var app = app || {};

$(function() {
    'use strict';

    function utils() {
        this.topDomains = [
            "gmail.com",
            "yahoo.com",
            "hotmail.com",
            "gmx.de",
            "googlemail.com",
            "web.de",
            "live.com",
            "aol.com",
            "gmx.net",
            "me.com",
            "msn.com",
            "comcast.net",
            "hushmail.com",
            "yahoo.de",
            "hotmail.co.uk",
            "lavabit.com",
            "sbcglobal.net",
            "safe-mail.net",
            "mail.com",
            "email.com",
            "mail.ru",
            "yandex.ru"
        ];

        this.initEmailAutoComplete = function(component) {
            var me = this;
            component.typeahead({
                source: function(email, process) {
                    var emails = [];
                    var indexOfAt = email.indexOf('@');
                    if ( indexOfAt > 0 ){
                        var emailOfAt = email.substring(indexOfAt + 1);
                        if (emailOfAt.length > 0) {
                            for ( var domain in me.topDomains) {
                                if (me.topDomains.hasOwnProperty(domain) && me.topDomains[domain].indexOf(emailOfAt) == 0) {
                                    emails.push(email.substring(0, indexOfAt + 1) + me.topDomains[domain]);
                                }
                            }
                        }
                    }
                    return emails;
                }
            });
        };

        this.cookieWithoutQuotes = function(name) {
            return this.removeQuotes($.cookie("email"));
        };

        this.removeQuotes = function(x) {
            if (x == null) {
                return null;
            }
            return x.replace(/['"]/g,'');
        };

        this.addClassWhenAjaxIsLoading = function(selector, clazz) {
            $(selector).on({
                ajaxStart: function() {
                    $(this).addClass(clazz)
                },
                ajaxStop: function() {
                    $(this).removeClass(clazz);
                }
            });
        };

        this.initValidatorDefaults = function(clazz) {
            $.validator.setDefaults({
                highlight: function(input) {
                    $(input).parent().parent().addClass(clazz)
                },
                unhighlight: function(input) {
                    $(input).parent().parent().removeClass(clazz)
                }
            });
        };

        this.highlightNavigationMenu = function() {
            $('div[class="navbar-inner"] ul[class="nav"] li a').each(function(){
                var path = window.location.pathname;
                if (path == $(this).attr('href')) {
                    $(this).parent().addClass('active');
                }
            });
        }
    };

    app.Utils = new utils();



    // todo: think about the indication of ajax loading
    app.Utils.addClassWhenAjaxIsLoading('body', 'loading');
    if ($.validator) {
        app.Utils.initValidatorDefaults('error');
    }
    app.Utils.highlightNavigationMenu();
});