var app = app || {};

$(function($) {
    'use strict';

    app.LoginView = Backbone.View.extend({
        el: '#loginForm',
        emailField: $("#email"),
        passwordField: $("#password"),

        initialize: function() {
            this.initFormValidation();
            this.initFocus();
            app.Utils.initEmailAutoComplete(this.emailField);
        },

        initFormValidation: function() {
            $(this.el).validate({
                rules: {
                    password: {
                        minlength: 5
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.insertAfter(element)
                    error.addClass('help-block');
                }
            });
        },

        initFocus: function() {
            this.emailField.val(app.Utils.cookieWithoutQuotes("email"));
            if (this.emailField.val().length === 0) {
                this.emailField.focus();
            }
            else {
                this.passwordField.focus();
            }
        }
    });
});