
$(function($) {
    'use strict';

    app.Messages = {
        err_passwords_did_not_match: 'Please enter the same password as above',
        err_not_valid_email: 'Please enter a valid email address'
    }
});