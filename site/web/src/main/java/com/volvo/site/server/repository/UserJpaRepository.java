package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface UserJpaRepository extends JpaRepositoryWithLongId<User> {

    @Query("select u.id from User u where u.email=:email")
    Long findUserIdByEmail(@Param("email") String email);

    @Query("select u.errLogins from User u where u.email=:email")
    Long findErrLoginsByEmail(@Param("email") String email);

    @Modifying
    @Query("update User u set u.errLogins=:count where u.email=:email")
    void setLoginErrorsCount(@Param("email") String email, @Param("count") long count);

    @Modifying
    @Query("update User u set u.errLogins=0 where u.email=:email")
    void resetLoginErrorsCount(@Param("email") String email);

    @Query("select u from User u where u.email=:email")
    User findUserByEmail(@Param("email") String email);

    @Query("select u from User u where u.id=:id and u.active=true")
    User findActiveUserById(@Param("id") long id);
}