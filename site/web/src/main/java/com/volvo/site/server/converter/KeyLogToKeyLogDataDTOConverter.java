package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;
import lombok.AllArgsConstructor;

import javax.annotation.Nullable;

@AllArgsConstructor
public class KeyLogToKeyLogDataDTOConverter implements Function<KeyLog, KeyLogDataDTO> {

	private final AutoBeanFactory beanFactory;

	@Nullable
	@Override
	public KeyLogDataDTO apply(@Nullable KeyLog keyLog) {
		KeyLogDataDTO result = beanFactory.create(KeyLogDataDTO.class).as();
		result.setHWND(keyLog.getHwnd());
		result.setProcessName(keyLog.getProcessName());
		result.setWindowText(keyLog.getWindowText());
		result.setStartDate(keyLog.getStartDate());
		result.setEndDate(keyLog.getEndDate());
		result.setText(keyLog.getText());
		return result;
	}
}
