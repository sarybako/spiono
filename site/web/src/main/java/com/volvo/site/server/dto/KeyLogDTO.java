package com.volvo.site.server.dto;

import lombok.*;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@ToString(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KeyLogDTO extends CertificatedDTO {

    @JsonProperty(value = "SpyData")
    @NotNull
    @NotEmpty
    private KeyLogDataDTO[] keyLogs;
}
