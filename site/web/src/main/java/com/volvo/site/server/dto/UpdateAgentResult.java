package com.volvo.site.server.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

@ToString
@AllArgsConstructor
@Getter
public class UpdateAgentResult {

    public static final UpdateAgentResult NO_AGENT_UPDATES = new UpdateAgentResult(false, null);

    @JsonProperty(value = "HasUpdate")
    private boolean hasUpdate;

    @JsonProperty(value = "Agent")
    private String agent;
}
