package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.AgentInstaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static com.volvo.site.server.util.SpringDataUtil.ONE_ELEM_PAGEABLE;
import static com.volvo.site.server.util.SpringDataUtil.firstOrNull;

@Repository
@Transactional(readOnly = true)
class AgentInstallerRepositoryImpl implements AgentInstallerRepository {

    @Autowired
    private AgentInstallerJpaRepository repository;

    @Override
    public boolean hasActiveAgentInstaller(long clientInstallerId) {
        return repository.getActiveCount(clientInstallerId) > 0;
    }

    @Override
    public AgentInstaller findLastActive(long clientInstallerId) {
        return firstOrNull(repository.findLastActiveInstallers(clientInstallerId, ONE_ELEM_PAGEABLE));
    }
}
