package com.volvo.site.server.config;

import com.volvo.site.server.component.JdbcSafeTokenRepositoryImpl;
import com.volvo.site.server.component.PwdAndEmailUserDetailsService;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.config.db.DevelopmentDbConfig;
import com.volvo.site.server.config.db.ProdDBConfig;
import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.DefaultWebInvocationPrivilegeEvaluator;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.security.web.session.SessionManagementFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.config.constants.SecurityRoles.*;
import static com.volvo.site.server.util.SecurityConfigUtil.*;
import static com.volvo.site.server.util.SecurityConfigUtil.DefaultSecurityMetadataSourceBuilder.defaultSecurityMetadataSourceBuilder;
import static java.util.Arrays.asList;

@Import( { RootPropertiesPlaceHolderConfig.class, DevelopmentDbConfig.class, ProdDBConfig.class })
@Configuration
public class SecurityConfig {

    private static final String LOGIN_URL = "/login/";

    @Value(AppProperties.RootUsername)
    private String rootUserName;

    @Value(AppProperties.RootPassword)
    private String rootPassword;

    @Value(AppProperties.RootPasswordUseHash)
    private String rootPasswordUseHash;

    @Value(AppProperties.SiteName)
    private String siteName;

    @Autowired
    private PwdAndEmailUserDetailsService pwdAndEmailUserDetailsService;

    @Autowired
    private DataSource dataSource;

    @Bean
    public PersistentTokenRepository rememberMeTokenRepository() {
        return new JdbcSafeTokenRepositoryImpl(dataSource);
    }

    @Bean
    public WebInvocationPrivilegeEvaluator defaultWebSecurityExpressionHandler() throws ConcurrentException {
        return new DefaultWebInvocationPrivilegeEvaluator(filterSecurityInterceptor());
    }

    @Bean
    public AbstractRememberMeServices rememberMeServices(){
        return new PersistentTokenBasedRememberMeServices("myKey", pwdAndEmailUserDetailsService, rememberMeTokenRepository());
    }

    @Bean
    public SecurityExpressionHandler securityExpressionHandler() {
        return new DefaultWebSecurityExpressionHandler();
    }

    @Bean
    public RememberMeAuthenticationFilter rememberMeAuthenticationFilter() throws ConcurrentException {
        return new RememberMeAuthenticationFilter(authenticationManager(), rememberMeServices());
    }

    @Bean
    public SessionManagementFilter sessionManagementFilter() {
        return new SessionManagementFilter(securityContextRepository(), sessionAuthenticationStrategy());
    }

    @Bean
    public SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        SessionFixationProtectionStrategy strategy = new SessionFixationProtectionStrategy();
        strategy.setAlwaysCreateSession(true);
        return strategy;
    }

    @Bean
    public SecurityContextPersistenceFilter securityContextPersistenceFilter() {
        return new SecurityContextPersistenceFilter(securityContextRepository());
    }

    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new HttpSessionSecurityContextRepository();
    }

    @Bean
    public FilterChainProxy filterChainProxy() throws ConcurrentException {
        return new FilterChainProxy(securityFilterChains());
    }

    @Bean
    public RememberMeAuthenticationProvider rememberMeAuthenticationProvider(){
        return new RememberMeAuthenticationProvider("myKey");
    }

    @Bean
    public List<SecurityFilterChain> securityFilterChains() throws ConcurrentException {
        List<SecurityFilterChain> securityFilterChains = newArrayList();

        securityFilterChains.add(new DefaultSecurityFilterChain(ANT_PATH_MATCH_ALL,
                securityContextPersistenceFilter(),
                servletApiFilter(),
                sessionManagementFilter(),
                exceptionTranslationFilter(),
                logoutFilter(),
                rememberMeAuthenticationFilter(),
                anonymousAuthenticationFilter(),
                filterSecurityInterceptor()
                ));

        return securityFilterChains;
    }

    @Bean
    public SecurityContextHolderAwareRequestFilter servletApiFilter() {
        return new SecurityContextHolderAwareRequestFilter();
    }

    @Bean
    public AnonymousAuthenticationFilter anonymousAuthenticationFilter() {
        return new AnonymousAuthenticationFilter(siteName);
    }

    @Bean
    public LogoutFilter logoutFilter() {
        return new LogoutFilter(new SimpleUrlLogoutSuccessHandler(), new SecurityContextLogoutHandler(), rememberMeServices());
    }

    @Bean
    public FilterSecurityInterceptor filterSecurityInterceptor() throws ConcurrentException {
        DefaultSecurityMetadataSourceBuilder sourceBuilder = defaultSecurityMetadataSourceBuilder();
        sourceBuilder.add("/admin/**", ROLE_ADMIN);
        sourceBuilder.add("/login", IS_AUTHENTICATED_ANONYMOUSLY);
        sourceBuilder.add("/register", IS_AUTHENTICATED_ANONYMOUSLY);
        sourceBuilder.add("/user-cabinet/**", ROLE_USER);

        FilterSecurityInterceptor interceptor = new FilterSecurityInterceptor();
        interceptor.setSecurityMetadataSource(sourceBuilder.toMetadataSource());
        interceptor.setAuthenticationManager(authenticationManager());
        interceptor.setAccessDecisionManager(accessDecisionManager());
        return interceptor;
    }

    @Bean
    public AccessDecisionManager accessDecisionManager() {
        return new AffirmativeBased(Arrays.<AccessDecisionVoter>asList(new RoleVoter(), new AuthenticatedVoter()));
    }

    @Bean
    public ExceptionTranslationFilter exceptionTranslationFilter() {
        LoginUrlAuthenticationEntryPoint authEntryPoint = new LoginUrlAuthenticationEntryPoint(LOGIN_URL) {
            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
                if (request.getMethod().equalsIgnoreCase("POST")) {
                    HttpServletResponse httpResponse = response;
                    httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Access Denied");
                }
                else {
                    super.commence(request, response, authException);
                }
            }
        };
        return new ExceptionTranslationFilter(authEntryPoint, NULL_REQUEST_CACHE);
    }

    @Bean
    public AuthenticationManager authenticationManager() throws ConcurrentException {
        return new ProviderManager(Arrays.asList(rootAuthenticationProvider(), emailAuthenticationProvider(), rememberMeAuthenticationProvider()));
    }

    @Bean
    public AuthenticationProvider rootAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        if (Boolean.getBoolean(rootPasswordUseHash)) {
            provider.setPasswordEncoder(new BCryptPasswordEncoder());
        }
        provider.setUserDetailsService(rootUserDetailsService());
        return provider;
    }

    @Bean
    public AuthenticationProvider emailAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        provider.setUserDetailsService(pwdAndEmailUserDetailsService);
        return provider;
    }

    @Bean
    public UserDetailsService rootUserDetailsService() {
        UserDetails userDetails = new User(rootUserName, rootPassword, asList(new SimpleGrantedAuthority(ROLE_ADMIN)));
        return new InMemoryUserDetailsManager(newArrayList(userDetails));
    }
}
