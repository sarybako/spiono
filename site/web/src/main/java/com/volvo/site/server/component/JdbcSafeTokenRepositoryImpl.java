package com.volvo.site.server.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Date;

@Component
public class JdbcSafeTokenRepositoryImpl extends JdbcTokenRepositoryImpl {

    @Autowired
    public JdbcSafeTokenRepositoryImpl(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Transactional
    @Modifying
    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        super.createNewToken(token);
    }

    @Transactional
    @Modifying
    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        super.updateToken(series, tokenValue, lastUsed);
    }

    @Transactional(readOnly = true)
    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        return super.getTokenForSeries(seriesId);
    }

    @Transactional
    @Modifying
    @Override
    public void removeUserTokens(String username) {
        super.removeUserTokens(username);
    }
}
