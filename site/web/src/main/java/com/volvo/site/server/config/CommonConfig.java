package com.volvo.site.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        com.volvo.site.server.controller.PackageInfo.PACKAGE_NAME,
        com.volvo.site.server.component.PackageInfo.PACKAGE_NAME})
public class CommonConfig  {


}
