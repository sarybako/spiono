package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Syslog;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface SyslogRepository extends JpaRepositoryWithLongId<Syslog> {
}
