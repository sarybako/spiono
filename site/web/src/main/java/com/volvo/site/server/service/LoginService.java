package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.exception.ConfirmRegistrationException;
import com.volvo.site.server.service.exception.DuplicateEntityException;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Locale;

public interface LoginService {

    Registration register(String email, String password, Locale locale) throws DuplicateEntityException;

    Pair<String, User> confirmRegistration(String key) throws EntityNotFoundException, ConfirmRegistrationException;

    void cleanUpOldRegistrations();
}