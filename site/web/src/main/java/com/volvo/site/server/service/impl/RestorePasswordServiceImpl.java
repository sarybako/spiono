package com.volvo.site.server.service.impl;

import com.volvo.platform.spring.qualifiers.AsyncImplementation;
import com.volvo.site.mailing.service.EmailService;
import com.volvo.site.server.model.entity.ForgotPassword;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.ForgotPasswordJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.RestorePasswordService;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.UniqueKeyIsAlreadyUsedException;
import com.volvo.site.server.service.exception.UniqueKeyTimedOutException;
import com.volvo.site.server.service.exception.UserInactiveException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.java.DateTimeUtils.nowMinusDays;
import static com.volvo.platform.spring.BCryptUtil.bcryptHash;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;
import static com.volvo.site.server.service.exception.UserInactiveException.throwExceptionIfUserInactive;

@Service
@Transactional(readOnly = true)
@Slf4j
public class RestorePasswordServiceImpl implements RestorePasswordService{

    @Autowired
    private UserJpaRepository userRepository;

    @Autowired
    private ForgotPasswordJpaRepository forgotPasswordJpaRepository;

    @Autowired
    @AsyncImplementation
    private EmailService emailService;

    @Modifying
    @Override
    public void requestPasswordReset(String email, Locale locale) throws EntityNotFoundException, UserInactiveException{
        RestorePasswordServiceImpl.log.info("> requestPasswordReset");
        checkNotNull(email);

        User user = userRepository.findUserByEmail(email);

        throwEntityNotFoundIfNull(user,User.class,email);
        throwExceptionIfUserInactive(user);

        ForgotPassword forgotPassword = forgotPasswordJpaRepository.findByUserIdAndStatus(user.getId(),
                ForgotPassword.ForgotPasswordStatus.CREATED);

        if( forgotPassword == null ){
            forgotPassword = ForgotPassword.of(user);
        } else {
            forgotPassword.resetCreationDate();
        }

        forgotPasswordJpaRepository.save(forgotPassword);
        emailService.sendForgotPasswordEmail(user.getEmail(), forgotPassword.getUniqueKey(), locale);
    }

    @Override
    public void checkPasswordResetKey(String resetKey) throws EntityNotFoundException, NullPointerException,
            UniqueKeyIsAlreadyUsedException, UniqueKeyTimedOutException, IllegalStateException{
        RestorePasswordServiceImpl.log.info("> checkPasswordResetKey");
        checkResetKeyAndReturnForgotPassword(resetKey);
    }

    @Modifying
    @Override
    public User changePassword(String key, String  newPassword) throws EntityNotFoundException, NullPointerException,
            UniqueKeyIsAlreadyUsedException, UniqueKeyTimedOutException, IllegalStateException{
        RestorePasswordServiceImpl.log.info("> changePassword");
        checkResetKeyIsValidUUID(key);
        checkNotNull(newPassword);

        ForgotPassword forgotPassword = checkResetKeyAndReturnForgotPassword(key);

        forgotPassword.getUser().setPassword(bcryptHash(newPassword));
        forgotPassword.setStatus(ForgotPassword.ForgotPasswordStatus.PWD_CHANGED);
        forgotPassword.setPwdResetDate(new Date());

        return forgotPasswordJpaRepository.save(forgotPassword).getUser();
    }

    private ForgotPassword checkResetKeyAndReturnForgotPassword (String resetKey) throws EntityNotFoundException, NullPointerException,
            UniqueKeyIsAlreadyUsedException, UniqueKeyTimedOutException, IllegalStateException{
        checkResetKeyIsValidUUID(resetKey);

        ForgotPassword forgotPassword = forgotPasswordJpaRepository.findByUniqueKey(resetKey);
        throwEntityNotFoundIfNull(forgotPassword,ForgotPassword.class,resetKey);

        switch (forgotPassword.getStatus()){
            case CREATED:
                return forgotPassword;
            case TIMED_OUT:
                throw new UniqueKeyTimedOutException(resetKey);
            case PWD_CHANGED:
                throw new UniqueKeyIsAlreadyUsedException(resetKey);
            default:
                throw new IllegalStateException("Unhandled enum value" + forgotPassword.getStatus());
        }
    }

    @Scheduled(cron = "0 0 * * * *"/* Every hour*/)
    @Override
    @Modifying
    public void cleanUpOldRequests() {
        Date deadLine = nowMinusDays(2);
        log.info("cleanUpOldForgotPasswordRequests: deadline date is {}", deadLine);

        int cleanedCount = forgotPasswordJpaRepository.markTooOldRequests(deadLine);
        log.info("Cleaned up {} old password recovery requests", cleanedCount);
    }

    private static void checkResetKeyIsValidUUID(String resetKey) {
        checkNotNull(resetKey);
        try {
            UUID.fromString(resetKey);
        } catch (IllegalArgumentException e) {
            throw new EntityNotFoundException(ForgotPassword.class,resetKey);
        }
    }
}
