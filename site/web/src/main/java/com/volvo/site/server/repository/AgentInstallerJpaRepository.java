package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.AgentInstaller;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface AgentInstallerJpaRepository extends JpaRepositoryWithLongId<AgentInstaller> {

    @Modifying
    @Query("update AgentInstaller c set c.active=:active where c.id=:id")
    int setAgentActive(@Param("id") long id, @Param("active") boolean active);

    @Query("select count(c.id) from AgentInstaller c where c.active=true and c.clientInstallerId=:client_installer")
    long getActiveCount(@Param("client_installer") long clientInstallerId);

    @Query("select c from AgentInstaller c where c.active=true and c.clientInstallerId=:client_installer  order by c.id desc")
    Page<AgentInstaller> findLastActiveInstallers(@Param("client_installer") long clientInstallerId, Pageable pageable);
}
