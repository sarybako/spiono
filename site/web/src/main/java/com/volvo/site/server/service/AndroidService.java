package com.volvo.site.server.service;

import com.volvo.site.server.dto.android.AndroidLogDTO;

public interface AndroidService {

	void logAgentData(AndroidLogDTO androidLog);
}
