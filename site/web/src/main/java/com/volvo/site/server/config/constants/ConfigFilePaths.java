package com.volvo.site.server.config.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConfigFilePaths {
    public static final String APP_CONTEXT_PATH = "com/volvo/site/server/config/appcontext.xml";

    public static final String APP_CONTEXT_CLASSPATH = CLASSPATH_URL_PREFIX + APP_CONTEXT_PATH;


    public static final String DB_DEV_CONFIG_PATH = "com/volvo/site/server/config/db-dev.properties";

    public static final String DB_DEV_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + DB_DEV_CONFIG_PATH;

    public static final String DB_TEST_CONFIG_PATH = "com/volvo/site/server/config/db-test.properties";

    public static final String DB_TEST_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + DB_TEST_CONFIG_PATH;

    public static final String DB_PROD_CONFIG_PATH = "com/volvo/site/server/config/db-prod.properties";

    public static final String DB_PROD_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + DB_PROD_CONFIG_PATH;


    public static final String APP_TEST_CONFIG_PATH = "com/volvo/site/server/config/app-test.properties";

    public static final String APP_TEST_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + APP_TEST_CONFIG_PATH;

    public static final String APP_DEV_CONFIG_PATH = "com/volvo/site/server/config/app-dev.properties";

    public static final String APP_DEV_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + APP_DEV_CONFIG_PATH;

    public static final String APP_PROD_CONFIG_PATH = "com/volvo/site/server/config/app-prod.properties";

    public static final String APP_PROD_CONFIG_CLASSPATH = CLASSPATH_URL_PREFIX + APP_PROD_CONFIG_PATH;
}
