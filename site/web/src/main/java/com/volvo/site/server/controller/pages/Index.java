package com.volvo.site.server.controller.pages;

import com.volvo.site.server.form.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.site.server.util.ControllersPaths.IndexController.VIEW_INDEX;

@Controller
public class Index {


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {
		return modelAndViewWithForm(VIEW_INDEX, LoginForm.class);
	}
}
