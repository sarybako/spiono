package com.volvo.site.server.config.db;

import com.googlecode.flyway.core.Flyway;
import com.jolbox.bonecp.BoneCPDataSource;
import com.volvo.platform.spring.conf.hibernate.HibernatePropertiesBuilder;
import com.volvo.site.server.config.constants.DatabaseProperties;
import com.volvo.site.server.model.migrations.PackageInfo;
import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.commons.lang3.concurrent.LazyInitializer;
import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;


public abstract class AbstractDbConfig {
	@Value(DatabaseProperties.DriverClass)
	private String dbDriverClass;
	
	@Value(DatabaseProperties.Url)
	private String dbJdbcUrl;
	
	@Value(DatabaseProperties.UserName)
	private String dbUsername;
	
	@Value(DatabaseProperties.Password)
	private String dbPassword;
	
	@Value(DatabaseProperties.PackagesToScan)
	private String dbPackagesToScan;
	
	@Value(DatabaseProperties.HibernateDialect)
	private String hibernateDialect;
	
	@Value(DatabaseProperties.HibernateFormatSql)
	private Boolean hibernateFormatSQL;
	
	@Value(DatabaseProperties.HibernateShowSql )
	private Boolean hibernateShowSQL;
	
	@Value(DatabaseProperties.HibernateNamingStrategy)
	private String hibernateNamingStrategy;
	
	@Value(DatabaseProperties.HibernateSearchProvider)
	private String hSearchProvider;
	
	@Value(DatabaseProperties.HibernateSearchDir)
	private String hSearchDir;

    private final AtomicBoolean isDBMigrated = new AtomicBoolean(false);

    private final LazyInitializer<Properties> hibernateProperties = new LazyInitializer<Properties>() {
        @Override
        protected Properties initialize() throws ConcurrentException {
            return new HibernatePropertiesBuilder()
                    .withDialect(hibernateDialect)
                    .withSqlFormatting(hibernateFormatSQL)
                    .withNamingStrategy(hibernateNamingStrategy)
                    .withShowingSql(hibernateShowSQL)
                    .withSearchProvider(hSearchProvider)
                    .withSearchDir(hSearchDir)
                    .withUnicodeSuppport()
                    .getProperties();
        }
    };

    private final LazyInitializer<Flyway> lazyDbMigrator = new LazyInitializer<Flyway>(){
        @Override
        protected Flyway initialize() throws ConcurrentException {
            Flyway flyway = new Flyway();
            flyway.setDataSource(dataSource());
            flyway.setBaseDir(PackageInfo.PACKAGE_PATH);
            return flyway;
        }
    };

	@Bean
	public AnnotationSessionFactoryBean hibernateSessionFactory() throws ConcurrentException {
        doDatabaseMigrationsIfNotExecuted();

		AnnotationSessionFactoryBean sessionFactory = new AnnotationSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { dbPackagesToScan });		
		sessionFactory.setHibernateProperties(hibernateProperties.get());
		return sessionFactory;
	}

	@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws ClassNotFoundException, ConcurrentException {
        doDatabaseMigrationsIfNotExecuted();

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setPackagesToScan(dbPackagesToScan);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistence.class);
        entityManagerFactoryBean.setJpaProperties(hibernateProperties.get());
        return entityManagerFactoryBean;
    }

    @Bean
    public DataSource dataSource() {
        BoneCPDataSource dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(dbDriverClass);
        dataSource.setJdbcUrl(dbJdbcUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException, ConcurrentException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

    @Bean
    public Flyway databaseMigrator() throws ConcurrentException {
        return lazyDbMigrator.get();
    }

    protected void doDatabaseMigrationsIfNotExecuted() throws ConcurrentException {
        if (!isDBMigrated.getAndSet(true)) {
            doDatabaseMigrations();
        }
    }

    protected void doDatabaseMigrations() throws ConcurrentException {
        databaseMigrator().migrate();
    }
}
