package com.volvo.site.server.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import static com.google.common.base.Preconditions.checkArgument;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SpringDataUtil {

    public static final Pageable ONE_ELEM_PAGEABLE = new PageRequest(0, 1);

    public static final Sort ORDER_BY_ID_DESC = new Sort(Sort.Direction.DESC, "id");

    public static void checkPageSize(int pageSize) {
        checkArgument(pageSize <= Constants.maxPageSize, "Page size can not be larger than " + Constants.maxPageSize);
    }

    public static <T> T firstOrNull(Page<T> result) {
        return result.hasContent() ? result.iterator().next() : null;
    }
}
