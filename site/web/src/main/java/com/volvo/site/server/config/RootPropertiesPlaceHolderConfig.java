package com.volvo.site.server.config;

import com.volvo.site.server.config.placeholder.PropertiesPlaceHolderDevConfig;
import com.volvo.site.server.config.placeholder.PropertiesPlaceHolderProdConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({PropertiesPlaceHolderDevConfig.class, PropertiesPlaceHolderProdConfig.class})
public class RootPropertiesPlaceHolderConfig {
}
