package com.volvo.site.server.controller.pages;

import com.volvo.site.server.component.Authenticator;
import com.volvo.site.server.form.LoginForm;
import com.volvo.site.server.form.RegisterForm;
import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.LoginService;
import com.volvo.site.server.service.exception.ConfirmRegistrationException;
import com.volvo.site.server.service.exception.DuplicateEntityException;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.util.MessagesConsts;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Locale;

import static com.volvo.platform.java.EmailUtils.getEmailSitename;
import static com.volvo.platform.spring.MvcUtil.modelAndView;
import static com.volvo.platform.spring.MvcUtil.modelAndViewWithForm;
import static com.volvo.site.server.util.ControllersPaths.IndexController.*;

@Controller
@RequestMapping(value = REGISTER)
public class Register {
    @Autowired
    private LoginService loginService;

    @Autowired
    private Authenticator authenticator;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView register() {
        return modelAndViewWithForm(VIEW_REGISTER, RegisterForm.class);
    }

    @RequestMapping(value = "/registerAjax", method = RequestMethod.POST)
    @ResponseBody
    public String registerAjax(@Valid RegisterForm form) throws InterruptedException {
        try {
            loginService.register(form.getEmail(), form.getPassword(), Locale.ENGLISH);
            return "ok";
        }
        catch (DuplicateEntityException exception) {
            return "email_busy";
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView register(@Valid RegisterForm form, BindingResult bindingResult, HttpServletRequest request,
                                 HttpServletResponse response) {
        if (bindingResult.hasErrors()) {
            return modelAndView(VIEW_REGISTER, bindingResult);
        }

        try {
            Registration registration = loginService.register(form.getEmail(), form.getPassword(),
                    RequestContextUtils.getLocaleResolver(request).resolveLocale(request));

            User user = User.of(registration);
            user.setActive(true);
            userJpaRepository.save(user);

            return authenticator.authenticateAndRedirect(form.getEmail(), form.getPassword(), request, response);
        }
        catch (DuplicateEntityException exception) {
            return modelAndView(VIEW_REGISTER, bindingResult).addObject("duplicateEmail", Boolean.TRUE);
        }
    }

    @RequestMapping(value = "/registration-confirm/{regCode}", method = RequestMethod.GET)
    public ModelAndView registerConfirm(@PathVariable("regCode") String regCode,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {
        try {
            Pair<String, User> confirmResult = loginService.confirmRegistration(regCode);
            String email = confirmResult.getRight().getEmail();
            String password = confirmResult.getLeft();
            return authenticator.authenticateAndRedirect(email, password, request, response);
        }
        catch (EntityNotFoundException exception) {
            return new ModelAndView("register-confirm");
        }
        catch (ConfirmRegistrationException exception) {
            String email = exception.registration.getEmail();
            switch (exception.reason) {
                case ALREADY_REGISTERED:
                    LoginForm loginForm = new LoginForm();
                    loginForm.setEmail(email);
                    return modelAndViewWithForm(VIEW_LOGIN, loginForm,
                            new ModelMap("confirmRegistration", Boolean.TRUE));
                case TIMEOUT:
                    RegisterForm registerForm = new RegisterForm();
                    registerForm.setEmail(email);
                    return modelAndViewWithForm(VIEW_REGISTER, registerForm,
                            new ModelMap("confirmRegistration", Boolean.TRUE));
                default:
                    throw new IllegalStateException("Unhandled enum value " + exception.reason);
            }
        }
    }

}
