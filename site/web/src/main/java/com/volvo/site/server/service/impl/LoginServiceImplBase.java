package com.volvo.site.server.service.impl;

import com.volvo.site.server.repository.RegistrationJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;

abstract class LoginServiceImplBase {

    @Autowired
    protected RegistrationJpaRepository registrationRepository;

    @Autowired
    protected UserJpaRepository userRepository;
}
