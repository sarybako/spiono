package com.volvo.site.server.dto.android;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
public class AndroidCallDataDTO {

	@JsonProperty(value = "StartDate")
	@NotNull
	@Min(value = 1)
	private long startDate;

	@JsonProperty(value = "EndDate")
	@NotNull
	@Min(value = 1)
	private long endDate;

	@JsonProperty(value = "Type")
	private CallType type;

	@JsonProperty(value = "Number")
	private String number;

	@JsonProperty(value = "Name")
	private String name;

}
