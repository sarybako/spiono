package com.volvo.site.server.dto;

import com.volvo.site.server.util.Constants;
import lombok.Getter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * User: Vladimir Korobkov
 * Date: 03/05/12
 * Time: 14:53
 */
@ToString
@Getter
public class RegisterNewAppDTO {

    @JsonProperty(value = "InstallerId")
    @NotNull
    @Length(min = Constants.installerIdLen, max = Constants.installerIdLen)
    private String installerId;
}
