package com.volvo.site.server.util;


import com.google.common.base.Objects;
import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.dto.KeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;

import static com.volvo.platform.java.testing.TestUtils.randomInt;
import static com.volvo.site.server.model.ModelConstants.spyItemMaxTextLen;
import static com.volvo.site.server.model.ModelConstants.spyItemMaxWindowTextLen;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class SpyKeyLogDataDTOMocker {

    public final String text = randomAlphabetic(spyItemMaxTextLen);
    public final long stardDate = System.currentTimeMillis() - 1;
    public final long endDate = System.currentTimeMillis() - 1;
    public final int hwnd = randomInt();
    public final String windowText = randomAlphabetic(spyItemMaxWindowTextLen);
    public final String processName = randomAlphabetic(spyItemMaxWindowTextLen);
    public final long agentId = randomInt();

    public final KeyLogDataDTO dto = new KeyLogDataDTO();

    public SpyKeyLogDataDTOMocker() {
        dto.setText(text);
        dto.setStartDate(stardDate);
        dto.setEndDate(endDate);
        dto.setHWND(hwnd);
        dto.setWindowText(windowText);
        dto.setProcessName(processName);
    }

    public KeyLogDTO spyLogDTO(String certificate) {
        KeyLogDTO spyLogDTO = new KeyLogDTO(new KeyLogDataDTO[]{ dto });
        spyLogDTO.setCertificate(certificate);
        return spyLogDTO;
    }

    public boolean assertDTOEqualsToModel(KeyLog keyLog) {
        return Objects.equal(text, keyLog.getText()) &&
                Objects.equal(stardDate, keyLog.getStartDate().getTime()) &&
                Objects.equal(endDate, keyLog.getEndDate().getTime()) &&
                Objects.equal((long)hwnd, keyLog.getHwnd()) &&
                Objects.equal(windowText, keyLog.getWindowText()) &&
                Objects.equal(processName, keyLog.getProcessName()) &&
                Objects.equal(agentId, keyLog.getAgentId());
    }
}
