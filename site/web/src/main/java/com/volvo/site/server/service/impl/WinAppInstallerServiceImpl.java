package com.volvo.site.server.service.impl;

import com.volvo.site.server.dto.UpdateAgentResult;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.*;
import com.volvo.site.server.service.WinAppInstallerService;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.java.IOUtils.bytesToBase64String;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;
import static com.volvo.site.server.util.SpringDataUtil.ORDER_BY_ID_DESC;
import static com.volvo.site.server.util.SpringDataUtil.checkPageSize;


@Service
@Transactional(readOnly = true)
class WinAppInstallerServiceImpl implements WinAppInstallerService {

    private final ClientInstallerJpaRepository clientJpaRepository;
    private final AgentInstallerJpaRepository agentInstallerJpaRepository;
    private final AgentInstallerRepository agentInstallerRepository;
    private final ClientInstallerRepository clientRepository;
    private final AgentUtilService agentUtilService;
    private final AgentJpaRepository agentJpaRepository;

    @Autowired
    WinAppInstallerServiceImpl(ClientInstallerJpaRepository clientJpaRepository,
                               AgentInstallerJpaRepository agentInstallerJpaRepository,
                               AgentInstallerRepository agentInstallerRepository,
                               ClientInstallerRepository clientRepository,
                               AgentUtilService agentUtilService, AgentJpaRepository agentJpaRepository) {
        this.clientJpaRepository = clientJpaRepository;
        this.agentInstallerJpaRepository = agentInstallerJpaRepository;
        this.agentInstallerRepository = agentInstallerRepository;
        this.clientRepository = clientRepository;
        this.agentUtilService = agentUtilService;
        this.agentJpaRepository = agentJpaRepository;
    }

    @Modifying
    @Override
    public ClientInstaller addClientInstaller(byte[] data) {
        checkNotNull(data);

        ClientInstaller installer = ClientInstaller.of(data);
        clientJpaRepository.save(installer);
        return installer;
    }

    @Modifying
    @Override
    public AgentInstaller addAgentInstaller(long clientInstallerId, byte[] data, boolean isActive)
            throws EntityNotFoundException {
        checkNotNull(data);
        ClientInstaller clientInstaller = findClientInstallerOrThrow(clientInstallerId);
        return agentInstallerJpaRepository.save(AgentInstaller.of(data, clientInstaller, isActive));
    }

    @Modifying
    @Override
    public void clearAll() {
        clientJpaRepository.deleteAll();
    }

    @Modifying
    @Override
    public void activateAgentInstaller(long agentInstallerId) throws EntityNotFoundException {
        int affectedRowsCount = agentInstallerJpaRepository.setAgentActive(agentInstallerId, true);
        checkAffectedRowsCountIsOne(affectedRowsCount, AgentInstaller.class, agentInstallerId);
    }

    @Modifying
    @Override
    public void inactivateAgentInstaller(long agentInstallerId) throws ServiceException {
        AgentInstaller agentInstaller = agentInstallerJpaRepository.findOne(agentInstallerId);
        throwEntityNotFoundIfNull(agentInstaller, AgentInstaller.class, agentInstallerId);

        if (!agentInstaller.isActive()) {
            return;
        }

        ClientInstaller clientInstaller = agentInstaller.getClientInstaller();
        if (clientInstaller.isActiveOrDownloaded() &&
                agentInstallerJpaRepository.getActiveCount(clientInstaller.getId()) < 2L) {
            throw new ServiceException("Must be at least one active agent installer");
        }

        agentInstaller.setActive(false);
        agentInstallerJpaRepository.save(agentInstaller);
    }

    @Modifying
    @Override
    public void activateClientInstaller(long clientInstallerId) throws ServiceException {
        ClientInstaller clientInstaller = findClientInstallerOrThrow(clientInstallerId);
        if (clientInstaller.isActive()) {
            return;
        }

        if (!agentInstallerRepository.hasActiveAgentInstaller(clientInstallerId)) {
            throw new ServiceException("Can't activate client installer couse no active agent installers");
        }

        clientInstaller.setActive(true);
        clientJpaRepository.save(clientInstaller);
    }

    @Modifying
    @Override
    public void inactivateClientInstaller(long clientInstallerId) throws ServiceException {
        ClientInstaller clientInstaller = findClientInstallerOrThrow(clientInstallerId);

        if (!clientInstaller.isActive()) {
            return;
        }

        if (clientInstaller.isDownloaded()) {
            throw new ServiceException("Can not inactivate client installer, because it is already downloaded by users");
        }

        clientInstaller.setActive(false);
        clientJpaRepository.save(clientInstaller);
    }

    @Override
    public ClientInstaller findLastActiveClientInstaller() throws EntityNotFoundException {
        ClientInstaller lastActive = clientRepository.findLastActive();
        throwEntityNotFoundIfNull(lastActive, ClientInstaller.class, null);
        return lastActive;
    }

    @Override
    public AgentInstaller findLastActiveAgentInstaller(long clientInstallerId) throws EntityNotFoundException {
        if (!findClientInstallerOrThrow(clientInstallerId).isActive()) {
            throw new ServiceException("Client installer is inactive, so can not get active agent installer");
        }

        AgentInstaller lastActive = agentInstallerRepository.findLastActive(clientInstallerId);
        throwEntityNotFoundIfNull(lastActive, AgentInstaller.class, null);
        return lastActive;
    }

    @Modifying
    @Override
    public void setClientInstallerDownloaded(long clientInstallerId) throws ServiceException {
        ClientInstaller clientInstaller = findClientInstallerOrThrow(clientInstallerId);
        if (!clientInstaller.isActive()) {
            throw new ServiceException("Client installer is inactive, so can not mark it as downloaded");
        }

        if (!clientInstaller.isDownloaded()) {
            clientInstaller.setDownloaded(true);
            clientJpaRepository.save(clientInstaller);
        }
    }

    @Override
    public Page<ClientInstaller> findLastActiveClientInstallers(int pageNumber, int pageSize) {
        checkPageSize(pageSize);

        Pageable pageable = new PageRequest(pageNumber, pageSize, ORDER_BY_ID_DESC);
        return clientJpaRepository.findAll(pageable);
    }

    @Override
    public Page<AgentInstaller> findLastActiveAgentInstallers(long clientInstallerId, int pageNumber, int pageSize) {
        checkPageSize(pageSize);
        return agentInstallerJpaRepository.findLastActiveInstallers(clientInstallerId, new PageRequest(pageNumber, pageSize));
    }

    @Modifying
    @Override
    public UpdateAgentResult updateAgentLib(String certificate) {
        agentUtilService.checkAgentCertificate(certificate);

        Agent agent = agentUtilService.findByCertificate(certificate);
        AgentInstaller lastAgentInstaller = findLastActiveAgentInstaller(agent.getClientInstaller().getId());

        if (agent.getAgentInstaller().getId().equals(lastAgentInstaller.getId())) {
            return UpdateAgentResult.NO_AGENT_UPDATES;
        }

        agent.setAgentInstaller(lastAgentInstaller);
        agentJpaRepository.save(agent);
        return new UpdateAgentResult(true, bytesToBase64String(lastAgentInstaller.getData()));
    }

    private ClientInstaller findClientInstallerOrThrow(long clientInstallerId) throws EntityNotFoundException {
        ClientInstaller clientInstaller = clientJpaRepository.findOne(clientInstallerId);
        throwEntityNotFoundIfNull(clientInstaller, ClientInstaller.class, clientInstallerId);
        return clientInstaller;
    }

    private void checkAffectedRowsCountIsOne(int affectedRowsCount, Class<?> entity, long id) {
        if (affectedRowsCount == 0) {
            throw new EntityNotFoundException(entity, id);
        } else if (affectedRowsCount > 1) {
            throw new ServiceException("Affected rows count more than one, but must be one!" +
                    "Clazz = ["+ entity + "], id = [" + id + "], affectedRowsCount = " + affectedRowsCount);
        }
    }
}
