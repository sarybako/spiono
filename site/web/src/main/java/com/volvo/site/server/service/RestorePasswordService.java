package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.UniqueKeyIsAlreadyUsedException;
import com.volvo.site.server.service.exception.UniqueKeyTimedOutException;
import com.volvo.site.server.service.exception.UserInactiveException;

import java.util.Locale;

public interface RestorePasswordService {

	void requestPasswordReset(String email, Locale locale) throws EntityNotFoundException, UserInactiveException;

    void checkPasswordResetKey(String resetKey) throws UniqueKeyIsAlreadyUsedException, UniqueKeyTimedOutException,
            EntityNotFoundException, NullPointerException;

    User changePassword(String key, String  newPassword) throws EntityNotFoundException,
            UniqueKeyIsAlreadyUsedException, UniqueKeyTimedOutException;

    void cleanUpOldRequests();

}
