package com.volvo.site.server.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

@Configuration
public class I18nConfig {
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(new String[] {"WEB-INF/i18n/messages"});
	    messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver result = new CookieLocaleResolver();
		result.setCookieName("locale");
		result.setCookieMaxAge(60 * 60 * 24 * 30 * 12); // 1 year :)
		return result;
	}
}
