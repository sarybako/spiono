package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.KeyLog;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface KeyLogJpaRepository extends JpaRepositoryWithLongId<KeyLog> {

	@Query("select" +
            "   kl from KeyLog kl " +
            "where" +
            "   kl.agentId=:agentId and " +
            "     (:searchingString is null or (kl.text like :searchingString " +
            "     or kl.windowText like :searchingString" +
            "     or kl.processName like :searchingString)) and " +
            "   (:searchByDate=false or (:startDate > kl.startDate and :endDate < kl.endDate))")
	List<KeyLog> findByAgentId(@Param("agentId") long agentId, @Param("searchingString") String searchingString,
                               @Param("searchByDate") boolean searchByDate, @Param("startDate") Date startDate,
                               @Param("endDate") Date endDate);

    @Modifying
    @Override
    <S extends KeyLog> List<S> save(Iterable<S> entities);
}
