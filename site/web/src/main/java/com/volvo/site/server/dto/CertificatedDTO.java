package com.volvo.site.server.dto;

import com.volvo.site.server.util.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * User: Vladimir Korobkov
 * Date: 24.03.12
 * Time: 17:09
 */
@ToString
@Getter
@Setter
public class CertificatedDTO {

    @JsonProperty(value = "Certificate")
    @NotNull
    @Length(min = Constants.certificateLen, max = Constants.certificateLen)
    private String certificate;
}
