package com.volvo.site.server.dto;

import com.volvo.site.server.util.Constants;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * User: Vladimir Korobkov
 * Date: 24.03.12
 * Time: 18:28
 */
@ToString
public class ErrorDTO {

    @JsonProperty(value = "Message")
    @NotNull
    @NotEmpty
    @Length(max= Constants.errorMessageMax)
    private String message;

    @JsonProperty(value = "InstallerId")
    @Length(min = Constants.installerIdLen, max = Constants.installerIdLen)
    private String installerId;

    @JsonProperty(value = "Certificate")
    @Length(min = Constants.certificateLen, max = Constants.certificateLen)
    private String certificate;

    @JsonProperty(value = "IsShowingErrorsMode")
    @NotNull
    private Boolean isShowingErrorsMode;

    @JsonProperty(value = "LogFileTail")
    @Length(max= Constants.errorMessageMax)
    private String logFileTail;
}
