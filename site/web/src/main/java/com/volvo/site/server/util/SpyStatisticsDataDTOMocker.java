package com.volvo.site.server.util;

import com.google.common.base.*;
import com.volvo.site.server.dto.*;
import com.volvo.site.server.model.entity.*;

import static com.volvo.site.server.model.ModelConstants.*;
import static org.apache.commons.lang3.RandomStringUtils.*;


public class SpyStatisticsDataDTOMocker {
	public final StatisticsDataDTO dto = new StatisticsDataDTO();

	public SpyStatisticsDataDTOMocker() {
		dto.setStartDate(System.currentTimeMillis() - 1);
		dto.setEndDate(System.currentTimeMillis() - 1);
		dto.setProcessName(randomAlphabetic(spyItemMaxWindowTextLen));
		dto.setProcessDescription(randomAlphabetic(spyItemMaxProcessDescriptionTextLen));
	}

	public SpyStatisticsDataDTOMocker(String processName, String processDescription, long startDate, long endDate) {
		dto.setStartDate(startDate);
		dto.setEndDate(endDate);
		dto.setProcessName(processName);
		dto.setProcessDescription(processDescription);
	}

	public StatisticsLogDTO spyStatisticsDTO(String certificate) {
		StatisticsLogDTO spyStatisticsDTO = new StatisticsLogDTO(new StatisticsDataDTO[]{ dto });
		spyStatisticsDTO.setCertificate(certificate);
		return spyStatisticsDTO;
	}

	public static StatisticsLogDTO spyStatisticsDTO(StatisticsDataDTO dto, String certificate) {
		StatisticsLogDTO spyStatisticsDTO = new StatisticsLogDTO(new StatisticsDataDTO[]{ dto });
		spyStatisticsDTO.setCertificate(certificate);
		return spyStatisticsDTO;
	}

	public boolean assertDTOEqualsToModel(StatisticsLog statisticsLog) {
		return  Objects.equal(dto.getStartDate(), statisticsLog.getStartDate().getTime()) &&
				Objects.equal(dto.getEndDate(), statisticsLog.getEndDate().getTime()) &&
				Objects.equal(dto.getProcessDescription(), statisticsLog.getProcessDescription()) &&
				Objects.equal(dto.getProcessName(), statisticsLog.getProcessName());
	}
}

