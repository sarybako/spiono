package com.volvo.site.server.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ProcessBlackList {

	private static final List<String> BLACK_LIST = new ArrayList<String>();

	static {
		BLACK_LIST.addAll(Arrays.asList(
				"Idle",
				"explorer",
				"javaw",
				"java",
				"Camera Software",
				"taskmgr",
				"taskhost",
				"Firefox Software Updater",
				"spyloader",
				"rundll32",
				"iesetup",
				"dwm",
				"csrss",
				"Product Registration",
				"Notification Tool",
				"GUP : a free (LGPL) Generic Updater",
				"Firefox Software Updater",
				"CAB-файл с автоизвлечением",
				"7zG",
				"DeviceProperties",
				"SndVol",
				"setup",
				"Хост-процесс Windows (Rundll32)"));
	}

	public static final List<String> getList(){
		return BLACK_LIST;
	}
}
