package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.ClientInstaller;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface ClientInstallerJpaRepository extends JpaRepositoryWithLongId<ClientInstaller>
{
    @Query("select c from ClientInstaller c where c.active=true order by c.id desc")
    Page<ClientInstaller> findLastActiveInstallers(Pageable pageable);

    @Modifying
    @Query("update ClientInstaller c set c.active=:active where c.id=:id")
    int setClientActive(@Param("id") long id, @Param("active") boolean active);
}
