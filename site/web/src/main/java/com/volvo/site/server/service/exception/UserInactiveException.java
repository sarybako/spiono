package com.volvo.site.server.service.exception;

import com.volvo.site.server.model.entity.User;

public class UserInactiveException extends ServiceException {
    public static void throwExceptionIfUserInactive(User user) {
        if (!user.isActive()) {
            throw new UserInactiveException(user.getEmail());
        }
    }

    public UserInactiveException(String email){
        super("Exception. User " + email + " is inactive.");
    }

}
