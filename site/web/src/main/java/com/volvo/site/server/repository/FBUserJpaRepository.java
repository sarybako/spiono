package com.volvo.site.server.repository;


import com.volvo.site.server.model.entity.FacebookUser;
import com.volvo.site.server.model.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface FBUserJpaRepository extends JpaRepositoryWithLongId<FacebookUser>  {

    FacebookUser findByFbUserId(Long id);

    FacebookUser findBySpionoUser(User user);

}
