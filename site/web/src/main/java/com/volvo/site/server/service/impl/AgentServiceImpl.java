package com.volvo.site.server.service.impl;

import com.volvo.site.server.dto.HeartbeatResult;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.WinAppInstallerService;
import com.volvo.site.server.service.impl.util.AgentUtilService;
import com.volvo.site.server.service.impl.util.UserUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;
import static com.volvo.site.server.service.impl.util.AgentUtilService.checkCertificate;
import static com.volvo.site.server.service.impl.util.AgentUtilService.throwIllegalStateExceptionIfNotRegistered;
import static com.volvo.site.server.util.SpringDataUtil.checkPageSize;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@Transactional(readOnly = true)
public class AgentServiceImpl implements AgentService {

    private final AgentUtilService agentUtilService;
    private final AgentJpaRepository agentJpaRepository;
    private final UserUtilService userUtilService;
    private final WinAppInstallerService winAppInstallerService;

    @Autowired
    public AgentServiceImpl(AgentUtilService agentUtilService, AgentJpaRepository agentJpaRepository,
                            UserUtilService userUtilService, WinAppInstallerService winAppInstallerService) {
        this.agentUtilService = agentUtilService;
        this.agentJpaRepository = agentJpaRepository;
        this.userUtilService = userUtilService;
        this.winAppInstallerService = winAppInstallerService;
    }

    @Modifying
    @Override
    public Agent deleteAgentAndCreateNewOne(long userId, long agentId) {
        agentJpaRepository.delete(agentId);
        return createAgent(userId);
    }

    @Modifying
    @Override
    public void deleteAgent(long agentId) {
        Agent agent = agentUtilService.findAgentById(agentId);
        agent.setOnPause(false);
        agent.setStatus(Agent.AgentStatus.AGENT_DELETED);
        agentJpaRepository.save(agent);
    }

    @Modifying
    @Override
    public Agent createAgent(long userId) {
        return createAgent(userId, null);
    }

    @Modifying
    @Override
    public Agent createAgent(long userId, String name) {
        User user = userUtilService.getActiveUser(userId);
        ClientInstaller clientInstaller = winAppInstallerService.findLastActiveClientInstaller();

        Agent agent = Agent.of(user, clientInstaller);
        agent.setName(name);
        return agentJpaRepository.save(agent);
    }

    @Modifying
    @Override
    public byte[] downloadAgent(long agentId, String clientInstallerKey) {
        checkNotNull(clientInstallerKey);

        Agent agent = agentJpaRepository.findByIdAndClientKey(agentId, clientInstallerKey);
        throwEntityNotFoundIfNull(agent, Agent.class, agentId);

        ClientInstaller clientInstaller = agent.getClientInstaller();
        winAppInstallerService.setClientInstallerDownloaded(clientInstaller.getId());

        agent.setStatus(Agent.AgentStatus.AGENT_DOWNLOADED);
        agentJpaRepository.save(agent);

        return clientInstaller.getData();
    }

    @Override
    public boolean isAgentRegistered(long agentId) {
        return agentJpaRepository.findByIdAndStatus(agentId, Agent.AgentStatus.AGENT_REGISTERED) != null;
    }

    @Modifying
    @Override
    public Agent registerAgent(String clientInstallerKey) {
        checkNotNull(clientInstallerKey);
        checkArgument(clientInstallerKey.length() == ModelConstants.clientInstallerKeyLen);

        Agent agent = agentJpaRepository.findByClientKeyAndStatus(clientInstallerKey, Agent.AgentStatus.AGENT_DOWNLOADED);
        throwEntityNotFoundIfNull(agent, Agent.class, clientInstallerKey);

        agent.generateCertificate();
        agent.setAgentInstaller(winAppInstallerService.findLastActiveAgentInstaller(agent.getClientInstaller().getId()));
        agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);
        agent = agentJpaRepository.save(agent);

        return agent;
    }

    @Modifying
    @Override
    public void setAgentName(long agentId, String name) {
        checkNotNull(name);

        Agent agent = agentUtilService.findAgentById(agentId);
        agent.setName(name);
        agentJpaRepository.save(agent);
    }

    @Modifying
    @Override
    public HeartbeatResult makeHeartBeat(String certificate) {
        checkCertificate(certificate);

        Agent agent = agentJpaRepository.findByCertificate(certificate);
        if (agent == null || agent.isDeleted()) {
            return HeartbeatResult.UNINSTALL;
        }

        if (agent.isOnPause()) {
            return HeartbeatResult.PAUSE;
        }

        agent.updateHeartBeatTime();
        agentJpaRepository.save(agent);

        return HeartbeatResult.OK;
    }

    @Override
    public Page<Agent> getAgents(long userId, String searchingString, List<Agent.AgentStatus> statusFilter,
                                 int pageNumber, int pageSize) {
        checkPageSize(pageSize);

        boolean isStatusFilterEnabled = !isEmpty(statusFilter);
        if(!isStatusFilterEnabled){
            statusFilter = newArrayList(Agent.AgentStatus.values());
        }
        searchingString = wrapSearchingStringToQuery(searchingString);
        return  agentJpaRepository.findAgentsForUser(userId, searchingString, isStatusFilterEnabled,
                statusFilter, new PageRequest(pageNumber, pageSize));
    }

    @Override
    public List<Agent> getAgents(long userId, List<Agent.AgentStatus> statusFilter) {
        boolean isStatusFilterEnabled = !isEmpty(statusFilter);
        if(!isStatusFilterEnabled){
            statusFilter = newArrayList(Agent.AgentStatus.values());
        }
        return agentJpaRepository.findAgentsForUser(userId, isStatusFilterEnabled, statusFilter);
    }

    @Modifying
    @Override
    public void setAgentOnPause(long agentId, boolean onPause){
        Agent agent = agentUtilService.findAgentById(agentId);
        throwIllegalStateExceptionIfNotRegistered(agent);
        if(agent.isOnPause() != onPause){
            agent.setOnPause(onPause);
            agentJpaRepository.save(agent);
        }
    }

    private static String wrapSearchingStringToQuery(String searchingString){
        return isBlank(searchingString) ? null :  "%" + searchingString + "%" ;
    }
}
