package com.volvo.site.server.util.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * User: Vladimir Korobkov
 * Date: 01.03.12
 * Time: 23:52
 */

@ResponseStatus(value = BAD_REQUEST, reason = "Bad request (error 400)")
public class BadRequestException extends RuntimeException {

    public BadRequestException() {
    }
}
