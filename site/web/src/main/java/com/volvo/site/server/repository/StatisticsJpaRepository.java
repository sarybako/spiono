package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.StatisticsLog;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public interface StatisticsJpaRepository extends JpaRepositoryWithLongId<StatisticsLog>{

	@Override
	@Modifying
	<S extends StatisticsLog> List<S> save(Iterable<S> entities);

	@Query("select st from StatisticsLog st where " +
			" st.agentId=:agentId and (:searchByDate=false or (:startDate <= st.startDate and :endDate >= st.endDate)) " +
			"and st.processName not in (:blackList) and (st.processDescription is null or st.processDescription not in (:blackList))")
	List<StatisticsLog> findByAgentId(@Param("agentId") long agentId, @Param("searchByDate") boolean searchByDate,
	                           @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("blackList") List<String> blackList);

}
