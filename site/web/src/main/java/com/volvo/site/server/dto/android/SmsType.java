package com.volvo.site.server.dto.android;

public enum SmsType {
    INCOMING, OUTGOING
}
