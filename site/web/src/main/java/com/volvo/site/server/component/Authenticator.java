package com.volvo.site.server.component;


import com.volvo.site.server.repository.RegistrationJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.AuthenticationException;
import com.volvo.site.server.service.exception.NotAuthenticatedAsUserException;
import com.volvo.site.server.util.ControllersPaths;
import com.volvo.site.server.util.CookieConsts;
import com.volvo.site.server.util.ExtendedUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.platform.spring.MvcUtil.newCookie;
import static com.volvo.platform.spring.MvcUtil.redirectToView;
import static com.volvo.site.server.util.ServerUtil.hasAdminAuthority;
import static com.volvo.site.server.util.ServerUtil.hasUserAuthority;

@Component
@Lazy
public class Authenticator {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private SessionAuthenticationStrategy sessionAuthenticationStrategy;

    @Autowired
    private SecurityContextRepository securityContextRepository;

    @Autowired
    private RegistrationJpaRepository registrationJpaRepository;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private RememberMeServices rememberMeServices;

    public ModelAndView authenticateAndRedirect(String email, String password, HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        response.addCookie(newCookie(CookieConsts.EMAIL, email));
        Authentication authentication = authenticateWithServletApi(email, password, request, response);
        rememberMeServices.loginSuccess(request, response, authentication);
        return getInitialViewAfterLogin(authentication);
    }

    public Authentication authenticateWithServletApi(String email, String password, HttpServletRequest request,
                                                     HttpServletResponse response) throws AuthenticationException {
        checkNotNull(request);
        checkNotNull(response);

        Authentication authentication = authenticate(email, password);

        sessionAuthenticationStrategy.onAuthentication(authentication, request, response);
        securityContextRepository.saveContext(SecurityContextHolder.getContext(), request, response);

        return authentication;
    }

    public Authentication authenticate(String email, String password) {
        checkNotNull(email);
        checkNotNull(password);

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(email, password);
        Authentication authentication = tryToAuthenticate(authRequest);

        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(authentication);
        return authentication;
    }

    public static long getCurrentUserId() throws NotAuthenticatedAsUserException {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        if (authentication == null) {
            throw new NotAuthenticatedAsUserException("Not authenticated at all");
        }
        if (!hasUserAuthority(authentication)) {
            throw new NotAuthenticatedAsUserException("Current user have not ROLE_USER role");
        }
        ExtendedUserDetails userDetails = (ExtendedUserDetails)authentication.getPrincipal();
        return userDetails.getUserId();
    }

    private Authentication tryToAuthenticate(UsernamePasswordAuthenticationToken authRequest) {
        String email = authRequest.getName();
        try {
            Authentication authentication = authenticationManager.authenticate(authRequest);
            if (hasUserAuthority(authentication)) {
                userJpaRepository.resetLoginErrorsCount(email);
            }
            return authentication;
        }
        catch (org.springframework.security.core.AuthenticationException exception) {
            Long errorLoginsCount = incrementAndGetErrorLoginsCount(email);
            boolean haveToConfirmRegistration = false;
            if (errorLoginsCount == null) {
                haveToConfirmRegistration = registrationJpaRepository.countPendingByEmail(email) > 0;
            }
            throw new AuthenticationException(exception, haveToConfirmRegistration, errorLoginsCount);
        }
    }

    private static ModelAndView getInitialViewAfterLogin(Authentication authentication) {
        if (hasUserAuthority(authentication)) {
            return redirectToView(ControllersPaths.IndexController.VIEW_USERCABINET);
        }
        if (hasAdminAuthority((authentication))) {
            return redirectToView(ControllersPaths.IndexController.VIEW_ADMIN);
        }
        throw new IllegalStateException("Unhandled authentication role: " + authentication);
    }

    @Transactional
    @Modifying
    private Long incrementAndGetErrorLoginsCount(String email) {
        checkNotNull(email);

        Long count = userJpaRepository.findErrLoginsByEmail(email);
        if (count == null) {
            return null;
        }

        ++count;
        userJpaRepository.setLoginErrorsCount(email, count);
        return count;
    }
}
