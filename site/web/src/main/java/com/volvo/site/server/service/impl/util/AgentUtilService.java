package com.volvo.site.server.service.impl.util;

import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.repository.AgentJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.volvo.site.server.service.exception.EntityNotFoundException.throwEntityNotFoundIfNull;

@Service
@Transactional(readOnly = true)
public class AgentUtilService {

	private AgentJpaRepository agentJpaRepository;

    // CGLib constructor
    @SuppressWarnings("unused")
    public AgentUtilService() {
    }

    @Autowired
    public AgentUtilService(AgentJpaRepository agentJpaRepository) {
        this.agentJpaRepository = agentJpaRepository;
    }

    public static void checkCertificate(String agentCertificate) {
		checkNotNull(agentCertificate);
		checkArgument(agentCertificate.length() == ModelConstants.agentCertificateLen);
	}

	public static boolean canAgentLog(Agent agent) {
		return agent != null && !agent.isOnPause() && agent.getStatus().equals(Agent.AgentStatus.AGENT_REGISTERED);
	}

	public static Agent throwIllegalStateExceptionIfNotRegistered(Agent agent){
		if(agent.getStatus() != Agent.AgentStatus.AGENT_REGISTERED ){
			throw new IllegalStateException("Agent " + agent.getId() + " is not registered!");
		}
		return agent;
	}

    public void checkAgentCertificate(String certificate) {
        checkCertificate(certificate);
    }

    public Agent findAgentById(long agentid) {
		return throwEntityNotFoundIfNull(agentJpaRepository.findOne(agentid), Agent.class, agentid);
	}

    public Agent findByCertificate(String certificate) {
        return throwEntityNotFoundIfNull(agentJpaRepository.findByCertificate(certificate), Agent.class, certificate);
    }
}
