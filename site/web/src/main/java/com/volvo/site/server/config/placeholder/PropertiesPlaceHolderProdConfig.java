package com.volvo.site.server.config.placeholder;

import com.volvo.platform.spring.conf.ProfileProduction;
import org.springframework.context.annotation.Configuration;

import static com.volvo.site.server.config.constants.ConfigFilePaths.APP_PROD_CONFIG_PATH;

@Configuration
@ProfileProduction
public class PropertiesPlaceHolderProdConfig extends AbstractPropertiesPlaceHolderConfig {
    @Override
    protected String getPropertiesFilePath() {
        return APP_PROD_CONFIG_PATH;
    }
}
