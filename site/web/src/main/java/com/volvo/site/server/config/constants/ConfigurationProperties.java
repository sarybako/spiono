package com.volvo.site.server.config.constants;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ConfigurationProperties {

	public static String LOAD_MODE_PROPERTY = "spiono.configuration.loadmode";
}
