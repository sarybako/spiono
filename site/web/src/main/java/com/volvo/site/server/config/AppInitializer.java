package com.volvo.site.server.config;

import com.volvo.platform.spring.conf.AppInitializerUtils;
import com.volvo.platform.spring.conf.ProfileDev;
import org.apache.commons.lang3.CharEncoding;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

import static com.volvo.site.server.config.constants.ConfigurationProperties.LOAD_MODE_PROPERTY;

/**
 * Entry point of whole web-app.
 * Configuring here filters, Spring MVC, Hibernate
 */
@SuppressWarnings("unused")
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {
                CommonConfig.class, AsyncConfig.class, I18nConfig.class, SecurityConfig.class, RootEmailConfig.class
        };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { MvcConfig.class };
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter utf8filter = new CharacterEncodingFilter();
        utf8filter.setEncoding(CharEncoding.UTF_8);
        utf8filter.setForceEncoding(true);
        return new Filter[] {new DelegatingFilterProxy("filterChainProxy"), utf8filter};
    }

    @Override
    protected WebApplicationContext createRootApplicationContext() {
        AnnotationConfigWebApplicationContext context = (AnnotationConfigWebApplicationContext)super.createRootApplicationContext();

        ConfigurableEnvironment environment = context.getEnvironment();
        String defaultProfile = ProfileDev.name;
        environment.setActiveProfiles(AppInitializerUtils.loadProfile(environment, LOAD_MODE_PROPERTY, defaultProfile));
        return context;
    }
}
