package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.ClientInstaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static com.volvo.site.server.util.SpringDataUtil.ONE_ELEM_PAGEABLE;
import static com.volvo.site.server.util.SpringDataUtil.firstOrNull;

@Repository
@Transactional(readOnly = true)
class ClientInstallerRepositoryImpl implements ClientInstallerRepository {

    @Autowired
    ClientInstallerJpaRepository repository;

    @Override
    public ClientInstaller findLastActive() {
        return firstOrNull(repository.findLastActiveInstallers(ONE_ELEM_PAGEABLE));
    }
}
