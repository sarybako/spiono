package com.volvo.site.server.service;


import com.volvo.site.server.dto.StatisticsLogDTO;

import java.util.Date;
import java.util.Map;

public interface StatisticsService {

	void logAgentData(StatisticsLogDTO spyLog);

	Map<String, Integer> listStatisticForAgent(Long agentId, Date startDate, Date endDate);
}
