package com.volvo.site.server.service.social.impl;

import com.volvo.site.server.config.RootPropertiesPlaceHolderConfig;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.model.entity.FacebookUser;
import com.volvo.site.server.model.entity.Syslog;
import com.volvo.site.server.model.entity.TwitterUser;
import com.volvo.site.server.model.entity.VKUser;
import com.volvo.site.server.repository.SyslogRepository;
import com.volvo.site.server.repository.TwitterUserJpaRepository;
import com.volvo.site.server.service.social.TwitterService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.volvo.site.server.util.ControllersPaths.IndexController.LOGIN_TWITTER_CALLBACK;
import static com.volvo.site.server.util.ControllersPaths.IndexController.TWITTER_CALLBACK;

@Service
@Transactional
@Import({RootPropertiesPlaceHolderConfig.class})
public class TwitterServiceImpl implements TwitterService{

    @Value(AppProperties.TWAuthorizationConsumerKey)
    private String twConsumerKey;

    @Value(AppProperties.TWAuthorizationConsumerSecret)
    private String twConsumerSecret;

    @Autowired
    private SyslogRepository syslogRepository;

    @Autowired
    private TwitterUserJpaRepository twitterUserJpaRepository;

    @Override
    public void redirectToAuthorizationPage(HttpServletRequest request, HttpServletResponse response) {
        Twitter twitter = new TwitterFactory().getInstance();
        request.getSession().setAttribute("twitter", twitter);
        try {
            twitter.setOAuthConsumer(twConsumerKey, twConsumerSecret);
            RequestToken requestToken = twitter.getOAuthRequestToken("http://spiono.com:80" + LOGIN_TWITTER_CALLBACK);
            request.getSession().setAttribute("requestToken", requestToken);
            response.sendRedirect(requestToken.getAuthenticationURL());
        } catch (Exception e) {
            syslogRepository.save(Syslog.of("Exception during redirection to twitter auth page: " + e.getMessage()));
        }
    }

    @Override
    public TwitterUser getAuthorizedUser(Twitter twitter, RequestToken requestToken, String verifier) {
        try {
            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

            TwitterUser twitterUser = twitterUserJpaRepository.findByTwitterUserId(accessToken.getUserId());
            if (twitterUser == null){
                twitterUser = TwitterUser.of(accessToken.getUserId(), 0, accessToken.getToken());
            } else {
                TwitterUser.update(twitterUser, 0, accessToken.getToken());
            }
            twitterUser.setUserName(accessToken.getScreenName());

            return twitterUserJpaRepository.save(twitterUser);
        } catch (TwitterException e) {
            syslogRepository.save(Syslog.of("Exception getAuthorizedUser() for Twitter: " + e.getMessage()));
            return null;
        }
    }


}
