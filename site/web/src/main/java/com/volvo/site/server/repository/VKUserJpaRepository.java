package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.model.entity.VKUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface VKUserJpaRepository extends JpaRepositoryWithLongId<VKUser> {

    VKUser findByVkUserId(Long id);

    VKUser findBySpionoUser(User user);
}
