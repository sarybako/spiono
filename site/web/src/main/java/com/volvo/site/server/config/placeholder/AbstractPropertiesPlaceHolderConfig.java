package com.volvo.site.server.config.placeholder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

public abstract class AbstractPropertiesPlaceHolderConfig {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyConfigurer() {
        PropertySourcesPlaceholderConfigurer bean = new PropertySourcesPlaceholderConfigurer();
        bean.setLocation(new ClassPathResource( getPropertiesFilePath()));
        return bean;
    }

    protected abstract String getPropertiesFilePath();
}
