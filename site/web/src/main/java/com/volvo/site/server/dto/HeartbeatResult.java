package com.volvo.site.server.dto;

public enum HeartbeatResult {

    OK, PAUSE, UNINSTALL
}
