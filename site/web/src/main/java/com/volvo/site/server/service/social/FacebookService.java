package com.volvo.site.server.service.social;

import com.volvo.site.server.model.entity.FacebookUser;

public interface FacebookService {

    String getCredentials(String code);

    FacebookUser getAuthorizedUser(String credentials);
}
