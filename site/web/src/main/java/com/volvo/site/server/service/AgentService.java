package com.volvo.site.server.service;

import com.volvo.site.server.dto.HeartbeatResult;
import com.volvo.site.server.model.entity.Agent;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AgentService {

    Agent deleteAgentAndCreateNewOne(long userId, long agentId);

    void deleteAgent(long agentId);

    Agent createAgent(long userId);

    Agent createAgent(long userId, String name);

    byte[] downloadAgent(long agentId, String clientInstallerKey);

    boolean isAgentRegistered(long agentId);

    Agent registerAgent(String clientInstallerKey);

    void setAgentName(long agentId, String name);

    HeartbeatResult makeHeartBeat(String certificate);

    Page<Agent> getAgents(long userId, String searchingString, List<Agent.AgentStatus> statusFilter,
                          int pageNumber, int pageSize);

    List<Agent> getAgents(long userId, List<Agent.AgentStatus> statusFilter);

    void setAgentOnPause(long agentId, boolean onPause);
}
