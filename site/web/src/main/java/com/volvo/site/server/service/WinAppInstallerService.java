package com.volvo.site.server.service;

import com.volvo.site.server.dto.UpdateAgentResult;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.springframework.data.domain.Page;


public interface WinAppInstallerService {

    ClientInstaller addClientInstaller(byte[] data);

    AgentInstaller addAgentInstaller(long clientInstallerId, byte[] data, boolean isActive) throws EntityNotFoundException;

    void clearAll();

    void activateAgentInstaller(long agentInstallerId) throws EntityNotFoundException;

    void inactivateAgentInstaller(long agentInstallerId) throws ServiceException;

    void activateClientInstaller(long clientInstallerId) throws ServiceException;

    void inactivateClientInstaller(long clientInstallerId) throws ServiceException;

    ClientInstaller findLastActiveClientInstaller() throws EntityNotFoundException;

    AgentInstaller findLastActiveAgentInstaller(long clientInstallerId) throws EntityNotFoundException;

    void setClientInstallerDownloaded(long clientInstallerId) throws ServiceException;

    Page<ClientInstaller> findLastActiveClientInstallers(int pageNumber, int pageSize);

    Page<AgentInstaller> findLastActiveAgentInstallers(long clientInstallerId, int pageNumber, int pageSize);

    UpdateAgentResult updateAgentLib(String certificate);
}
