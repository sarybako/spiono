package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.AgentDTO;
import com.volvo.site.server.model.entity.Agent;
import lombok.AllArgsConstructor;

import javax.annotation.Nullable;


@AllArgsConstructor
public class AgentToAgentsResultItemDTOConverter implements Function<Agent, AgentDTO> {

    private final AutoBeanFactory beanFactory;

    private final AgentToAgentStatusDTOConverter statusDTOConverter = new AgentToAgentStatusDTOConverter();

    @Override
    public AgentDTO apply(@Nullable Agent agent) {
        AgentDTO result = beanFactory.create(AgentDTO.class).as();
        result.setAgentId(agent.getId());
        result.setName(agent.getName());
        result.setStatus(statusDTOConverter.apply(agent));
        result.setClientKey(agent.getClientKey());
        return result;
    }
}
