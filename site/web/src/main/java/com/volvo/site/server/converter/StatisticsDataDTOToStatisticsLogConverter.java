package com.volvo.site.server.converter;

import com.google.common.base.*;
import com.volvo.site.server.dto.*;
import com.volvo.site.server.model.entity.*;
import lombok.*;

import javax.annotation.*;
import java.util.*;

@AllArgsConstructor
public class StatisticsDataDTOToStatisticsLogConverter implements Function<StatisticsDataDTO, StatisticsLog> {

	private final long agentId;

	@Override
	public StatisticsLog apply(@Nullable StatisticsDataDTO dto) {
		StatisticsLog statisticsLog = new StatisticsLog();
		statisticsLog.setAgentId(agentId);
		statisticsLog.setStartDate(new Date(dto.getStartDate()));
		statisticsLog.setEndDate(new Date(dto.getEndDate()));
		statisticsLog.setProcessName(dto.getProcessName());
		statisticsLog.setProcessDescription(dto.getProcessDescription());
		statisticsLog.setTimeOfUse(dto.getEndDate() - dto.getStartDate());
		return statisticsLog;
	}
}
