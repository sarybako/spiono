package com.volvo.site.server.converter;


import com.google.common.base.Function;
import com.volvo.site.server.dto.KeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;
import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
public class KeyLogDataDTOToKeyLogConverter implements Function<KeyLogDataDTO, KeyLog> {

    private final long agentId;

    @Override
    public KeyLog apply(KeyLogDataDTO dto) {
        KeyLog keyLog = new KeyLog();
        keyLog.setAgentId(agentId);
        keyLog.setText(dto.getText());
        keyLog.setStartDate(new Date(dto.getStartDate()));
        keyLog.setEndDate(new Date(dto.getEndDate()));
        keyLog.setHwnd(dto.getHWND());
        keyLog.setWindowText(dto.getWindowText());
        keyLog.setProcessName(dto.getProcessName());
        return keyLog;
    }
}
