package com.volvo.site.server.converter;

import com.google.common.base.Function;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.KeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;
import lombok.AllArgsConstructor;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class KeyLogListToKeyLogDataDTOListConverter implements Function<List<KeyLog>, List<KeyLogDataDTO>> {

	private final AutoBeanFactory beanFactory;

	@Nullable
	@Override
	public List<KeyLogDataDTO> apply(@Nullable List<KeyLog> keyLogs) {
		KeyLogToKeyLogDataDTOConverter keyLogConverter = new KeyLogToKeyLogDataDTOConverter(beanFactory);
		List<KeyLogDataDTO> result = new ArrayList<KeyLogDataDTO>();
		for(KeyLog keyLog : keyLogs) {
			result.add(keyLogConverter.apply(keyLog));
		}

		return result;
	}
}
