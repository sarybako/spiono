package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.ForgotPassword;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
@Transactional(readOnly = true)
public interface ForgotPasswordJpaRepository extends JpaRepositoryWithLongId<ForgotPassword> {

    ForgotPassword findByUniqueKey(String uniqueKey);

    ForgotPassword findByUserIdAndStatus(Long userId, ForgotPassword.ForgotPasswordStatus status);

    @Modifying
    @Query("update ForgotPassword f set f.status='TIMED_OUT' where (f.status = 'CREATED') and (f.crDate < :deadLine)")
    int markTooOldRequests(@Param("deadLine") Date deadLine);

}
