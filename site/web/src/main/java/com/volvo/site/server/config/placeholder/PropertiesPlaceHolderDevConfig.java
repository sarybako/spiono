package com.volvo.site.server.config.placeholder;

import com.volvo.platform.spring.conf.ProfileDev;
import org.springframework.context.annotation.Configuration;

import static com.volvo.site.server.config.constants.ConfigFilePaths.APP_DEV_CONFIG_PATH;

@Configuration
@ProfileDev
public class PropertiesPlaceHolderDevConfig extends AbstractPropertiesPlaceHolderConfig {
    @Override
    protected String getPropertiesFilePath() {
        return APP_DEV_CONFIG_PATH;
    }
}
