package com.volvo.site.server.config;


import static org.springframework.util.ClassUtils.getPackageName;

public final class PackageInfo {

    public static final String PACKAGE_NAME = getPackageName(PackageInfo.class);
}
