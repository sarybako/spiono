package com.volvo.site.server.service.impl;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Async
@Service
@Transactional(readOnly = true)
@Slf4j
class LoginServiceAsyncIml extends LoginServiceImplBase  {

    @Modifying
    public void deleteRegistrationsByEmail(String email) {
        registrationRepository.deleteByEmail(email);
    }
}
