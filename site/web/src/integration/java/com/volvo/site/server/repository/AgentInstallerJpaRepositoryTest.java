package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.testing.InstallerJpaTestBase;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;

import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;

public class AgentInstallerJpaRepositoryTest extends InstallerJpaTestBase<AgentInstaller, AgentInstallerJpaRepository> {

    @Autowired
    @Getter
    AgentInstallerJpaRepository repository;

    @Autowired
    ClientInstallerJpaRepository clientRepository;

    private ClientInstaller clientInstaller;

    protected AgentInstallerJpaRepositoryTest() {
        super(AgentInstaller.class);
    }

    @BeforeClass
    public void beforeClass() {
        clientInstaller = clientRepository.save(ClientInstaller.of(random255Bytes));
    }

    @AfterClass
    public void afterClass() {
        clientRepository.delete(clientInstaller);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createWithoutClientInstallerTest() {
        repository.save(AgentInstaller.of(random255Bytes));
    }

    @Override
    protected AgentInstaller save(AgentInstaller installer) {
        installer.setClientInstaller(clientInstaller);
        return super.save(installer);
    }
}
