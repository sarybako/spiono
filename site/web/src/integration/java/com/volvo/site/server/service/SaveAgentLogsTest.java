package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import com.volvo.site.server.testutil.components.KeyLogTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertTrue;

@Test(groups = "SaveAgentLogsTestConflict")
public class SaveAgentLogsTest extends AbstractFullSpringTest{

    @Autowired
    KeyLogJpaRepository keyLogJpaRepository;

    @Autowired
    KeyLogTestUtils keyLogTestUtils;

    @Autowired
    AgentTestUtils agentTestUtils;

	private static final String SEARCHING_STRING = "%%";
    private static final boolean SEARCH_BY_DATE = false;
    private static final short AMOUNT_OF_LOGS = 10;

    @Test
    public void logAgentKeyLogTest(){
        Long agentId = agentTestUtils.createNormalAgentWithCertificate().agent.getId();

        keyLogJpaRepository.save(keyLogTestUtils.generateKeyLogsIterable(agentId, AMOUNT_OF_LOGS));

        List<KeyLog> keyLogList = keyLogJpaRepository.findByAgentId(agentId, SEARCHING_STRING, SEARCH_BY_DATE,
                new Date(), new Date());
        assertTrue(keyLogList.size() >= AMOUNT_OF_LOGS);
    }
}
