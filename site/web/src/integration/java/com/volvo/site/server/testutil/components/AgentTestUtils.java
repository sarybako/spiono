package com.volvo.site.server.testutil.components;

import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentInstallerJpaRepository;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.AgentService;
import com.volvo.site.server.service.WinAppInstallerService;
import com.volvo.site.server.testutil.RepositoryTestUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;

@Component
public class AgentTestUtils {

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    WinAppInstallerService winAppInstallerService;

    @Autowired
    AgentInstallerJpaRepository agentInstallerJpaRepository;

    @Autowired
    ClientInstallerJpaRepository clientInstallerJpaRepository;

    @Autowired
    AgentService agentService;

    public TestAgentStuff createRegisteredAgent() {
        TestAgentStuff stuff = createNormalAgentWithCertificate();
        stuff.agent.setStatus(Agent.AgentStatus.AGENT_REGISTERED);
        stuff.agent = agentJpaRepository.save(stuff.agent);
        return stuff;
    }

    public TestAgentStuff createNormalAgentWithCertificate() {
        TestAgentStuff stuff = createNormalAgent();
        stuff.agent.generateCertificate();
        return stuff;
    }

    public TestAgentStuff createNormalAgent() {
        return createNormalAgent(null);
    }

    public TestAgentStuff createNormalAgent(String name) {
        User user = userJpaRepository.save(createRandomActiveUser());
        return createNormalAgent(user, name);
    }

    public TestAgentStuff createNormalAgent(User user, String name) {
        return createNormalAgent(user, name, null);
    }

    public TestAgentStuff createNormalAgent(User user, String name, Agent.AgentStatus status) {
        ClientInstaller clientInstaller = winAppInstallerService.addClientInstaller(RepositoryTestUtils.random255Bytes);
        AgentInstaller agentInstaller = winAppInstallerService.addAgentInstaller(clientInstaller.getId(), random255Bytes, true);
        winAppInstallerService.activateClientInstaller(clientInstaller.getId());
        Agent agent = agentService.createAgent(user.getId(), name);
        if (status != null) {
            agent.setStatus(status);
            agent = agentJpaRepository.save(agent);
        }
        return new TestAgentStuff(agent, user, clientInstaller, agentInstaller);
    }

    public void deleteTestAgentStuffFromDb(TestAgentStuff ... stuffs) {
        Set<AgentInstaller> agentInstallersToDelete = newHashSet();
        Set<ClientInstaller> clientInstallersToDelete = newHashSet();
        Set<User> usersToDelete = newHashSet();

        for (AgentTestUtils.TestAgentStuff stuff: stuffs) {
            agentJpaRepository.delete(stuff.agent.getId());

            agentInstallersToDelete.add(stuff.agentInstaller);
            clientInstallersToDelete.add(stuff.clientInstaller);
            usersToDelete.add(stuff.user);
        }

        agentInstallerJpaRepository.delete(agentInstallersToDelete);
        clientInstallerJpaRepository.delete(clientInstallersToDelete);
        userJpaRepository.delete(usersToDelete);
    }

    @AllArgsConstructor
    public static class TestAgentStuff {
        public Agent agent;
        public User user;
        public ClientInstaller clientInstaller;
        public AgentInstaller agentInstaller;
    }
}
