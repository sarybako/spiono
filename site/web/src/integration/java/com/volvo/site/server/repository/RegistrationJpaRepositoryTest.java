package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static org.testng.Assert.*;

public class RegistrationJpaRepositoryTest extends AbstractFullSpringTest {

    @Autowired
    RegistrationJpaRepository repository;

    @Test
    public void findByUniqueKeyTest() {
        assertNull(repository.findByUniqueKey("blah blah"));

        Registration registration = repository.save(Registration.of("gcc@bk.ru", randomPwd()));
        assertNotNull(repository.findByUniqueKey(registration.getUniqueKey()));
    }

    @Test
    public void deleteByEmailTestWithNotExistingEmail() {
        assertEquals(repository.deleteByEmail(randomEmail()), 0);
    }

    @Test
    public void deleteByEmailTest() {
        // Given
        final String email = randomEmail();
        Long id1 = repository.save(Registration.of(email, randomPwd())).getId();
        Long id2 = repository.save(Registration.of(email, randomPwd())).getId();

        // When
        assertEquals(repository.deleteByEmail(email), 2);

        // Then
        assertFalse(repository.exists(id1));
        assertFalse(repository.exists(id2));
    }

    @Test
    public void findByEmailAndStatusTest() {
        // Given
        final String email = randomEmail();
        repository.save(Registration.of(email, randomPwd()));

        assertNotNull(repository.findByEmailAndStatus(email, Registration.RegistrationStatus.PENDING));
        assertNull(repository.findByEmailAndStatus("not_found" + randomEmail(), Registration.RegistrationStatus.PENDING));
    }

    @Test
    public void countPendingByEmailTest() {
        // Given
        final String email1 = randomEmail();
        repository.save(Registration.of(email1, randomPwd())).getId();
        repository.save(Registration.of(email1, randomPwd())).getId();

        // When
        final String email2 = randomEmail();
        repository.save(Registration.of(email2, randomPwd())).getId();

        // Then
        assertEquals(repository.countPendingByEmail(email1), 2L);
        assertEquals(repository.countPendingByEmail(email2),1L);
        assertEquals(repository.countPendingByEmail("not_existing" + email1), 0L);
    }

    @Test
    public void findByEmailTest() {
        final String email = randomEmail();
        repository.save(Registration.of(email, randomPwd()));

        assertNotNull(repository.findByEmail(email));
        assertNull(repository.findByEmail("not_found" + randomEmail()));
    }
}
