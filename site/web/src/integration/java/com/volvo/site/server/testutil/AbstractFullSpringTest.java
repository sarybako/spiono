package com.volvo.site.server.testutil;

import com.volvo.platform.spring.email.conf.TestMailSenderConfig;
import com.volvo.site.mailing.config.EmailingConfig;
import com.volvo.site.server.config.CommonConfig;
import com.volvo.site.server.config.SecurityConfig;
import com.volvo.site.server.testutil.spring.PropertiesPlaceHolderTestConfig;
import com.volvo.site.server.testutil.spring.TestDBConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;

@ContextConfiguration(classes ={
        PropertiesPlaceHolderTestConfig.class,
        CommonConfig.class,
        SecurityConfig.class,
        TestDBConfig.class,
        EmailingConfig.class,
        TestMailSenderConfig.class}, loader = AnnotationConfigContextLoader.class)
public abstract class AbstractFullSpringTest extends AbstractTransactionalTestNGSpringContextTests {
}
