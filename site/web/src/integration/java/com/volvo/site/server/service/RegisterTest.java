package com.volvo.site.server.service;

import com.volvo.platform.spring.email.service.SimpleMailSendEvent;
import com.volvo.site.mailing.service.EmailTemplateService;
import com.volvo.site.server.config.constants.AppProperties;
import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.RegistrationJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.DuplicateEntityException;
import com.volvo.site.server.testutil.spring.ApplicationContextListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.Locale;

import static com.volvo.platform.java.testing.TestUtils.*;
import static com.volvo.site.server.testutil.spring.ApplicationContextListener.createContextListener;
import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class RegisterTest extends AbstractLoginService {

    @Autowired
    UserJpaRepository userRepository;

    @Autowired
    RegistrationJpaRepository registrationRepository;

    @Autowired
    EmailTemplateService emailTemplateService;

    @Autowired
    ApplicationContext context;

    @Value(AppProperties.RootUsername)
    private String rootUsername;

    @Value("${mail.username}")
    private String fromEmailUserName;

    @Test(expectedExceptions = NullPointerException.class)
    public void emailIsNullTest() {
        loginService.register(null, randomPwd(), Locale.ENGLISH);
    }

    @Test(dataProvider = "invalidEmails", expectedExceptions = ConstraintViolationException.class)
    public void emailIncorrectTest(String email) {
        loginService.register(email, randomPwd(), Locale.ENGLISH);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void emailTooLongTest() {
        loginService.register(randomEmail(ModelConstants.emailMaxLength + 1), randomPwd(), Locale.ENGLISH);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void passwordIsNullTest() {
        loginService.register(randomEmail(), null, Locale.ENGLISH);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void passwordTooLongTest() {
        loginService.register(randomEmail(), randomAlphabetic(9000), Locale.ENGLISH);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void passwordTooSmallTest() {
        loginService.register(randomEmail(), randomAlphabetic(3), Locale.ENGLISH);
    }

    @Test(expectedExceptions = DuplicateEntityException.class)
    public void tryRegisterWithExistingEmail() {
        final String email = randomEmail();
        userRepository.save(User.of(email, randomPwdHash()));

        loginService.register(email, randomPwd(), Locale.ENGLISH);
    }

    @Test(expectedExceptions = DuplicateEntityException.class)
    public void tryRegisterWithSameEmailInDifferentCase() {
        final String email = randomEmail();
        userRepository.save(User.of(email.toLowerCase(), randomPwdHash()));

        loginService.register(email.toUpperCase(), randomPwd(), Locale.ENGLISH);
    }

    @Test(expectedExceptions = DuplicateEntityException.class)
    public void tryRegisterToRootEmail() {
        loginService.register(rootUsername, randomPwd(), Locale.ENGLISH);
    }

    @Test
    public void sendMailTest() {
        ApplicationContextListener<SimpleMailSendEvent> listener = createContextListener(context);

        String email = randomEmail();
        Registration registration = loginService.register(email, randomPwd(), Locale.ENGLISH);

        SimpleMailMessage expectedEmail =
                emailTemplateService.registrationEmail(email, registration.getUniqueKey(), Locale.ENGLISH);
        expectedEmail.setFrom(fromEmailUserName);
        SimpleMailSendEvent expectedEvent = new SimpleMailSendEvent(this, expectedEmail);
        SimpleMailSendEvent event = listener.findEvent(expectedEvent);
        assertNotNull(event);

        listener.stopListening();
    }

    @Test
    public void doubleRegistrationTest() {
        String email = randomEmail();

        loginService.register(email, randomPwd(), Locale.ENGLISH);
        String newPwd = randomPwd();
        loginService.register(email, newPwd, Locale.ENGLISH).getCrDate();

        Registration registration = registrationRepository.findByEmail(email);
        assertEquals(registration.getPassword(), newPwd);
    }

    @DataProvider(name="invalidEmails")
    public Object[][] invalidEmails() {
        return invalidEmailsDataProvider();
    }
}
