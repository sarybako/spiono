package com.volvo.site.server.service;


import com.volvo.site.server.service.exception.EntityNotFoundException;
import org.testng.annotations.Test;

@Test
public class FindLastActiveClientInstallerTest extends AbstractWinappServiceTest {
    @Test(expectedExceptions = EntityNotFoundException.class)
    public void findIfNoActiveInDb() {
        service.findLastActiveClientInstaller();
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void findLastActive() {
        newClientInstaller();
        service.findLastActiveClientInstaller();
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void findLastActive1() {
        newInactiveAgentInstaller(newClientInstaller().getId());
        service.findLastActiveClientInstaller();
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void findLastActive2() {
        newActiveAgentInstaller(newClientInstaller().getId());
        service.findLastActiveClientInstaller();
    }
}
