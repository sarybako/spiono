package com.volvo.site.server.service;


import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class InactivateClientInstallerTest extends AbstractWinappServiceTest {

    @Autowired
    ClientInstallerJpaRepository clientJpaRepository;

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void withNotExistingClientInstallerTest() {
        inactivate(0);
    }

    @Test
    public void inactivateAlreadyInactiveTest() {
        inactivate(newClientInstaller().getId());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void inactivateDownloadedTest() {
        long id = newActiveClientInstaller().getId();
        service.setClientInstallerDownloaded(id);
        inactivate(id);
    }

    @Test
    public void inactivateTest() {
        // Given
        long id = newActiveClientInstaller().getId();
        assertTrue(isActive(id));

        // When
        service.inactivateClientInstaller(id);

        // Than
        assertFalse(isActive(id));
    }

    @Test
    public void inactivateTwiceTest() {
        // Given
        long id = newActiveClientInstaller().getId();
        assertTrue(isActive(id));

        // When
        service.inactivateClientInstaller(id);
        service.inactivateClientInstaller(id);

        // Than
        assertFalse(isActive(id));
    }

    private void inactivate(long id) {
        service.inactivateClientInstaller(id);
    }

    private boolean isActive(long id) {
        return clientJpaRepository.findOne(id).isActive();
    }
}
