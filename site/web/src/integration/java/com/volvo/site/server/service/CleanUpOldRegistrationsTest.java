package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.Registration;
import com.volvo.site.server.repository.RegistrationJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwd;
import static org.joda.time.DateTime.now;
import static org.testng.Assert.assertEquals;

public class CleanUpOldRegistrationsTest extends AbstractLoginService {

    @Autowired
    RegistrationJpaRepository repository;

    Registration regNew;

    Registration regOld;

    @Test
    @Rollback(false)
    public void installRegistrationsTest() {
        regNew = repository.save(Registration.of(randomEmail(), randomPwd()));
        regOld = repository.save(Registration.of(randomEmail(), randomPwd()));
    }

    @Test(dependsOnMethods = "installRegistrationsTest")
    @Rollback(false)
    public void hackDateTest() {
        this.simpleJdbcTemplate.update("update registration set cr_date = ? where id=?",
                new Object[]{now().minusDays(3).toDate(), regOld.getId()});
    }

    @Rollback(false)
    @Test(dependsOnMethods = "hackDateTest")
    public void cleanUpOldRegistrationsTest() {
        loginService.cleanUpOldRegistrations();
    }

    @Rollback(false)
    @Test(dependsOnMethods = "cleanUpOldRegistrationsTest")
    public void timeoutStatusCheckTest() {
        assertEquals(repository.findByEmail(regNew.getEmail()).getStatus(), Registration.RegistrationStatus.PENDING);
        assertEquals(repository.findByEmail(regOld.getEmail()).getStatus(), Registration.RegistrationStatus.TIMED_OUT);
        assertEquals(repository.findByEmail(regOld.getEmail()).getPassword(), "timed_out_pwd_stub");
    }

    @AfterClass
    @Rollback(false)
    private void afterClass() {
        repository.delete(regNew);
        repository.delete(regOld);
    }
}
