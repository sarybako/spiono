package com.volvo.site.server.service;

import com.google.web.bindery.autobean.vm.AutoBeanFactorySource;
import com.volvo.site.gwt.usercabinet.shared.dto.BeanFactory;
import com.volvo.site.gwt.usercabinet.shared.dto.GetKeyLogDataDTO;
import com.volvo.site.server.model.entity.KeyLog;
import com.volvo.site.server.repository.KeyLogJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class KeyLogsSearchTest extends AbstractFullSpringTest {

	@Autowired
	KeyLogJpaRepository keyLogJpaRepository;

	@Autowired
	LogsService logsService;

    @PersistenceContext
    EntityManager entityManager;

	private final ThreadLocal<BeanFactory> beanFactoryHolder = new ThreadLocal<BeanFactory>();
	private HashSet<KeyLog> generatedLogs = new HashSet<KeyLog>();
	/*.withMillisOfSecond(0) since Resolution of KeyLog's startDate and endDate = DAY*/
	private final Date startDate = new DateTime().minusYears(5).withMillisOfSecond(0).toDate();
	private final Date endDate = new DateTime().minusYears(5).plusDays(2).withMillisOfSecond(0).toDate();
	private final static Random random = new Random();

	private final int AGENT_ID_FOR_FIRST_TEST  = 10;
	private final int AGENT_ID_FOR_SECOND_TEST = 11;
	private final int AGENT_ID_FOR_THIRD_TEST  = 12;
	private final int NUMBER_OF_GEN_LOGS_FOR_FIRST_TEST  = 30;
	private final int NUMBER_OF_GEN_LOGS_FOR_SECOND_TEST = 20;
	private final int NUMBER_OF_GEN_LOGS_FOR_THIRD_TEST  = 10;

    private final static int ONE_DATE_AGENT_ID = 557;
    private final static Date ONE_DAY_START_DATE = new DateTime(2013, 2, 25, 11, 15, 12).toDate();
    private final static Date ONE_DAY_END_DATE = new DateTime(2013, 2, 25, 11, 15, 20).toDate();

	private final static int PAGE_SIZE_FOR_TEST = 100;
	private final static int SEARCH_FROM_BEGIN = 0;


	@BeforeClass
	@Rollback(value = false)
	public void beforeClass(){
        // Clear lucene index
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        fullTextEntityManager.purgeAll(KeyLog.class);

		beanFactoryHolder.set(AutoBeanFactorySource.create(BeanFactory.class));

		for(int i = 0; i < NUMBER_OF_GEN_LOGS_FOR_FIRST_TEST; i++){
			generatedLogs.add(keyLogJpaRepository.saveAndFlush(generateKeyLog(AGENT_ID_FOR_FIRST_TEST, new Date(),
					new Date(), "null", "null", "null")));
		}
		for(int i = 0; i < NUMBER_OF_GEN_LOGS_FOR_SECOND_TEST; i++){
			generatedLogs.add(keyLogJpaRepository.saveAndFlush(generateKeyLog(AGENT_ID_FOR_SECOND_TEST, startDate,
					endDate, "null", "null", "null")));
		}
		for(int i = 0; i < NUMBER_OF_GEN_LOGS_FOR_THIRD_TEST; i++){
			generatedLogs.add(keyLogJpaRepository.saveAndFlush(generateKeyLog(AGENT_ID_FOR_THIRD_TEST, startDate,
					endDate, "windowText", "text", "process")));
		}

        generatedLogs.add(keyLogJpaRepository.saveAndFlush(generateKeyLog(ONE_DATE_AGENT_ID, ONE_DAY_START_DATE,
                ONE_DAY_END_DATE, "window", "text", "process")));
	}

    @Test
    public void findLogsForAllAgentsTest() {
        GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
        filter.setAgentId(-1L);
        filter.setPageNumber(SEARCH_FROM_BEGIN);
        filter.setPageSize(Integer.MAX_VALUE);
        List<KeyLog> result = logsService.search(filter);
        assertEquals(result.size(), generatedLogs.size());
    }

    @Test
    public void findLogsForAllAgentsWithDatesFilterTest() {
        GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
        filter.setAgentId(-1L);
        filter.setPageNumber(SEARCH_FROM_BEGIN);
        filter.setPageSize(Integer.MAX_VALUE);
        filter.setStartDate(startDate);
        filter.setEndDate(endDate);

        List<KeyLog> result = logsService.search(filter);
        assertEquals(NUMBER_OF_GEN_LOGS_FOR_SECOND_TEST + NUMBER_OF_GEN_LOGS_FOR_THIRD_TEST, result.size());
    }

	//TODO: add test for pagination!

	@Test
	public void findLogsByAgentIdTest(){
		GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
		filter.setAgentId(AGENT_ID_FOR_FIRST_TEST);
		filter.setPageNumber(SEARCH_FROM_BEGIN);
		filter.setPageSize(PAGE_SIZE_FOR_TEST);
		List<KeyLog> result = logsService.search(filter);
		assertEquals(NUMBER_OF_GEN_LOGS_FOR_FIRST_TEST, result.size());
		for(KeyLog keyLog : result){
			assertEquals(keyLog.getAgentId(), AGENT_ID_FOR_FIRST_TEST);
		}
	}

	@Test
	public void findLogsByAgentIdAndDatesTest(){
		GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
		filter.setPageNumber(SEARCH_FROM_BEGIN);
		filter.setPageSize(PAGE_SIZE_FOR_TEST);
		filter.setAgentId(AGENT_ID_FOR_SECOND_TEST);
		filter.setStartDate(startDate);
		filter.setEndDate(endDate);

		List<KeyLog> result = logsService.search(filter);
		assertEquals(NUMBER_OF_GEN_LOGS_FOR_SECOND_TEST, result.size());
		for(KeyLog keyLog : result){
			assertEquals(AGENT_ID_FOR_SECOND_TEST, keyLog.getAgentId());
			assertEquals(keyLog.getStartDate().getTime(), startDate.getTime());
			assertEquals(keyLog.getEndDate().getTime(), endDate.getTime());
		}
	}

	@Test
	public void findLogsByAgentIdAndDatesAndSearchingStringTest(){
		String[] searchingText = {"windowText", "text", "process"};
		GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
		System.out.println("filter = " + filter);
		filter.setPageNumber(SEARCH_FROM_BEGIN);
		filter.setPageSize(PAGE_SIZE_FOR_TEST);
		filter.setAgentId(AGENT_ID_FOR_THIRD_TEST);
		filter.setStartDate(startDate);
		filter.setEndDate(endDate);
		List<KeyLog> result;
		for (String aSearchingText : searchingText) {
			filter.setSearchingString(aSearchingText);
			result = logsService.search(filter);
			assertEquals(NUMBER_OF_GEN_LOGS_FOR_THIRD_TEST, result.size());
			for(KeyLog keyLog : result){
				assertEquals(AGENT_ID_FOR_THIRD_TEST, keyLog.getAgentId());
				assertEquals(keyLog.getStartDate().getTime(), startDate.getTime());
				assertEquals(keyLog.getEndDate().getTime(), endDate.getTime());
				assertTrue(aSearchingText.equals(keyLog.getWindowText()) ||
					aSearchingText.equals(keyLog.getText()) || aSearchingText.equals(keyLog.getProcessName()));
			}
		}
	}

    @Test
    public void findLogsForTheOneDayTest() {
        GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
	    filter.setPageNumber(SEARCH_FROM_BEGIN);
	    filter.setPageSize(PAGE_SIZE_FOR_TEST);
        filter.setAgentId(ONE_DATE_AGENT_ID);
        filter.setStartDate(ONE_DAY_START_DATE);
        filter.setEndDate(ONE_DAY_END_DATE);

        List<KeyLog> result = logsService.search(filter);
        assertEquals(1, result.size());
    }

    @Test
    public void findLogsForTheOneDayInverseTest() {
        Date newDate = new DateTime(ONE_DAY_START_DATE).plusDays(1).toDate();
        GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
        filter.setAgentId(ONE_DATE_AGENT_ID);
        filter.setStartDate(newDate);
        filter.setEndDate(newDate);

        List<KeyLog> result = logsService.search(filter);
        assertEquals(0, result.size());
    }

    @Test
    public void findLogsForTheOneDayInverseTest2() {
        Date newDate = new DateTime(ONE_DAY_START_DATE).minusDays(1).toDate();
        GetKeyLogDataDTO filter = beanFactoryHolder.get().create(GetKeyLogDataDTO.class).as();
        filter.setAgentId(ONE_DATE_AGENT_ID);
        filter.setStartDate(newDate);
        filter.setEndDate(newDate);

        List<KeyLog> result = logsService.search(filter);
        assertEquals(0, result.size());
    }

	@AfterClass
	public void afterClass(){
		keyLogJpaRepository.delete(generatedLogs);
	}

	private static KeyLog generateKeyLog(long agentId, Date startDate, Date endDate, String windowText,
								  String text, String processName){
		KeyLog keyLog = new KeyLog();
		keyLog.setAgentId(agentId);
		long randomLong = random.nextLong();
		keyLog.setHwnd(randomLong);
		keyLog.setStartDate(startDate);
		keyLog.setEndDate(endDate);
		keyLog.setWindowText(windowText);
		keyLog.setProcessName(processName);
		keyLog.setText(text);
		return keyLog;
	}
}
