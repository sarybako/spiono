package com.volvo.site.server.service;


import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FindLastActiveAgentInstallersTest extends AbstractWinappServiceTest {
    @Autowired
    ClientInstallerJpaRepository clientJpaRepository;

    @Test
    public void emptyListTest() {
        Page<AgentInstaller> result = service.findLastActiveAgentInstallers(0L, 100, 10);
        assertNotNull(result);
        assertFalse(result.hasContent());
        assertFalse(result.iterator().hasNext());
        assertTrue(result.getContent().isEmpty());
        assertEquals(result.getNumberOfElements(), 0);
    }

    @Test
    public void orderingTest() {
        long clientInstallerId = newClientInstaller().getId();
        final int count = 20;
        for (int j = 0; j < count; ++j) {
           newActiveAgentInstaller(clientInstallerId);
        }

        Page<AgentInstaller> result = service.findLastActiveAgentInstallers(clientInstallerId, 0, 10);
        assertEquals(result.getNumberOfElements(), 10);
        assertTrue(result.getTotalElements() >= 20);
        assertTrue(result.getTotalPages() >= 2);

        long id = -1;
        for (AgentInstaller installer : result.getContent()) {
            if (id != -1) {
                assertTrue(installer.getId() < id);
            }
            id = installer.getId();
        }
    }
}
