package com.volvo.site.server.service;


import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FindLastActiveAgentInstallerTest extends AbstractWinappServiceTest {

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void findIfClientInstallerNotExists() {
         service.findLastActiveAgentInstaller(0L);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void findIfClientIsInactive() {
        service.findLastActiveAgentInstaller(newClientInstaller().getId());
    }

    @Test
    public void findLastActiveTest() {
        long clientInstallerId = newClientInstaller().getId();
        newActiveAgentInstaller(clientInstallerId);
        newInactiveAgentInstaller(clientInstallerId);
        Long agentId = newActiveAgentInstaller(clientInstallerId).getId();
        newInactiveAgentInstaller(clientInstallerId);

        service.activateClientInstaller(clientInstallerId);
        assertEquals(service.findLastActiveAgentInstaller(clientInstallerId).getId(), agentId);
    }
}
