package com.volvo.site.server.service;

import com.volvo.site.server.repository.AgentInstallerJpaRepository;
import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ActivateAgentInstallerTest extends AbstractWinappServiceTest {
    @Autowired
    AgentInstallerJpaRepository agentRepository;

    @Autowired
    ClientInstallerJpaRepository clientRepository;

    private long clientInstallerId;

    private long agentInstallerId;

    @Test
    @Rollback(false)
    public void activateAgentInstallerTest1() {
        clientInstallerId = newClientInstaller().getId();
        agentInstallerId = newInactiveAgentInstaller(clientInstallerId).getId();

        assertFalse(isAgentInstallerActive());
        service.activateAgentInstaller(agentInstallerId);
    }

    @Test(dependsOnMethods = "activateAgentInstallerTest1")
    @Rollback(false)
    public void activateAgentInstallerTest2() {
        assertTrue(isAgentInstallerActive());
    }

    @AfterClass
    @Rollback(false)
    public void afterClass() {
        clientRepository.delete(clientInstallerId);
    }

    private boolean isAgentInstallerActive() {
        return agentRepository.findOne(agentInstallerId).isActive();
    }
}
