package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.AgentJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.model.entity.Agent.AgentStatus.AGENT_CREATED;
import static com.volvo.site.server.model.entity.Agent.AgentStatus.AGENT_REGISTERED;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class FindAgentsForUserTest extends AbstractFullSpringTest {

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    AgentService agentService;

    private AgentTestUtils.TestAgentStuff registeredAgent;
    private AgentTestUtils.TestAgentStuff createdAgent;
    private AgentTestUtils.TestAgentStuff deletedAgent;

    @BeforeClass
    @Rollback(false)
    public void setup() {
        registeredAgent = agentTestUtils.createRegisteredAgent();
        User user = registeredAgent.user;

        createdAgent = agentTestUtils.createNormalAgent(user, "new", AGENT_CREATED);
        deletedAgent = agentTestUtils.createNormalAgent(user, "deleted", Agent.AgentStatus.AGENT_DELETED);
    }

    @Test
    public void doPositiveTest() {
        User user = registeredAgent.user;
        List<Agent> result = agentService.getAgents(user.getId(), newArrayList(
                AGENT_CREATED, AGENT_REGISTERED));

        assertEquals(result.size(), 2);

        assertEquals(result.get(0).getId(), createdAgent.agent.getId());
        assertEquals(result.get(1).getId(), registeredAgent.agent.getId());

        assertEquals(result.get(0).getName(), "new");

        assertEquals(Long.valueOf(result.get(0).getUserId()), user.getId());
        assertEquals(Long.valueOf(result.get(1).getUserId()), user.getId());
    }

    @Test
    public void doTestWithWrongAgentId() {
        List<Agent> result = agentService.getAgents(-1L, newArrayList(
                AGENT_CREATED, AGENT_REGISTERED));
        assertTrue(result.isEmpty());
    }

    @Test
    public void doTestWithEmptyFilterFlagSet() {
        List<Agent> result = agentService.getAgents(registeredAgent.user.getId(), new ArrayList<Agent.AgentStatus>());
        assertEquals(result.size(), 3);
    }

    @Test
    public void doTestWithNullFilterFlagSet() {
        List<Agent> result = agentService.getAgents(registeredAgent.user.getId(), null);
        assertEquals(result.size(), 3);
    }

    @AfterClass
    @Rollback(false)
    public void tearDown() {
        agentTestUtils.deleteTestAgentStuffFromDb(registeredAgent, createdAgent, deletedAgent);
    }
}
