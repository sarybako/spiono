package com.volvo.site.server.service;

import com.volvo.site.server.dto.KeyLogDTO;
import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.components.AgentTestUtils;
import com.volvo.site.server.util.SpyKeyLogDataDTOMocker;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


@Test(groups = "SingleClientInstaller")
public class LogsServiceTest extends AbstractFullSpringTest {

    @Autowired
    AgentTestUtils agentTestUtils;

    @Autowired
    LogsService logsService;

    @Test
    public void logAgentDataTest() {
        Agent agent = agentTestUtils.createRegisteredAgent().agent;

        int count = logsService.writeLogs(new SpyKeyLogDataDTOMocker().spyLogDTO(agent.getCertificate()));

        assertEquals(count, 1);
    }

    @Test
    public void shouldNotLogAgentDataIfTestIsNotPrintable() {
        Agent agent = agentTestUtils.createRegisteredAgent().agent;
        KeyLogDTO dto = new SpyKeyLogDataDTOMocker().spyLogDTO(agent.getCertificate());
        dto.getKeyLogs()[0].setText("\r\n");

        int count = logsService.writeLogs(dto);

        assertEquals(count, 0);
    }

}
