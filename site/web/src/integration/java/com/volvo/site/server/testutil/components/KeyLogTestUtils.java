package com.volvo.site.server.testutil.components;

import com.volvo.platform.java.ArrayIterable;
import com.volvo.site.server.model.entity.KeyLog;
import de.svenjacobs.loremipsum.LoremIpsum;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
@Transactional
public class KeyLogTestUtils {
    private final LoremIpsum loremIpsum = new LoremIpsum();
    private static final short NUMBER_OF_WORDS_FROM_IPSUM = 10;

    public Iterable<KeyLog> generateKeyLogsIterable(long agentId, int size) {
        return new ArrayIterable<KeyLog>(generateKeyLogsArray(agentId, size));
    }

    public KeyLog[] generateKeyLogsArray(long agentId, int size) {
        KeyLog[] result = new KeyLog[size];
        for (int j = 0; j < size; ++j) {
            result[j] = generateKeyLog(agentId);
        }
        return result;
    }

    public KeyLog generateKeyLog(long agentId){
        KeyLog keyLog = new KeyLog();
        keyLog.setAgentId(agentId);
        long randomLong = 100L;
        keyLog.setHwnd(randomLong);
        keyLog.setStartDate(new Date());
        keyLog.setEndDate(new Date());
        keyLog.setWindowText(loremIpsum.getWords(NUMBER_OF_WORDS_FROM_IPSUM));
        keyLog.setProcessName(loremIpsum.getWords(NUMBER_OF_WORDS_FROM_IPSUM));
        keyLog.setText(loremIpsum.getWords(NUMBER_OF_WORDS_FROM_IPSUM));
        return keyLog;
    }
}
