package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

public class ActivateClientInstallerTest  extends AbstractWinappServiceTest {

    @Autowired
    ClientInstallerJpaRepository clientJpaRepository;

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void activateNotExistingTest() {
        service.activateClientInstaller(0L);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void tryActivateClientInstallerTest1() {
        ClientInstaller clientInstaller = newClientInstaller();
        service.activateClientInstaller(clientInstaller.getId());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void tryActivateClientInstallerTest2() {
        ClientInstaller clientInstaller = newClientInstaller();
        newInactiveAgentInstaller(clientInstaller.getId());
        service.activateClientInstaller(clientInstaller.getId());
    }

    @Test
    public void tryActivateClientInstallerTest3() {
        ClientInstaller clientInstaller = newClientInstaller();
        newActiveAgentInstaller(clientInstaller.getId());
        service.activateClientInstaller(clientInstaller.getId());
    }

    @Test
    public void tryActivateClientInstallerTest4() {
        ClientInstaller clientInstaller = newClientInstaller();
        long id = newInactiveAgentInstaller(clientInstaller.getId()).getId();
        service.activateAgentInstaller(id);
        service.activateClientInstaller(clientInstaller.getId());
    }
}
