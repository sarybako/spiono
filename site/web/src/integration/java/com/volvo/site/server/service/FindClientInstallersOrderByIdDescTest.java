package com.volvo.site.server.service;

import com.volvo.site.server.model.entity.ClientInstaller;
import org.springframework.data.domain.Page;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.testng.Assert.*;


public class FindClientInstallersOrderByIdDescTest extends AbstractWinappServiceTest {

    private final List<ClientInstaller> addedInstallers = newArrayList();

    @Test
    public void emptyListTest() {
        Page<ClientInstaller> result = service.findLastActiveClientInstallers(100, 10);
        assertNotNull(result);
        assertFalse(result.hasContent());
        assertFalse(result.iterator().hasNext());
        assertTrue(result.getContent().isEmpty());
        assertEquals(result.getNumberOfElements(), 0);
    }

    @Test
    public void orderingTest() {
        final int count = 20;
        for (int j = 0; j < count; ++j) {
            addedInstallers.add(newClientInstaller());
        }

        Page<ClientInstaller> result = service.findLastActiveClientInstallers(0, 10);
        assertEquals(result.getNumberOfElements(), 10);
        assertTrue(result.getTotalElements() >= 20);
        assertTrue(result.getTotalPages() >= 2);

        long id = -1;
        for (ClientInstaller installer : result.getContent()) {
            if (id != -1) {
                assertTrue(installer.getId() < id);
            }
            id = installer.getId();
        }
    }

    @AfterClass
    public void afterClass() {
        clientJpsRepository.delete(addedInstallers);
    }
}
