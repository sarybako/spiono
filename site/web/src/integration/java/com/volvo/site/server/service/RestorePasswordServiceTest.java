package com.volvo.site.server.service;

import com.volvo.platform.spring.email.service.SimpleMailSendEvent;
import com.volvo.site.mailing.service.EmailTemplateService;
import com.volvo.site.server.model.entity.ForgotPassword;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.ForgotPasswordJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.service.exception.EntityNotFoundException;
import com.volvo.site.server.service.exception.UniqueKeyIsAlreadyUsedException;
import com.volvo.site.server.service.exception.UniqueKeyTimedOutException;
import com.volvo.site.server.service.exception.UserInactiveException;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.spring.ApplicationContextListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.Locale;

import static com.volvo.platform.java.testing.TestUtils.*;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createInactiveRandomUser;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static com.volvo.site.server.testutil.spring.ApplicationContextListener.createContextListener;
import static java.util.UUID.randomUUID;
import static org.testng.Assert.*;

public class RestorePasswordServiceTest extends AbstractFullSpringTest {

    @Autowired
    RestorePasswordService restorePasswordService;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    ForgotPasswordJpaRepository forgotPasswordJpaRepository;

    @PersistenceContext
    EntityManager em;

    @Value("${mail.username}")
    private String fromEmailUserName;

    @Autowired
    EmailTemplateService emailTemplateService;

    @Autowired
    ApplicationContext context;

    @Test(expectedExceptions = NullPointerException.class)
    public void testRequestPasswordResetNullEmail(){
        restorePasswordService.requestPasswordReset(null, Locale.ENGLISH);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testRequestPasswordResetUserNotFound(){
        restorePasswordService.requestPasswordReset(randomEmail(), Locale.ENGLISH);
    }

    @Test(expectedExceptions = UserInactiveException.class)
    public void testRequestPasswordResetUserInactive(){
        User localUser = userJpaRepository.save(createInactiveRandomUser());
        restorePasswordService.requestPasswordReset(localUser.getEmail(), Locale.ENGLISH);
    }

    @Test
    public void testRequestPasswordResetCreateRequestWithNewForgotPassword(){
        // Given
        User user = createNewActiveUser();
        assertEquals(userJpaRepository.findOne(user.getId()).getForgotPasswords().size(), 0);


        // When
        restorePasswordService.requestPasswordReset(user.getEmail(), Locale.ENGLISH);
        em.refresh(user);

        // Then
        assertEquals(user.getForgotPasswords().size(), 1);
        ForgotPassword password = user.getForgotPasswords().get(0);
        em.refresh(password);

        assertEquals(password.getStatus(), ForgotPassword.ForgotPasswordStatus.CREATED);
        assertEquals(Long.valueOf(password.getUserId()), user.getId());
        assertEquals(password.getPwdResetDate(), null);
        assertValidUUID(password.getUniqueKey());
    }

    @Test
    public void testRequestPasswordResetWithExistingRequest() throws InterruptedException {
        // Given
        User user = createNewActiveUser();
        restorePasswordService.requestPasswordReset(user.getEmail(), Locale.ENGLISH);

        em.refresh(user);
        assertEquals(user.getForgotPasswords().size(), 1);
        Date oldCrDate = user.getForgotPasswords().get(0).getCrDate();

        // When
        restorePasswordService.requestPasswordReset(user.getEmail(), Locale.ENGLISH);

        // Then
        em.refresh(user);
        Date newCrDate = user.getForgotPasswords().get(0).getCrDate();
        assertEquals(user.getForgotPasswords().size(), 1);
        assertTrue(newCrDate.after(oldCrDate));
    }

    @Test
    public void testRequestPasswordEmailSend() {
        ApplicationContextListener<SimpleMailSendEvent> listener = createContextListener(context);

        User user = createNewActiveUser();
        String email = user.getEmail();
        userJpaRepository.save(user);
        restorePasswordService.requestPasswordReset(email, Locale.ENGLISH);
        ForgotPassword forgotPassword = forgotPasswordJpaRepository.findByUserIdAndStatus(
            userJpaRepository.findUserIdByEmail(email), ForgotPassword.ForgotPasswordStatus.CREATED);

        SimpleMailMessage expectedEmail =
                emailTemplateService.forgotPasswordEmail(email, forgotPassword.getUniqueKey(), Locale.ENGLISH);
        expectedEmail.setFrom(fromEmailUserName);
        SimpleMailSendEvent expectedEvent = new SimpleMailSendEvent(this, expectedEmail);
        SimpleMailSendEvent event = listener.findEvent(expectedEvent);
        assertNotNull(event);

        listener.stopListening();
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testCheckPasswordResetKeyNullKey(){
        restorePasswordService.checkPasswordResetKey(null);
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testCheckPasswordResetKeyNotFound(){
        restorePasswordService.checkPasswordResetKey(randomUUID().toString());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testCheckPasswordResetInvalidKey(){
        restorePasswordService.checkPasswordResetKey("invalid-key-format");
    }

    @Test
    public void testCheckPasswordResetKeySuccessful(){
        User localUser = userJpaRepository.save(createInactiveRandomUser());
        ForgotPassword forgotPassword = ForgotPassword.of(localUser);
        forgotPasswordJpaRepository.save(forgotPassword);
        restorePasswordService.checkPasswordResetKey(forgotPassword.getUniqueKey());
    }

    @Test(expectedExceptions = UniqueKeyTimedOutException.class)
    public void testCheckPasswordResetKeyTimedOut(){
        User localUser = userJpaRepository.save(User.of(randomEmail(),randomPwd()));
        ForgotPassword forgotPassword = ForgotPassword.of(localUser);
        forgotPasswordJpaRepository.save(forgotPassword);

        forgotPassword.setStatus(ForgotPassword.ForgotPasswordStatus.TIMED_OUT);
        forgotPasswordJpaRepository.save(forgotPassword);
        restorePasswordService.checkPasswordResetKey(forgotPassword.getUniqueKey());
    }

    @Test(expectedExceptions = UniqueKeyIsAlreadyUsedException.class)
    public void testCheckPasswordResetKeyIsAlreadyUsed(){
        User localUser = userJpaRepository.save(User.of(randomEmail(),randomPwd()));
        ForgotPassword forgotPassword = ForgotPassword.of(localUser);
        forgotPasswordJpaRepository.save(forgotPassword);

        forgotPassword.setStatus(ForgotPassword.ForgotPasswordStatus.PWD_CHANGED);
        forgotPasswordJpaRepository.save(forgotPassword);
        restorePasswordService.checkPasswordResetKey(forgotPassword.getUniqueKey());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testChangePasswordNullKey(){
        restorePasswordService.changePassword(null, randomPwd());
    }

    @Test(expectedExceptions = EntityNotFoundException.class)
    public void testChangePasswordInvalidKey(){
        restorePasswordService.changePassword("invalid-key-format", randomPwd());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testChangePasswordNullPassword(){
        restorePasswordService.changePassword(randomUUID().toString() , null);
    }

    @Test
    public void testChangePasswordSuccessful(){
        User user = createNewActiveUser();
        String oldPassword = user.getPassword();

        restorePasswordService.requestPasswordReset(user.getEmail(), Locale.ENGLISH);
        String uniqueKey = forgotPasswordJpaRepository.findByUserIdAndStatus(user.getId(),
                ForgotPassword.ForgotPasswordStatus.CREATED).getUniqueKey();
        user = restorePasswordService.changePassword(uniqueKey, randomPwd());

        assertNotEquals(oldPassword, user.getPassword());
        ForgotPassword forgotPassword = user.getForgotPasswords().get(0);
        assertNotNull(forgotPassword.getPwdResetDate());
        assertEquals(forgotPassword.getStatus(), ForgotPassword.ForgotPasswordStatus.PWD_CHANGED);
        assertEquals(forgotPassword.getUser().getId(), user.getId());
    }

    private User createNewActiveUser() {
        return userJpaRepository.save(createRandomActiveUser());
    }
}
