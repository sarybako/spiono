package com.volvo.site.server.repository.testing;


import com.volvo.site.server.model.entity.Installer;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.orm.jpa.JpaSystemException;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;

import static com.volvo.platform.java.JavaUtils.invokeConstructorQuietly;
import static com.volvo.site.server.testutil.RepositoryTestUtils.*;
import static org.apache.commons.lang3.ArrayUtils.EMPTY_BYTE_ARRAY;
import static org.testng.Assert.assertEquals;

public abstract class InstallerJpaTestBase<
        TModel extends Installer,
        TRepository extends JpaRepository<TModel, Long>>
        extends AbstractFullSpringTest {

    private final Class<TModel> modelClass;

    protected InstallerJpaTestBase(Class<TModel> modelClass) {
        this.modelClass = modelClass;
    }

    protected abstract TRepository getRepository();

    @Test
    public void createTest() {
        save(random255Bytes);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createNullTest() {
        save((byte[])null);
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createEmptyTest() {
        save(EMPTY_BYTE_ARRAY);
    }

    @Test(expectedExceptions = JpaSystemException.class)
    public void createMaxSizeTest() {
        save(mysqlMediumBlobData());
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createTooLargeTest() {
        save(mysqlMediumBlobDataPlusOneByte());
    }

    @Test
    public void createAndReadDataTest() {
        Long id = save(new byte[] {1, 2, 3}).getId();
        assertEquals(getRepository().findOne(id).getData(), new byte[]{1, 2, 3});
    }

    protected TModel save(byte[] data) {
        TModel model = invokeConstructorQuietly(modelClass);
        model.setData(data);
        return save(model);
    }

    protected TModel save(TModel installer) {
        return getRepository().save(installer);
    }
}
