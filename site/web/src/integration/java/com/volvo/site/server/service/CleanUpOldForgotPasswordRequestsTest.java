package com.volvo.site.server.service;

import com.volvo.platform.java.JavaUtils;
import com.volvo.site.server.model.entity.ForgotPassword;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.repository.ForgotPasswordJpaRepository;
import com.volvo.site.server.repository.UserJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.RepositoryTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.Locale;

import static com.volvo.platform.java.DateTimeUtils.nowMinusDays;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;


public class CleanUpOldForgotPasswordRequestsTest extends AbstractFullSpringTest {

    @Autowired
    ForgotPasswordJpaRepository forgotPasswordRepository;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    RestorePasswordService restorePasswordService;

    ForgotPassword forgotPasswordRequest;


    @Test
    @Rollback(false)
    public void testCreateRequest(){
        User user = createNewActiveUser();
        restorePasswordService.requestPasswordReset(user.getEmail(), Locale.ENGLISH);
        forgotPasswordRequest = forgotPasswordRepository.findByUserIdAndStatus(user.getId(),
                ForgotPassword.ForgotPasswordStatus.CREATED);
        assertNotNull(forgotPasswordRequest);
    }

    @Test(dependsOnMethods = "testCreateRequest")
    @Rollback(false)
    public void testHackDate() throws IllegalAccessException {
        forgotPasswordRequest = forgotPasswordRepository.findOne(forgotPasswordRequest.getId());
        JavaUtils.setPrivateFieldValue(ForgotPassword.class, forgotPasswordRequest, "crDate", nowMinusDays(3));
        forgotPasswordRequest = forgotPasswordRepository.save(forgotPasswordRequest);
    }

    @Test(dependsOnMethods = "testHackDate")
    @Rollback(false)
    public void testCleanUpOld() {
        restorePasswordService.cleanUpOldRequests();
    }

    @Test(dependsOnMethods = "testCleanUpOld")
    public void testCheckCleaned() {
        forgotPasswordRequest = forgotPasswordRepository.findByUniqueKey(
                forgotPasswordRequest.getUniqueKey());
        assertNotNull(forgotPasswordRequest);
        assertEquals(forgotPasswordRequest.getStatus(), ForgotPassword.ForgotPasswordStatus.TIMED_OUT);
    }


    @AfterClass
    @Rollback(false)
    private void testDeleteCreated(){
        userJpaRepository.delete(forgotPasswordRequest.getUser());
    }

    private User createNewActiveUser() {
        return userJpaRepository.save(RepositoryTestUtils.createRandomActiveUser());
    }
}
