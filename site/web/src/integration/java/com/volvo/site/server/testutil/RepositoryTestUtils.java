package com.volvo.site.server.testutil;

import com.volvo.site.server.model.ModelConstants;
import com.volvo.site.server.model.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwd;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RepositoryTestUtils {

    public static final byte[] random255Bytes = new byte[Byte.MAX_VALUE];

    public static byte[] mysqlMediumBlobData() {
        return new byte[ModelConstants.mysqlMediumBlobMaxSize];
    }

    public static byte[] mysqlMediumBlobDataPlusOneByte() {
        return new byte[ModelConstants.mysqlMediumBlobMaxSize + 1];
    }

    public static User createActiveUserWithPassword(String password) {
        return makeUserActive(User.of(randomEmail(), password));
    }

    public static User createRandomActiveUser() {
        return makeUserActive(createInactiveRandomUser());
    }

    public static User createInactiveRandomUser() {
        return User.of(randomEmail(), randomPwd());
    }

    private static User makeUserActive(User user) {
        user.setActive(true);
        return user;
    }
}
