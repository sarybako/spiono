package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.ForgotPassword;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.Test;

import static com.volvo.platform.java.testing.TestUtils.randomEmail;
import static com.volvo.platform.java.testing.TestUtils.randomPwdHash;
import static com.volvo.site.server.model.entity.ForgotPassword.ForgotPasswordStatus;
import static org.testng.Assert.*;


public class UserForgotPasswordOneToManyTest extends AbstractFullSpringTest {

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    ForgotPasswordJpaRepository forgotPasswordJpaRepository;

    private User user;

    @Test
    @Rollback(false)
    public void testCascadeCreate() {
        user = userJpaRepository.save(User.of(randomEmail(),randomPwdHash()));
        user.addForgotPassword(forgotPasswordJpaRepository.save(ForgotPassword.of(user)));
        user.addForgotPassword(forgotPasswordJpaRepository.save(ForgotPassword.of(user)));
    }

    @Test(dependsOnMethods = "testCascadeCreate")
    @Rollback(false)
    public void testRecordExisted() {
        User loadedUser = userJpaRepository.findOne(user.getId());
        assertFalse(loadedUser.getForgotPasswords().isEmpty());
    }

    @Test(dependsOnMethods = "testRecordExisted")
    @Rollback(false)
    public void testUpdateRecord(){
        User newUser = userJpaRepository.findOne(user.getId());

        assertFalse(newUser.isActive());
        newUser.setActive(true);

        userJpaRepository.save(newUser);
        for (ForgotPassword forgotPassword : newUser.getForgotPasswords()){
            forgotPassword.setStatus(ForgotPasswordStatus.PWD_CHANGED);
        }
    }

    @Test(dependsOnMethods = "testUpdateRecord")
    public void testCascadeReadAfterUpdate(){
        User newUser = userJpaRepository.findOne(user.getId());

        assertTrue(newUser.isActive());
        for (ForgotPassword forgotPassword : newUser.getForgotPasswords()){
            assertEquals(forgotPassword.getStatus(),ForgotPasswordStatus.PWD_CHANGED);
        }
    }

    @Test(dependsOnMethods = "testCascadeReadAfterUpdate")
    @Rollback(false)
    public void testCascadeDelete(){
        userJpaRepository.delete(user);
    }

    @Test(dependsOnMethods = "testCascadeDelete")
    public void testCheckCascadeDelete(){
        assertFalse(userJpaRepository.exists(user.getId()));
        for(ForgotPassword forgotPassword: user.getForgotPasswords()){
            assertFalse(forgotPasswordJpaRepository.exists(forgotPassword.getId()));
        }
    }
}
