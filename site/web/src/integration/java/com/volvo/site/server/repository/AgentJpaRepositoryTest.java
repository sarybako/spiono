package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.Agent;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.model.entity.User;
import com.volvo.site.server.service.WinAppInstallerService;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import com.volvo.site.server.testutil.RepositoryTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.service.UserService.FILTER_AGENT_STATUS_OFF;
import static com.volvo.site.server.service.UserService.SEARCHING_NAME_ANY;
import static com.volvo.site.server.testutil.RepositoryTestUtils.createRandomActiveUser;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;
import static org.testng.Assert.*;

@Test(dependsOnGroups = "SingleClientInstaller")
public class AgentJpaRepositoryTest extends AbstractFullSpringTest {

    @Autowired
    AgentJpaRepository agentJpaRepository;

    @Autowired
    WinAppInstallerService winAppInstallerService;

    @Autowired
    UserJpaRepository userJpaRepository;

    private ClientInstaller clientInstaller;
    private static final String SEARCHING_STRING = "THIS IS NAME OF AGENT FOR SEARCH";

    @BeforeClass
    public void beforeClass(){
        clientInstaller = winAppInstallerService.addClientInstaller(RepositoryTestUtils.random255Bytes);
        winAppInstallerService.addAgentInstaller(clientInstaller.getId(), random255Bytes, true);
        winAppInstallerService.activateClientInstaller(clientInstaller.getId());
    }

    @Test
    public void findByIdAndKeyPositiveTest() {
        Agent agent = createNormalAgent();

        assertEquals(agentJpaRepository.findByIdAndClientKey(agent.getId(), agent.getClientKey()), agent);
        assertNull(agentJpaRepository.findByIdAndClientKey(agent.getId(), agent.getClientKey() + "another"));
        assertNull(agentJpaRepository.findByIdAndClientKey(agent.getId() + 1, agent.getClientKey()));
    }

    @Test
    public void findByIdAndStatusTest() {
        Agent agent = createNormalAgent();

        assertEquals(agentJpaRepository.findByIdAndStatus(agent.getId(), Agent.AgentStatus.AGENT_CREATED), agent);
        assertNull(agentJpaRepository.findByIdAndStatus(agent.getId(), Agent.AgentStatus.AGENT_DOWNLOADED));
        assertNull(agentJpaRepository.findByIdAndStatus(0, Agent.AgentStatus.AGENT_CREATED));
    }

    @Test
    public void findByClientKeyAndStatusTest() {
        Agent agent = createNormalAgent();

        assertEquals(agentJpaRepository.findByClientKeyAndStatus(agent.getClientKey(), Agent.AgentStatus.AGENT_CREATED), agent);
        assertNull(agentJpaRepository.findByClientKeyAndStatus(agent.getClientKey(), Agent.AgentStatus.AGENT_DOWNLOADED));
        assertNull(agentJpaRepository.findByClientKeyAndStatus("bad-key", Agent.AgentStatus.AGENT_DOWNLOADED));
    }

    @Test
    public void findByCertificateTest() {
        Agent agent = createNormalAgent();
        agent.generateCertificate();
        agentJpaRepository.save(agent);

        assertEquals(agentJpaRepository.findByCertificate(agent.getCertificate()), agent);
        assertNull(agentJpaRepository.findByCertificate(agent.getCertificate() + "1"));
    }

    @Test
    public void findAgentsForUserFullTest(){
        //Given
        User user = createFewAgents();

        //When
        List<Agent> agentList = getDesiredAgents(user, SEARCHING_NAME_ANY, false, FILTER_AGENT_STATUS_OFF);
        //Then
        assertEquals(agentList.size(), 10);
        assertTrue(checkOrderDesc(agentList));

        //When
        agentList = getDesiredAgents(user, SEARCHING_STRING, false, FILTER_AGENT_STATUS_OFF);
        Iterator<Agent> iterator = agentList.iterator();
        //Then
        assertEquals(agentList.size(), 5);
        assertTrue(checkOrderDesc(agentList));
        while(iterator.hasNext()){
            assertEquals(iterator.next().getName(), SEARCHING_STRING);
        }

        //When
        agentList = getDesiredAgents(user, SEARCHING_NAME_ANY, true, newArrayList(Agent.AgentStatus.AGENT_DOWNLOADED));
        iterator = agentList.iterator();
        //Then
        assertEquals(agentList.size(), 5);
        assertTrue(checkOrderDesc(agentList));
        while(iterator.hasNext()){
            assertEquals(iterator.next().getStatus(), Agent.AgentStatus.AGENT_DOWNLOADED);
        }
    }

    private Agent createNormalAgent() {
        User user = userJpaRepository.save(createRandomActiveUser());
        return agentJpaRepository.save(Agent.of(user, clientInstaller));
    }

    private User createFewAgents(){
        User user = userJpaRepository.save(createRandomActiveUser());
        createFewNamedAgentsForUser(user, SEARCHING_STRING);
        createFewAgentsWithStatusForUser(user, Agent.AgentStatus.AGENT_DOWNLOADED);
        return user;
    }

    private void createFewNamedAgentsForUser(User user, String agentName){
        for(int i = 0; i < 5; i++){
            Agent agent = Agent.of(user, clientInstaller);
            agent.setName(agentName);
            agentJpaRepository.save(agent);
        }
    }

    private void createFewAgentsWithStatusForUser(User user, Agent.AgentStatus agentStatus){
        for(int i = 0; i < 5; i++){
            Agent agent = Agent.of(user, clientInstaller);
            agentJpaRepository.save(agent);
            agent = agentJpaRepository.findOne(agent.getId());
            agent.setStatus(agentStatus);
            agentJpaRepository.save(agent);
        }
    }

    private List<Agent> getDesiredAgents(User user, String searchingString, boolean isStatusFilterEnabled,
                                         List<Agent.AgentStatus> statusFilter ){
        return agentJpaRepository.findAgentsForUser(user.getId(), searchingString, isStatusFilterEnabled,
                statusFilter, new PageRequest(0, 10)).getContent();
    }

    private boolean checkOrderDesc(List<Agent> agentList){
        if(!CollectionUtils.isEmpty(agentList)){
            Iterator<Agent> iterator = agentList.iterator();

            Agent agent = iterator.next();
            long lastId = agent.getId();

            while (iterator.hasNext()){
                agent = iterator.next();
                if(lastId < agent.getId()){
                    return false;
                }
                lastId = agent.getId();
            }
            return true;
        }
        return false;
    }
}
