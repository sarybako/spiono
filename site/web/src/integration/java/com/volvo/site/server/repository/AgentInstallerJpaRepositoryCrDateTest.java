package com.volvo.site.server.repository;

import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.testing.HasCrDateTestBase;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;

public class AgentInstallerJpaRepositoryCrDateTest extends HasCrDateTestBase<AgentInstaller, AgentInstallerJpaRepository> {

    @Autowired
    @Getter
    AgentInstallerJpaRepository repository;

    @Autowired
    ClientInstallerJpaRepository clientRepository;

    private ClientInstaller clientInstaller;

    protected AgentInstallerJpaRepositoryCrDateTest() {
        super(AgentInstaller.class);
    }

    @BeforeClass
    public void beforeClass() {
        clientInstaller = clientRepository.save(ClientInstaller.of(random255Bytes));
    }

    @AfterClass
    public void afterClass() {
        clientRepository.delete(clientInstaller);
    }

    @Override
    protected AgentInstaller createModel() {
        AgentInstaller installer = super.createModel();
        installer.setData(random255Bytes);
        installer.setClientInstaller(clientInstaller);
        return installer;
    }
}
