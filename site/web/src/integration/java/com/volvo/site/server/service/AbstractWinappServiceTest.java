package com.volvo.site.server.service;


import com.volvo.site.server.model.entity.AgentInstaller;
import com.volvo.site.server.model.entity.ClientInstaller;
import com.volvo.site.server.repository.ClientInstallerJpaRepository;
import com.volvo.site.server.testutil.AbstractFullSpringTest;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.volvo.site.server.testutil.RepositoryTestUtils.random255Bytes;

public abstract class AbstractWinappServiceTest extends AbstractFullSpringTest {

    @Autowired
    protected WinAppInstallerService service;

    @Autowired
    protected ClientInstallerJpaRepository clientJpsRepository;

    private final List<ClientInstaller> clientInstallers = newArrayList();

    protected ClientInstaller newActiveClientInstaller() {
        ClientInstaller installer = newClientInstaller();
        newActiveAgentInstaller(installer.getId());
        service.activateClientInstaller(installer.getId());
        return installer;
    }

    protected Pair<ClientInstaller, AgentInstaller> newActiveClientAndAgentInstallers() {
        ClientInstaller clientInstaller = newClientInstaller();
        AgentInstaller agentInstaller = newActiveAgentInstaller(clientInstaller.getId());
        service.activateClientInstaller(clientInstaller.getId());
        return Pair.of(clientInstaller, agentInstaller);
    }

    protected ClientInstaller newClientInstaller() {
        return newClientInstaller(random255Bytes);
    }

    protected ClientInstaller newClientInstaller(byte[] bytes) {
        ClientInstaller result = service.addClientInstaller(bytes);
        clientInstallers.add(result);
        return result;
    }

    protected AgentInstaller newInactiveAgentInstaller() {
        return newInactiveAgentInstaller(newClientInstaller().getId());
    }

    protected AgentInstaller newInactiveAgentInstaller(long clientInstallerId) {
        return newInactiveAgentInstaller(clientInstallerId, random255Bytes);
    }

    protected AgentInstaller newInactiveAgentInstaller(long clientInstallerId, byte[] data) {
        return newAgentInstaller(clientInstallerId, data, false);
    }

    protected AgentInstaller newActiveAgentInstaller(long clientInstallerId) {
        return newActiveAgentInstaller(clientInstallerId, random255Bytes);
    }

    protected AgentInstaller newActiveAgentInstaller(long clientInstallerId, byte[] data) {
        return newAgentInstaller(clientInstallerId, data, true);
    }

    protected AgentInstaller newAgentInstaller(long clientInstallerId, byte[] data, boolean active) {
        return service.addAgentInstaller(clientInstallerId, data, active);
    }

    @AfterClass
    public void afterSuperClass() {
         for (ClientInstaller clientInstaller : clientInstallers) {
            if (clientJpsRepository.exists(clientInstaller.getId())) {
                clientJpsRepository.delete(clientInstaller);
            }
         }
    }
}
