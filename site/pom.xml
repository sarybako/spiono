
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
	http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.volvoclubnn</groupId>
    <artifactId>site-parent</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <name>Spiono</name>
    <url>http://spiono.com</url>

    <modules>
        <module>platform</module>
        <module>components</module>
        <module>web</module>
    </modules>

    <properties>
        <!-- Standard unit tests area -->
        <tests.srcdir>src/test/java</tests.srcdir>

        <org.springframework-version>3.2.1.RELEASE</org.springframework-version>
        <org.springsecurity-version>3.1.3.RELEASE</org.springsecurity-version>
        <spring.data.jpa.version>1.3.0.RELEASE</spring.data.jpa.version>
        <java-version>1.7</java-version>
        <guava-version>11.0.2</guava-version>
        <commons-lang3-version>3.0</commons-lang3-version>
        <commons-io-version>1.3.2</commons-io-version>
        <commons-codec-version>1.4</commons-codec-version>
        <commons-collections-version>3.1</commons-collections-version>
        <javax.servlet-api-version>3.0.1</javax.servlet-api-version>
        <freemarker-version>2.3.19</freemarker-version>
        <testng-version>6.8</testng-version>
        <guava-version>12.0</guava-version>
        <org.slf4j-version>1.6.4</org.slf4j-version>
        <cglib-version>2.2.2</cglib-version>
        <hibernate.version>3.6.9.Final</hibernate.version>
        <bonecp.version>0.7.1.RELEASE</bonecp.version>
        <apache.tiles-version>2.2.2</apache.tiles-version>
        <org.json-version>20090211</org.json-version>
        <jackson-version>1.9.4</jackson-version>
        <validation.api-version>1.0.0.GA</validation.api-version>
        <hibernate.validator-version>4.2.0.Final</hibernate.validator-version>
        <flyway-version>1.6.1</flyway-version>
        <joda-time.version>2.1</joda-time.version>
        <mockito.version>1.9.5</mockito.version>
        <reflections.version>0.9.8</reflections.version>
        <gwtVersion>2.5.0</gwtVersion>
        <gwt-bootstrap-version>2.3.2.0-SNAPSHOT</gwt-bootstrap-version>
        <guava-gwt-version>14.0-rc1</guava-gwt-version>
        <gin-version>1.5.0</gin-version>
        <warname>volvoclubnn</warname>
        <powermock.version>1.5</powermock.version>
    </properties>


    <dependencies>
        <!-- Lomboc routine helper -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>0.11.6</version>
            <scope>provided</scope>
        </dependency>

        <!-- Testing -->
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>${testng-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${org.springframework-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-testng</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-api-mockito</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Logging -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${org.slf4j-version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${org.slf4j-version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${org.slf4j-version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.16</version>
            <exclusions>
                <exclusion>
                    <groupId>javax.jms</groupId>
                    <artifactId>jms</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.sun.jdmk</groupId>
                    <artifactId>jmxtools</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.sun.jmx</groupId>
                    <artifactId>jmxri</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda-time.version}</version>
        </dependency>
    </dependencies>

    <repositories>
        <!-- Spring repo -->
        <repository>
            <id>com.springsource.repository.maven.release</id>
            <url>http://maven.springframework.org/release/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>repository.jboss.org-public</id>
            <name>JBoss repository</name>
            <url>https://repository.jboss.org/nexus/content/groups/public</url>
        </repository>
    </repositories>


    <build>
        <testSourceDirectory>${tests.srcdir}</testSourceDirectory>
        <plugins>
            <!-- Java compiler -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java-version}</source>
                    <target>${java-version}</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>integration</id>
            <properties>
                <tests.srcdir>src/integration/java</tests.srcdir>
            </properties>
        </profile>
    </profiles>


</project>